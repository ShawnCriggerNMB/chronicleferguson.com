<?php

if ( ! function_exists( 'human_filesize' ) ) :
	/**
	 * Return sizes readable by humans
	 */
	function human_filesize($bytes, $decimals = 2)
	{
	  $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
	  $factor = floor((strlen($bytes) - 1) / 3);

	  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
	      @$size[$factor];
	}
endif;
// ------------------------------------------------------------------------

if ( ! function_exists( 'is_image' ) ) :
	/**
	 * Is the mime type an image
	 */
	function is_image($mimeType)
	{
	    return starts_with($mimeType, 'image/');
	}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'is_draft' ) ) :
	/**
	 * ECHOs Yes if set to 1 or No if set to 0
	 * @param integer $is_draft
	 */
	function is_draft( $is_draft = 1 )
	{
	    echo ( 0 == $is_draft ) ? 'No' : 'Yes';
	}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'set_active' ) ) :

	/**
	 * Set active page
	 *
	 * @param string $uri
	 * @return string
	 */
	function set_active($uri)
	{
	    return Request::is($uri) ? 'active' : '';
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'content_class' ) ) :

	/**
	 * Returns CSS class name
	 *
	 * @return string
	 */
	function content_class()
	{

		if ( Request::is('blog') OR Request::is('blog/*') ) return 'blog-section';
		if ( Request::is('sponsors') OR Request::is('sponsors/*') ) return 'sponsors-section';
		return 'middle-section';
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'is_blog' ) ) :

	/**
	 * Returns boolean if on Blog page
	 *
	 * @return boolean
	 */
	function is_blog()
	{

		return ( Request::is('blog') OR Request::is('blog/*') ) ? TRUE : FALSE;
	}

endif;
// ------------------------------------------------------------------------

if ( ! function_exists( 'page_image' ) ) :

	/**
	 * Return img url for headers
	 * @param  string|null $value
	 * @return string
	 */
	function page_image( $value = NULL )
	{
	    if ( empty( $value ) )
	    {
	        $value = config( 'blog.page_image' );
	    }
	    if ( ! starts_with( $value, 'http' ) && $value[0] !== '/' )
	    {
	        $value = config('blog.uploads.webpath') . '/page-headers/' . $value;
	    }
	    return $value;
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'selected' ) ) :

	/**
	 * Helper function for select elements
	 * @param  mixed  $value
	 * @param  mixed  $input
	 * @param  boolean $echo
	 * @return mixed
	 */
	function selected( $value, $input, $echo = TRUE )
	{
//		p_dump( $value, $input );
		if ( is_array( $input ) )
		{
			/*
			$sel = FALSE;
			foreach ($value as $key => $value)
			{
				p_dump($input, $value);
				if ( $input != $value ) continue;
				$sel = TRUE;
			}
			*/
			$sel = in_array( $value, $input );// ? TRUE : FALSE;
		} else
		{
			$sel = ( $value == $input ) ? TRUE : FALSE;
		}

		if ( FALSE === $sel ) return;

		$sel = ' selected="selected" ';

		if ( FALSE === $echo ) return $sel;

		echo $sel;
	}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'checked' ) ) :

	/**
	 * Helper function for select elements
	 * @param  mixed  $value
	 * @param  mixed  $input
	 * @param  boolean $echo
	 * @return mixed
	 */
	function checked( $value, $input, $echo = TRUE )
	{
		if ( is_array( $value ) )
		{
			$sel = in_array( $input, $value ) ? TRUE : FALSE;
		} else
		{
			$sel = ( $value == $input ) ? TRUE : FALSE;
		}
		if ( FALSE === $sel ) return;

		$sel = ' checked="checked" ';

		if ( FALSE === $echo ) return $sel;

		echo $sel;
	}
endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'module_header' ) ) :

	/**
	 * Echos module header HTML
	 * @param  string $icon   Font-Awesome icon
	 * @param  string $action Action ( edit/create/etc )
	 * @param  string $module Module name ( users )
	 * @return STDOUT
	 */
	function module_header( $icon = 'user', $action = 'Edit', $module = 'User' )
	{
		$str = ucfirst( $action . ' ' . $module );
		echo "<h2><i class=\"fa fa-{$icon}\">&nbsp;</i> {$str}</h2><hr>\n";
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists( 'fetch_users_ip' ) ) :

	/**
	 * Returns current users IP address or NULL.
	 * @return string|null
	 */
	function fetch_users_ip()
	{
        // Check for X-Forwarded-For headers and use those if found
        if (isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ( '' !== trim( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) )
        {
            return trim( $_SERVER['HTTP_X_FORWARDED_FOR'] );
        } else
        {
            if ( isset( $_SERVER['REMOTE_ADDR'] ) && ( '' !== trim( $_SERVER['REMOTE_ADDR'] ) ) )
            {
                return trim( $_SERVER['REMOTE_ADDR'] );
            }
        }
        return NULL;
	}

endif;

// ------------------------------------------------------------------------

if ( ! function_exists('p_dump'))
{
	/**
	* Outputs the given variables with formatting and location. Huge props
	* out to Phil Sturgeon for this one (http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications).
	* To use, pass in any number of variables as arguments.
	*
	* @return void
	*/

	function p_dump()
	{
		list($callee) = debug_backtrace();
		$arguments = func_get_args();
		$total_arguments = count($arguments);

		echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">';
		echo '<legend style="background:lightgrey; padding:5px;">'.$callee['file'].' @ line: '.$callee['line'].'</legend><pre>';

		$i = 0;
		foreach ($arguments as $argument)
		{
			echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: ';
			if ( (is_array($argument) || is_object($argument)) && count($argument))
			{
				print_r($argument);
			} else {
				var_dump($argument);
			}
		}

		echo '</pre>' . PHP_EOL;
		echo '</fieldset>' . PHP_EOL;
	}
}

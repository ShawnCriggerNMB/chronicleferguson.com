<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Image extends Model
{

//	protected $otherKey = 'id';

    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------

    /**
     * Get the comments for the gallery image.
     * @return Illuminate\Database\Eloquent\Model
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    // ------------------------------------------------------------------------

    /**
     * Get the comments for the gallery image likes.
     * @return Illuminate\Database\Eloquent\Model
     */
    public function likes()
    {
        return $this->hasMany('App\Image_Like');
    }

    // ------------------------------------------------------------------------


    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAddSort($query, $sort=NULL)
    {
        $location = \Input::get('location');
        $camera   = \Input::get('camera');

        if ( strlen( $location ) > 0 && strlen( $camera ) > 0 )
        {
            $sort = 'both';
        }

        switch ( $sort )
        {

            case 'chronology':
                $dir = \Input::get('chronology');
                if ( ! $dir OR 'desc' === $dir )
                {
                    return $query->orderBy('taken_at', 'DESC');
                }
                return $query->orderBy('taken_at', 'ASC');
                break;
            case 'photographer':
                $name  = \Input::get('photographer');
                if ( NULL === $name )
                {
                    return $query->orderBy('name', 'ASC');
                }
                return $query->where('name', '=', $name)->orderBy('name', 'ASC');
                break;
            case 'both':
                return static::where('location', '=', $location)->where('camera', '=', $camera);
            case 'location':
                return static::where('location', '=', $location);
            case 'camera':
                return static::where('camera', '=', $camera);
                break;
            case 'allDates' :
                return $query->orderBy('taken_at', 'ASC');
                break;
            case 'dates' :
                $start  = \Input::get('start_date');
                $end    = \Input::get('end_date');
                $start  = date( 'Y-m-d', strtotime( $start ) );
                $end    = date( 'Y-m-d', strtotime( $end ) );
                return $query->where('taken_at', '>=', $start)->where('taken_at', '<', $end)->orderBy('taken_at', 'ASC');
                break;
            default:
                return $query->orderBy('taken_at', 'ASC');
                break;
        }

        return $query->where('active', 1);
    }

    // ------------------------------------------------------------------------

    /**
    * Return URL to post
    *
    * @return string
    */
    public function url()
    {
        $url = url('gallery/'.$this->id);

        return $url;
    }

    // ------------------------------------------------------------------------

    /**
    * Return next image after this one or null
    *
    * @return Image
    */
    public function nextImage()
    {

//$previous = $this->photo->where('sort', '<', $photo->sort)->where('user_id', '=', $photo->user_id)->orderBy('', 'DESC')->first();

//$next = $this->photo->where('sort', '>', $photo->sort)->where('user_id', '=', $photo->user_id)->orderBy('sort', 'ASC')->first();
        $query = static::where('updated_at', '>', $this->updated_at)->where('updated_at', '<=', Carbon::now())->orderBy('date', 'asc');
//        $query = static::AddSort()->orderBy('date', 'asc');
        return $query->first();
    }

    // ------------------------------------------------------------------------

    /**
    * Return older image before this one or null
    *
    * @param Tag $tag
    * @return Post
    */
    public function prevImage()
    {
        $query = static::where('updated_at', '<', $this->updated_at)->orderBy('date', 'desc');
//        $query = static::AddSort()->orderBy('date', 'asc');
        return $query->first();
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Stringy\Stringy as S;

class Comment extends Model
{
    /**
     * Get the post that owns the comment.
     */
    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    // ------------------------------------------------------------------------

    /**
     * Get the post's author.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    // ------------------------------------------------------------------------

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date
     * @return string
     */
    public function date( $date = NULL )
    {
        if( is_null( $date ) )
        {
            $date = $this->created_at;
        }
        return String::date( $date );
    }

    // ------------------------------------------------------------------------

    /**
     * Returns the date of the blog post creation,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function created_at()
    {
        return $this->date( $this->created_at );
    }

    // ------------------------------------------------------------------------

    /**
     * Returns the date of the blog post last update,
     * on a good and more readable format :)
     *
     * @return string
     */
    public function updated_at()
    {
        return $this->date( $this->updated_at );
    }

    // ------------------------------------------------------------------------

    /**
     * Get the date the post was created.
     *
     * @param \Carbon|null $date
     * @return string
     */
    public function relative_time( $date = NULL )
    {
        if ( ! $date )
        {
            $date = $this->updated_at();
        }

    }
    // ------------------------------------------------------------------------

    public function bad_words( $comment = '' )
    {
        $bad_words = array();
        return S::containsAny( $comment, $bad_words, false );
    }

}
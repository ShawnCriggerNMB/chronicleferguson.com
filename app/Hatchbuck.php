<?php

class Hatchbuck {

    private $api_key  = 'SlktNEFNV2lzOUV2TF9aaGR3bGstV2JzX253anlESWx0T21PbHZFZVFaODE1';
    private $base_url = 'https://api.hatchbuck.com/api/v1/';
    private $url      = NULL;

    // ------------------------------------------------------------------------

    /**
     * Builds array from User Interests and User Type
     * @param  object $user
     * @return array
     */
    public function _build_user_tags($user)
    {
        $tags = array();

        $interests = str_replace( 'Book', 'I want the Book!', $user->interests );
//        $interests = str_replace( '|', PHP_EOL, $interests );
        $interests = explode( '|', $interests );
        foreach ( $interests as $value )
        {
            $value       = strtolower( $value );
            $tags[]      = ucfirst( $value );
        }

        $tags[] = $user->user_type;
        return $tags;
    }

    // ------------------------------------------------------------------------

    /**
     * Retrieves contact information from Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    public function _get_hatchbuck_user( $user = NULL )
    {
        if ( ! $user ) return FALSE;

        $name = explode( ' ', $user->name );

        $data = array(
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),

        );

        $data = json_encode($data);

        $this->url = $this->base_url . 'contact/search?api_key=' . trim( $this->api_key );
        $ch = curl_init( $this->url );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( 200 != $code OR ! is_array( $result ) && isset( $result->Message ) )
        {
            return FALSE;
        }

        foreach ($result as $row)
        {

            $contactId = $row->contactId;
            foreach ($row->emails as $email)
            {
                if ( $email->address === $user->email )
                {
                    return $contactId;
                }
            }
        }

        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Updates contact information on Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    public function _update_hatchbuck_user( $user = NULL, $contactId = NULL )
    {
        if ( ! $user ) return FALSE;

        $name   = explode( ' ', $user->name );
        $method = 'POST';

        $data = array(
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),
            'status' => array(
                'name' => 'Prospect'
            ),
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'company'   => $user->company,
            'addresses' => array(
                'street' => '',
                'city'   => '',
                'country' => '',
                'state'   => '',
                'zip'     => $user->zipcode,
                'type'    => 'Home',
            ),

        );

        if ( is_string( $contactId ) )
        {
            $method = 'PUT';
            $data['contactId'] = $contactId;
        }

        $data = json_encode($data);

        $this->url = $this->base_url . 'contact?api_key=' . trim( $this->api_key );
        $result = $this->_send_data( $data, $method );

        if ( ! $result )
        {
        	return FALSE;
        }

        $result = json_decode( $result );

        return $result->contactId;
    }

    // ------------------------------------------------------------------------

    public function update_hatchbuck_tags( $tags = array(), $contactId = '' )
    {
        $this->url  = $_url . 'contact/' . $contactId . '/Tags?api_key=' . $this->api_key;
        $data = array();

        foreach ($tags as $tag)
        {
            if ( '' == $tag OR ! $tag ) continue;
            $data[] = array(
                'name' => $tag,
            );
        }
        $method = 'POST';

        return $this->_send_data( $data, $method );
        if ( ! $result )
        {
        	return FALSE;
        }

        $result = json_decode( $result );

        return $result->contactId;
    }

    // ------------------------------------------------------------------------

    /**
     * Handles CURL action of sending data to Hatchbuck
     * @param  array  $data
     * @param  string $method
     * @return boolean
     */
    private function _send_data( $data = array(), $method = 'POST' )
    {
        $data = json_encode($data);

        $ch   = curl_init( $this->url );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ( curl_errno( $ch ) )
        {
            print curl_error( $ch );
        } else {
            curl_close( $ch );
        }

        $result = json_decode( $result );

        if ( 200 != $code OR ! is_array( $result ) && isset( $result->Message ) )
        {
            return FALSE;
        }

        return $result;
    }


}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image_Like extends Model
{
    /**
     * Get the post that owns the comment.
     */
    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    /**
     * Get the user that uploaded the image.
     * @return Illuminate\Database\Eloquent\Model
     */
    public function users()
    {
        return $this->hasOne('App\User');
    	return $this->belongsToMany('App\User', 'users');
    }

}

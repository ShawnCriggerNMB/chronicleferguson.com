<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Auth\UserInterface;

use Zizaco\Entrust\Traits\EntrustUserTrait;


use Illuminate\Support\Facades\Config;
//use Zizaco\Entrust\HasRole;

//use Zizaco\Confide\ConfideUser;
//use Zizaco\Confide\ConfideUserInterface;
//use Zizaco\Entrust\HasRole;

//use Illuminate\Auth\Reminders\RemindableInterface;
//use App\Role as Role;
/*
                                    UserInterface,
                                    RemindableInterface,
                                    CanResetPasswordContract


class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
    use Authenticatable, Authorizable, CanResetPassword, EntrustUserTrait;
*/

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable;
    use CanResetPassword;
    use EntrustUserTrait;
    //use HasRole;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'email', 'password', 'zip', 'phone', 'organization', 'institution', 'comment', 'uuid', 'provider', 'provider_id', 'avatar', 'username', 'interests' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    // ------------------------------------------------------------------------

    public function __construct()
    {
      # code...
    }

    /**
     * @return Illuminate\Database\Eloquent\Model
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', Config::get('entrust::assigned_roles_table'));
    }

    // ------------------------------------------------------------------------

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    public function saveRoles($inputRoles)
    {
        if(! empty($inputRoles)) {
            $this->roles()->sync($inputRoles);
        } else {
            $this->roles()->detach();
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {
        $roles = $this->roles;
        $roleIds = false;
        if( !empty( $roles ) ) {
            $roleIds = array();
            foreach( $roles as &$role )
            {
                $roleIds[] = $role->id;
            }
        }
        return $roleIds;
    }

    // ------------------------------------------------------------------------

    /**
     * Returns current user object
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function currentUser()
    {
    	if ( ! class_exists( 'Auth' ) )
    	{
    		abort( 403, 'Class Auth does not exist.');
    	}

        return \Auth::user();
    }

    // ------------------------------------------------------------------------

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
    	if ( ! property_exists( $this, 'email' ) )
    	{
    		abort( 403, 'Class Property email does not exist.');
    	}
        return $this->email;
    }

}
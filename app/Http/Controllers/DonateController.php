<?php
// maybe look into
// https://github.com/logicalgrape/paypal-ipn-laravel
// https://github.com/sh0umik/Laravel5-PaypalIPN
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use \Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Auth\CanResetPassword;

use Illuminate\Foundation\Auth\Authenticatable as AuthenticatableContract;
use Fahim\PaypalIPN\PaypalIPNListener;

//use App\Http\Requests\NewsletterRequest;
use App\User;
use Auth;
use Meta;

//use Eusonlito\LaravelMeta\Meta as Meta;

class DonateController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Hatchbuck API key
     * @var string
     */
    private $api_key = 'SlktNEFNV2lzOUV2TF9aaGR3bGstV2JzX253anlESWx0T21PbHZFZVFaODE1';

    private $_debug = FALSE;
    private $use_sandbox = FALSE;

    // ------------------------------------------------------------------------

    public function __construct()
    {

    	parent::__construct();
//        Meta::title('Contact Us | Chronicle Ferguson');
        // add noindex, nofollow robots tag to prevent search engines indexing contact page.
    }

    // ------------------------------------------------------------------------

    public function show()
    {
        Meta::title('Chronicle Ferguson | Donate');

        return view('pages.paypal-form');
    }

    // ------------------------------------------------------------------------

    public function success()
    {
        Meta::title('Chronicle Ferguson | Donation Successful');
        Meta::set('robots', 'noindex,nofollow');
        return view('pages.paypal-success');
    }

    // ------------------------------------------------------------------------

    public function cancel()
    {
        Meta::title('Chronicle Ferguson | Donation Cancelled');
        Meta::set('robots', 'noindex,nofollow');
        return view('pages.paypal-cancel');
    }

    // ------------------------------------------------------------------------

    public function ipn()
    {
        ini_set('log_errors', true);
        ini_set('error_log', public_path() . '/ipn_errors.log');


        $ipn = new PaypalIPNListener();
        $ipn->use_sandbox = $this->use_sandbox;

        try {
            $verified = $ipn->processIpn();
            $html     = $ipn->getTextReport();
        } catch (Exception $e) {
            \Log::info( $e->getMessage() );
        }
        $subject  = '<h2>New Payment to Chronicle Ferguson</h2>';

        \Mail::send(array(), array(), function ($message) use ($html, $subject) {
            $html = nl2br( $html );

          $message->to( 'ithippyshawn@gmail.com' )
            ->subject( $subject )
            ->from( 'debug@chronicleferguson.com')
            ->setBody( $html, 'text/html' );
        });

        if ( $verified && isset( $_POST['address_status'] ) && $_POST['address_status'] == 'confirmed' )
        {
            \Log::info("payment verified and inserted to db");
        } else
        {
            \Log::info("Some thing went wrong in the payment !");
        }
    }

    // NEWSLETTER SIGNUP / HOM PAGE
    // ------------------------------------------------------------------------

    /**
     * Stores Newsletter signup form step 1 values in Session and displays step 2
     * @param  NewsletterRequest $request
     * @return STDOUT
     */
    public function registerStep2(NewsletterRequest $request)
    {
    	$request->session()->put('name',      $request->get('name') );
    	$request->session()->put('email',     $request->get('email') );
    	$request->session()->put('zipcode',   $request->get('zipcode') );
    	$request->session()->put('comment',   $request->get('comment') );
    	$request->session()->put('user_type', $request->get('_user_type') );
        if ( $request->get( 'phone' ) )
        {
            $request->session()->put('phone', $request->get('phone') );
        }

        $company = '';
        if ( $request->get( 'institution' ) )
        {
            $company = $request->get( 'institution' );
        } elseif ( $request->get( 'organization' ) )
        {
            $company = $request->get( 'organization' );
        }

        $request->session()->put('company', $request->get('company') );

        return view('pages.step2');
    }

    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // CONTACT PAGE
    // ------------------------------------------------------------------------

    /**
     * Displays contact form page
     * @return STDOUT
     */
    public function create()
    {
    	Meta::set('robots', 'noindex,nofollow');
        return view('about.contact');
    }

    // ------------------------------------------------------------------------

    /**
     * Sends contact form notification email
     * @param  ContactFormRequest $request
     * @return VOID
     */
	public function store(ContactFormRequest $request)
	{
        \Mail::send('emails.contact',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message'),
        ), function($message)
        {

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
            $message->from( $mail_from, $mail_name );
            $message->to( $mail_from, $mail_name );
            $message->bcc( 'ithippyshawn@gmail.com', $mail_name )->subject('Feedback');
        });

		\Mail::send('emails.contact_user',
		array(
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'user_message' => $request->get('message'),
		), function($message) use ($request)
		{

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
			$_subject  = \Config::get('blog.contact_subject');

			$message->from( $mail_from, $mail_name );
			$message->to( $request->get('email'), $request->get('name') )->subject( $_subject );
//			$message->bcc( 'ithippyshawn@gmail.com', 'admin' )->subject( $_subject );
		});

		return \Redirect::route('contact')->with('success', 'Thanks for contacting us!');
	}

    // ------------------------------------------------------------------------

}

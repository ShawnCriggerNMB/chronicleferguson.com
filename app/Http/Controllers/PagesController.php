<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Meta;

class PagesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHome()
    {
        Meta::title('Chronicle Ferguson');
        return view('pages.home');
    }

    // ------------------------------------------------------------------------
    // CONTACT PAGE
    // ------------------------------------------------------------------------

    /**
     * Displays contact form page
     * @return STDOUT
     */
    public function contactPage()
    {
        Meta::set('robots', 'index,follow');
        //Meta::meta('robots', 'noindex,nofollow');
        return view('pages.contact');
    }

    // ------------------------------------------------------------------------

    /**
     * Sends contact form notification email
     * @param  ContactFormRequest $request
     * @return VOID
     */
    public function store(ContactFormRequest $request)
    {
        \Mail::send('emails.contact',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message'),
        ), function($message)
        {

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
            $message->from( $mail_from, $mail_name );
            $message->to( $mail_from, $mail_name );
            $message->bcc( 'ithippyshawn@gmail.com', $mail_name )->subject('Feedback');
        });

        \Mail::send('emails.contact_user',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message'),
        ), function($message) use ($request)
        {

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
            $_subject  = \Config::get('blog.contact_subject');

            $message->from( $mail_from, $mail_name );
            $message->to( $request->get('email'), $request->get('name') )->subject( $_subject );
//          $message->bcc( 'ithippyshawn@gmail.com', 'admin' )->subject( $_subject );
        });

        return \Redirect::route('contact')->with('success', 'Thanks for contacting us!');
    }


}

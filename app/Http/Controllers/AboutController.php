<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use \Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Auth\CanResetPassword;

use Illuminate\Foundation\Auth\Authenticatable as AuthenticatableContract;

use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\NewsletterRequest;
use App\User;
//use Eusonlito\LaravelMeta\Meta as Meta;
use Auth;


class AboutController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * User interests
     * @access protected
     * @var array
     */
    protected $user_interests = [ 'activist', 'blogger', 'educator', 'book', 'influencer', 'journalist', 'photojournalist' ];

    private $api_key = 'SlktNEFNV2lzOUV2TF9aaGR3bGstV2JzX253anlESWx0T21PbHZFZVFaODE1';

    private $_debug = FALSE;

    public function __construct()
    {

    	parent::__construct();
//        Meta::title('Contact Us | Chronicle Ferguson');
        // add noindex, nofollow robots tag to prevent search engines indexing contact page.
    }

    // ------------------------------------------------------------------------

    public function getHome()
    {
        Meta::title('Chronicle Ferguson');

        return view('pages.home');
    }

    // NEWSLETTER SIGNUP / HOM PAGE
    // ------------------------------------------------------------------------

    /**
     * Stores Newsletter signup form step 1 values in Session and displays step 2
     * @param  NewsletterRequest $request
     * @return STDOUT
     */
    public function registerStep2(NewsletterRequest $request)
    {
    	$request->session()->put('name', $request->get('name') );
    	$request->session()->put('email', $request->get('email') );
    	$request->session()->put('zipcode', $request->get('zipcode') );
    	$request->session()->put('comment', $request->get('comment') );
    	$request->session()->put('user_type', $request->get('_user_type') );
        if ( $request->get( 'phone' ) )
        {
            $request->session()->put('phone', $request->get('phone') );
        }

        $company = '';
        if ( $request->get( 'institution' ) )
        {
            $company = $request->get( 'institution' );
        } elseif ( $request->get( 'organization' ) )
        {
            $company = $request->get( 'organization' );
        }

        $request->session()->put('company', $request->get('company') );

        return view('pages.step2');
    }

    // ------------------------------------------------------------------------

    /**
     * Validates Newsletter signup and sends data to Hatchbuck CRM
     * @uses   self::_send_hatchbuck()
     * @param  Request $request
     * @return VOID
     */
    public function saveStep2(Request $request)
    {
        $data = $request->all();
        $interests = array();
        foreach ($this->user_interests as $key)
        {
        	if ( ! isset( $data[$key] ) ) continue;
        	$interests[] = ucfirst( $data[$key] );
        }

        $data = array(
        	'name'      => $request->session()->pull('name'),
        	'email'     => $request->session()->pull('email'),
        	'zipcode'   => $request->session()->pull('zipcode'),
            'comment'   => $request->session()->pull('comment'),

            'company'   => $request->session()->pull('company'),
        	'phone'     => $request->session()->pull('phone'),

        	'user_type' => $request->session()->pull('user_type'),
        	'interests' => $interests,
        );

        $data = (object) $data;

//        $user = User::find( $user_id )->first();
//
        $contact = $this->_get_hatchbuck_user( $data );

        $this->_update_hatchbuck_user( $data, $contact );

        return redirect( '/gallery' )->with('user', $data)->with('success','Thank you for registering.');
    }


    // ------------------------------------------------------------------------
    // CONTACT PAGE
    // ------------------------------------------------------------------------

    /**
     * Displays contact form page
     * @return STDOUT
     */
    public function create()
    {
//    	\Meta::meta('robots', 'noindex,nofollow');
        return view('about.contact');
    }

    // ------------------------------------------------------------------------

    /**
     * Sends contact form notification email
     * @param  ContactFormRequest $request
     * @return VOID
     */
	public function store(ContactFormRequest $request)
	{
        \Mail::send('emails.contact',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message'),
        ), function($message)
        {

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
            $message->from( $mail_from, $mail_name );
            $message->to( $mail_from, $mail_name );
            $message->bcc( 'ithippyshawn@gmail.com', $mail_name )->subject('Feedback');
        });

		\Mail::send('emails.contact_user',
		array(
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'user_message' => $request->get('message'),
		), function($message) use ($request)
		{

            $mail_from = \Config::get('blog.email_from');
            $mail_name = \Config::get('blog.email_name');
			$_subject  = \Config::get('blog.contact_subject');

			$message->from( $mail_from, $mail_name );
			$message->to( $request->get('email'), $request->get('name') )->subject( $_subject );
//			$message->bcc( 'ithippyshawn@gmail.com', 'admin' )->subject( $_subject );
		});

		return \Redirect::route('contact')->with('success', 'Thanks for contacting us!');
	}

	// ------------------------------------------------------------------------
	// HATCHBUCK
	// ------------------------------------------------------------------------

    // ------------------------------------------------------------------------

    /**
     * Retrieves contact information from Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    private function _get_hatchbuck_user( $user = NULL )
    {
        if ( ! $user ) return FALSE;
        $_url = 'https://api.hatchbuck.com/api/v1/';
        $name = explode( ' ', $user->name );

        $data = array(
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),

        );

        $data = json_encode($data);

        $url = $_url . 'contact/search?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( 200 != $code OR ! is_array( $result ) && isset( $result->Message ) )
        {
            return FALSE;
        }

        foreach ($result as $row)
        {

            $contactId = $row->contactId;
            foreach ($row->emails as $email)
            {
                if ( $email->address === $user->email )
                {
                    return $contactId;
                }
            }
        }

        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Updates contact information on Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    private function _update_hatchbuck_user( $user = NULL, $contactId = NULL )
    {
        if ( ! $user ) return FALSE;

        $_url   = 'https://api.hatchbuck.com/api/v1/';
        $name   = explode( ' ', $user->name );
        $method = 'POST';

        $data = array(
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),
            'status' => array(
                'name' => 'Prospect'
            ),
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'company'   => $user->company,
            'addresses' => array(
                'street' => '',
                'city'   => '',
                'country' => '',
                'state'   => '',
                'zip'     => $user->zipcode,
                'type'    => 'Home',
            ),

        );

        if ( is_string( $contactId ) )
        {
            $method = 'PUT';
            $data['contactId'] = $contactId;
        }

/*
            'q8_interests8'          => $interests,
            'q9_userCategory'        => $user->user_type,
        );
*/

        $data = json_encode($data);

        $url = $_url . 'contact?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( TRUE === $this->_debug )
        {
            p_dump( $code, $result);
        }

        if ( 200 != $code )
        {
            if ( TRUE === $this->_debug ) die('bad');

            return FALSE;
        }

        $contactId = $result->contactId;

        $url  = $_url . 'contact/' . $contactId . '/Tags?api_key=' . $this->api_key;

        $tags = array();

        $interests = $user->interests;
        if ( is_array( $interests ) )
        {
            $interests = implode( $interests, '|' );
        }

        $interests = str_replace( 'Book', 'I want the Book!', $interests );
//        $interests = str_replace( '|', PHP_EOL, $interests );
        $interests = explode( '|', $interests );
        foreach ( $interests as $value )
        {
            $value       = strtolower( $value );
            $tags[]      = ucfirst( $value );
        }

        $tags[] = $user->user_type;

        $data = array();

        foreach ($tags as $tag)
        {
            if ( '' == $tag OR ! $tag ) continue;
            $data[] = array(
                'name' => $tag,
            );
        }
        $method = 'POST';
        $data = json_encode($data);

//        $url = $_url . 'contact?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( TRUE === $this->_debug )
        {
            p_dump( $code, $result);
//            die();
        }

        if ( 200 != $code OR 201 != $code OR ( ! is_array( $result ) && isset( $result->Message ) ) )
        {
            return FALSE;
        }

//        p_dump( $result );die();

        return TRUE;
    }

    // ------------------------------------------------------------------------

}

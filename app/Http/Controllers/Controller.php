<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//use Eusonlito\LaravelMeta\Meta as Meta;
use Meta;


abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        # Default title
//        Meta::title('Chronicle Ferguson');

        # Default robots
//        Meta::meta('robots', 'index,follow');
    }

}

/**
 * Tests a route against the current URL for active state
 * If true, returns 'selected' for class name
 * Usage: HTML::activeState('named.route')
 */
/*
\HTML::macro('activeState', function ($route) {
    return strpos(\Request::url(), route($route)) !== false ? " class=active-menu " : '';
});

*/
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Validator;
use Illuminate\Http\Request;
//use Eusonlito\LaravelMeta\Meta as Meta;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    private $redirectTo = '/';
    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	parent::__construct();
//        Meta::meta('robots', 'noindex,nofollow');
        $this->middleware('guest');
    }

    // ------------------------------------------------------------------------

    /**
     * Send Reset Password Email and Email for OAUTH registered users to set a password.
     * @param  Request $request
     * @return Redirect

	public function postEmail(Request $request)
	{
	    $this->validate( $request, ['email' => 'required'] );

	    $response = $this->passwords->sendResetLink($request->only('email'), function($message)
	    {
	        $message->subject('Password Reminder');
	    });

	    switch ($response)
	    {
	        case PasswordBroker::RESET_LINK_SENT:
	            return redirect()->back()->with('status', trans($response));

	        case PasswordBroker::INVALID_USER:
	            return redirect()->back()->withErrors(['email' => trans($response)]);
	    }
	}
*/
	// ------------------------------------------------------------------------

    /**
     * Send Reset Password Email and Email for OAUTH registered users to set a password.
     * @param  Request $request
     * @return Redirect
     */
	public function _postEmail(Request $request)
	{
	    $this->validate( $request, ['email' => 'required'] );

	    $response = $this->passwords->sendResetLink($request->only('email'), function($message)
	    {
	        $message->subject('Password Reminder');
	    });

	    switch ($response)
	    {
	        case PasswordBroker::RESET_LINK_SENT:
	            return redirect()->back()->with('status', trans($response));

	        case PasswordBroker::INVALID_USER:
	            return redirect()->back()->withErrors(['email' => trans($response)]);
	    }
	}

	// ------------------------------------------------------------------------



}

<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use \Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Auth\CanResetPassword;

use Illuminate\Foundation\Auth\Authenticatable as AuthenticatableContract;

use Laravel\Socialite\Contracts\Factory as Socialite;
use ResetsPasswords;
use Illuminate\Http\Request;
use App\Http\Requests\SignUpUserRequest;
//use Eusonlito\LaravelMeta\Meta as Meta;
class AuthController extends Controller
{
    /**
     * the User model instance.
     * @access protected
     * @var User
     */
    protected $user;

    /**
     * The Guard implementation.
     * @access protected
     * @var Authenticator
     */
    protected $auth;

    /**
     * Socialite OAUTH provider.
     * @access protected
     * @var Socialite
     */
    protected $socialite;

    /**
     * The password token repository.
     * @access protected
     * @var \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    protected $tokens;

    /**
     * Path to redirect to on successful login.
     * @access private
     * @var string
     */
    private $redirectTo     = '/';
    protected $loginPath    = '/login';
    protected $redirectPath = '/gallery';

    /**
     * User interests
     * @access protected
     * @var array
     */
    protected $user_interests = [ 'activist', 'blogger', 'educator', 'book', 'influencer', 'journalist', 'photojournalist' ];

    private $api_key = 'SlktNEFNV2lzOUV2TF9aaGR3bGstV2JzX253anlESWx0T21PbHZFZVFaODE1';

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
//    public function __construct(Guard $auth, User $user,Socialite $socialite)
    public function __construct(User $user,Socialite $socialite,TokenRepositoryInterface $tokens)
    {
        parent::__construct();

//        \Meta::meta('robots', 'noindex,nofollow');

        if ( isset( $_REQUEST['redirect_to'] ) )
        {
            $this->redirectTo   = $_REQUEST['redirect_to'];
            $this->redirectPath = $_REQUEST['redirect_to'];
        }

        $this->user     = $user;

        $this->socialite = $socialite;
        $this->tokens    = $tokens;
//        $this->auth = $auth;

        // require user to be logged out **EXCEPT** for the following methods.
        $this->middleware('guest', [
            'except' => [ 'getLogout', 'getProfile', 'postProfile', 'unique_email', 'hatchbuck_landing' ]//, 'getSocialAuthCallback', 'getSocialAuth']
        ]);
    }

    // ------------------------------------------------------------------------

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name'         => 'required|min:3|max:255',
            'email'        => 'required|email|max:255|unique:users',
            'password'     => 'required|confirmed|min:6',
            'zipcode'      => 'sometimes|min:4|max:10',
            'phone'        => 'sometimes|min:10|max:12',
            'institution'  => 'sometimes|min:1|max:100',
            'organization' => 'sometimes|min:1|max:100',
            'comment'      => 'sometimes|max:100',
            '_user_type'   => 'required|min:0|max:20',
        ]);
        return $valid;
    }

    // ------------------------------------------------------------------------

    /**
     * Create a new user instance after a valid registration.
     *
     * @access private
     * @param  array  $data
     * @return User
     */
    private function _create(array $data)
    {

        $user = new User;
        $user->name              = $data['name'];
        $user->username          = $data['name'];
        $user->email             = $data['email'];
        $user->password          = bcrypt( $data['password'] );

        $user->zipcode           = $data['zipcode'];

        $user->comments          = self::clean_comments( $data['comment'] );

        $confirmation_code       = str_random(30);
        $user->confirmation_code = $confirmation_code;

        if ( isset( $data['phone'] ) )
        {
            $user->phone = $data['phone'];
        }
        if ( isset( $data['institution'] ) )
        {
            $user->institution = $data['institution'];
        }
        if ( isset( $data['organization'] ) )
        {
            $user->organization = $data['organization'];
        }
        if ( isset( $data['_user_type'] ) )
        {
            $user->user_type = $data['_user_type'];
        }

        $interests = array();
        $interests = implode( '|', $interests );

        $user->interests = $interests;


        $user->save();
        $role = Role::where('name','=','user')->first();
        //return $user;

        $user->attachRole($role);

        return $user;
    }

    // ------------------------------------------------------------------------

    public function getRegister()
    {

        return view('auth.signup');
    }

    // ------------------------------------------------------------------------

    public function getProfile()
    {
        $user = User::find(\Auth::id())->first();
//        $user->role = User::find( $user->id )->roles()->first();

        return view('auth.profile')->with('user', $user);
    }

    // ------------------------------------------------------------------------

    public function postProfile()
    {
        $user = User::find(\Auth::id())->first();
        $data = \Input::all();
        $validation = Validator::make($data, [
            'name'         => 'sometimes|min:3|max:255',
            'password'     => 'sometimes|confirmed|min:6',
            'zipcode'      => 'sometimes|min:4|max:10',
            'phone'        => 'sometimes|min:10|max:12',
            'institution'  => 'sometimes|min:1|max:100',
            'organization' => 'sometimes|min:1|max:100',
            'comment'      => 'sometimes|max:100',
            'user_type'    => 'sometimes|min:0|max:20',
        ]);
        // Check if it fails //
        if( $validation->fails() )
        {
            return redirect()->back()->withInput()->with('errors', $validation->errors() );
        }

        if ( isset( $data['name'] ) )
        {
            $user->name = $data['name'];
        }

        if ( isset( $data['username'] ) )
        {
            $user->name = $data['username'];
        }

        if ( isset( $data['zipcode'] ) )
        {
            $user->zipcode = $data['zipcode'];
        }
        if ( isset( $data['phone'] ) )
        {
            $user->phone = $data['phone'];
        }
        if ( isset( $data['institution'] ) )
        {
            $user->institution = $data['institution'];
        }
        if ( isset( $data['organization'] ) )
        {
            $user->organization = $data['organization'];
        }
        if ( isset( $data['user_type'] ) )
        {
            $user->user_type = $data['user_type'];
        }

        if ( isset( $data['comment'] ) )
        {
            $user->comments = self::clean_comments( $data['comment'] );
        }

        if ( isset( $data['interests'] ) )
        {
            $user->interests = implode( '|', $data['interests'] );
        }

        $user->save();
//        $role = Role::where('name','=','user')->first();
//        $user->attachRole($role);

        return redirect( '/profile' )->with('user', $user)->with('success','Your profile is updated.');
    }

    // ------------------------------------------------------------------------

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(SignUpUserRequest $request)
    {
        $user = $this->_create( $request->all() );
        \Auth::login( $user );
        $contact = $this->_get_hatchbuck_user( $user );

        $this->_update_hatchbuck_user( $user, $contact );


        return redirect( '/gallery' )->with('user', $user)->with('success','Thank you for registering.');
    }

    // ------------------------------------------------------------------------
    // SOCIAL AUTH METHODS
    // ------------------------------------------------------------------------

    /**
     * Opens OAUTH Provider window for access.
     * @param  string $provider Provider name, currently only Facebook implemnted.
     * @return Socialite
     */
    public function getSocialAuth( $provider = NULL )
    {
        if( ! config("services.{$provider}" ) ) abort('404'); //just to handle providers that doesn't exist

        return $this->socialite->with($provider)->redirect();
    }

    // ------------------------------------------------------------------------

    /**
     * Fired on return from OAUTH provider, creates new user.
     *
     * @param  string $provider Social Auth provider, currently only Facebook
     * @return mixed
     */
    public function getSocialAuthCallback($provider=null, Request $request)
    {
        if( ! $data = $this->socialite->with( $provider )->user() )
        {
            die('Bad ' . $provider . ' callback. ');
            //return 'something went wrong';
        }

        $user   = User::where( 'provider_id', '=', $data->id )->first();
        $exists = User::where( 'email', '=', $data->email )->first();

/*
        // user never logged in with facebook but email exists. return false.
        if ( ! $user && $exists )
        {
//            die('@todo: add error msg');
            return redirect()->back()->with('error', 'Your email is not associated with this ' . $provider . ' account.');
            return FALSE;
        }
*/

        // no user, signup
        if ( ! isset( $exists->id ) )
        {
            // kinda cheesey method around hitting a unique
/*
            while ( $exists = User::where( 'username', '=', $data->nickname )->first() )
            {
                $data->nickname .= ' ';
            }
*/
            $user = new User;
            $user->name        = $data->name;
            $user->email       = $data->email;
            $user->provider_id = $data->id;
            $user->provider    = $provider;
            $user->uuid        = $data->id;
            $user->avatar      = $data->avatar;
            $user->username    = $data->nickname;

            $confirmation_code       = str_random(30);
            $user->confirmation_code = $confirmation_code;

            $user->save();

            $role = Role::where('name','=','user')->first();
            $user->attachRole($role);
            $user->save();
            \Auth::login( $user, TRUE );
            $contact = $this->_get_hatchbuck_user( $user );

            $this->_update_hatchbuck_user( $user, $contact );

            return redirect( '/gallery' )->with('status', 'You have successfully signed up.')->with('user', $user)->with('hatchbuck', TRUE );
        } else {
            $user = $exists;
            // update user if anything is different.
//            if ( array_diff( (array) $data, (array) $user ) )
            {
                $user->name        = $data->name;
                $user->email       = $data->email;
                $user->provider_id = $data->id;
                $user->provider    = $provider;
                $user->uuid        = $data->id;
                $user->avatar      = $data->avatar;
                $user->username    = $data->nickname;
                $user->save();
                \Auth::login( $user, true );

                return redirect( $this->redirectPath );
            }
        }

        $the_user = array(
            'id' => $user->id,
        );
    }

    // ------------------------------------------------------------------------

    // testing
    public function hatchbuck_landing()
    {
        die();
        $user    = \Auth::user();
        $contact = $this->_get_hatchbuck_user( $user );

        $this->_update_hatchbuck_user( $user, $contact );
    }

    // ------------------------------------------------------------------------

    /**
     * Retrieves contact information from Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    private function _get_hatchbuck_user( $user = NULL )
    {
        if ( ! $user ) return FALSE;
        $_url = 'https://api.hatchbuck.com/api/v1/';
        $name = explode( ' ', $user->name );

        $data = array(
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),

        );

        $data = json_encode($data);

        $url = $_url . 'contact/search?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( 200 != $code OR ! is_array( $result ) && isset( $result->Message ) )
        {
            return FALSE;
        }

        foreach ($result as $row)
        {

            $contactId = $row->contactId;
            foreach ($row->emails as $email)
            {
                if ( $email->address === $user->email )
                {
                    return $contactId;
                }
            }
        }

        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Updates contact information on Hatchbuck API
     * @param  object $user
     * @return string|boolean
     */
    private function _update_hatchbuck_user( $user = NULL, $contactId = NULL )
    {
        if ( ! $user ) return FALSE;

        $_url   = 'https://api.hatchbuck.com/api/v1/';
        $name   = explode( ' ', $user->name );
        $method = 'POST';

        $data = array(
            'emails' => array(
                array(
                    'address' => $user->email,
                    'type'    => 'Home',
                ),
            ),
            'status' => array(
                'name' => 'Prospect'
            ),
            'firstName' => $name[0],
            'lastName'  => $name[1],
            'company'   => $user->company,
            'addresses' => array(
                'street' => '',
                'city'   => '',
                'country' => '',
                'state'   => '',
                'zip'     => $user->zipcode,
                'type'    => 'Home',
            ),

        );

        if ( is_string( $contactId ) )
        {
            $method = 'PUT';
            $data['contactId'] = $contactId;
        }

/*
            'q8_interests8'          => $interests,
            'q9_userCategory'        => $user->user_type,
        );
*/

        $data = json_encode($data);

        $url = $_url . 'contact?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( 200 != $code )
        {
            return FALSE;
        }

        $contactId = $result->contactId;

        $url  = $_url . 'contact/' . $contactId . '/Tags?api_key=' . $this->api_key;

        $tags = array();

        $interests = str_replace( 'Book', 'I want the Book!', $user->interests );
//        $interests = str_replace( '|', PHP_EOL, $interests );
        $interests = explode( '|', $interests );
        foreach ( $interests as $value )
        {
            $value       = strtolower( $value );
            $tags[]      = ucfirst( $value );
        }

        $tags[] = $user->user_type;

        $data = array();

        foreach ($tags as $tag)
        {
            if ( '' == $tag OR ! $tag ) continue;
            $data[] = array(
                'name' => $tag,
            );
        }
        $method = 'POST';
        $data = json_encode($data);

//        $url = $_url . 'contact?api_key=' . trim( $this->api_key );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: " . strlen($data)));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $code   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            print curl_error($ch);
        } else {
            curl_close($ch);
        }

        $result = json_decode( $result );

        if ( 200 != $code OR ! is_array( $result ) && isset( $result->Message ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    // ------------------------------------------------------------------------
    //  API METHODS
    // ------------------------------------------------------------------------

    /**
     * Form Validation helper to verify that email account has not been used
     * @param  Request $request
     * @return VOID
     */
    public function unique_email(Request $request)
    {
        $user_id = null;
        if ( $request->has('user_id' ) )
        {
            $user_id = ',' . $request->get('user_id');
        }

        $rules = array(
            'email' => 'required|email|max:255|unique:users,email' . $user_id,
        );
        $data = $request->all();
        $data['email'] = urldecode( $data['email'] );
        $validator = Validator::make($data, $rules);

        if ( ! $validator->fails() ) exit(0);
        $messages  = $validator->errors();

        return \Response::make( $messages->first('email'), 400);
    }

    // ------------------------------------------------------------------------

    /**
     * Send user data to Hatchbuck CRM
     *
     * @access private
     * @param  User $user
     * @return boolean
     */
    private function _send_hatchbuck( $user = NULL, Request $request )
    {
        if ( ! $user ) return FALSE;
//        $test = $request->session()->get('user', FALSE);
//        p_dump($test);
//        die();
        \Auth::login( $user, true );
        $url       = 'https://app.hatchbuck.com/onlineForm/submit.php';
        $name      = explode( ' ', $user->name );
        $interests = array();
        if ( ! is_array( $user->interests ) )
        {
            $user->interests = array();
        }

        foreach ( $user->interests as $value )
        {
            $value       = strtolower( $value );
            $interests[] = ucfirst( $value );
        }

        $interests = str_replace( 'Book', 'I want the Book!', $interests );
        $interests = str_replace( '|', PHP_EOL, $interests );

        $user->user_type = strtolower( $user->user_type );
        $user->user_type = ucfirst( $user->user_type );

        $company = NULL;

        if ( ! empty( $user->organization ) )
        {
            $company = $user->organization;
        } elseif ( ! empty( $user->institution ) )
        {
            $company = $user->institution;
        }

        $fields = array(
            'formID'                 => '53633640131',
            'simple_spc'             => '53633640131-53633640131',
            'enable303Redirect'      => '0',
            'enableServerValidation' => '1',
            'q1_firstName1'          => isset( $name[0] ) ? $name[0] : '',
            'q3_lastName3'           => isset( $name[1] ) ? $name[1] : '',
            'q4_email'               => $user->email,
            'q5_zipcode'             => $user->zipcode,
            'q6_comments'            => $user->comment,
            'q8_interests8'          => $interests,
            'q9_userCategory'        => $user->user_type,
            'q10_company10'          => $company,
        );

        $fields_string = http_build_query( $fields );

        $cr     = curl_init();
        curl_setopt( $cr, CURLOPT_URL, $url );
        curl_setopt( $cr, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $cr, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $cr, CURLOPT_POST, 1 );
        curl_setopt( $cr, CURLOPT_POSTFIELDS, $fields_string );
        $result = curl_exec( $cr );
        curl_close($cr);

//        \Auth::login( $user, true );

        echo $result;
        exit(0);

        return TRUE;
    }


    // ------------------------------------------------------------------------
    // HELPER METHODS
    // ------------------------------------------------------------------------

    /**
     * Removes HTML tags, normalizes line endings, etc from user comments.
     * @static
     * @param  string $comment
     * @return string
     */
    static public function clean_comments( $comment = NULL )
    {
        $comment = str_replace( array( '<br>', '<br/>', '<br />' ), PHP_EOL,  $comment );
        $comment = strip_tags( $comment );

        return $comment;
    }

    // ------------------------------------------------------------------------


}

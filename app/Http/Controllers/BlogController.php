<?php
namespace App\Http\Controllers;

use App\Jobs\BlogIndexData;
use App\Http\Requests;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
//use Eusonlito\LaravelMeta\Meta as Meta;

class BlogController extends Controller
{
	public function index(Request $request)
	{
		$tag    = $request->get( 'tag' );
		$data   = $this->dispatch( new  BlogIndexData( $tag ) );
		$layout = $tag ? Tag::layout( $tag ) : 'blog.index';

//        Meta::title( 'Blog | Chronicle Ferguson');
//        Meta::set( 'description', config('blog.seo_meta_description') );

		return view($layout, $data);
	}

	// ------------------------------------------------------------------------

	public function showPost($slug, Request $request)
	{
//		        $max_size =  ini_get('post_max_size');
//        p_dump($max_size);die();

		$post = Post::with('tags')->whereSlug($slug)->firstOrFail();
		$tag  = $request->get('tag');
		if ($tag)
		{
			$tag = Tag::whereTag($tag)->firstOrFail();
		}

		$post->layout = str_replace( 'layouts.', '', $post->layout );

  		$meta_description = $post->meta_description ?: config('blog.seo_meta_description');

//        Meta::title( $post->title . ' | Chronicle Ferguson');
//        Meta::set( 'description', $meta_description );

		return view($post->layout, compact('post', 'tag'));
	}
}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Contracts\Auth\Authenticatable as Auth;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use yajra\Datatables\Datatables;
use DB;

use App\Image as Images;
use App\Users;
use Validator;
use Intervention\Image\ImageManagerStatic as Img;

class GalleryController extends Controller
{
    use EntrustUserTrait;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * On successful creation/update of item, path to redirect to.
     * @access private
     * @var string
     */
    private $redirectTo  = '/admin/images';

    /**
     * Max File size in bytes to allow uploads
     * @access private
     * @var integer
     */
    private $maxFileSize = 10240;

    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
/*
        if ( ! $user->hasRole( ['admin', 'super_admin' ] ) )
        {
            return redirect( $this->redirectTo )->with('message', 'You do not have permission to login to the admin area.' );
        }
*/

        return view('admin.gallery.index');
    }

    public function getImages()
    {
        $images = Images::select(array('images.id', 'images.file','images.name', 'images.rating', 'images.taken_at', 'images.created_at'));
//        dd($images);
//        die();
        return Datatables::of($images)
        ->edit_column('taken_at', '{{ date("m/d/Y", strtotime( $taken_at )) }}')
        ->edit_column('created_at', '{{ $created_at->format("m/d/Y") }}')
        ->edit_column('file', '<img src="/{{ str_replace("uploads/", "uploads/t_", $file ) }}"" width="50" height="50" />')
//        ->edit_column('users','{!! App\Image::find($id)->users()->lists(\'name\')[0] !!}')
        ->add_column('actions', '
            <div class="btn-group">
            <a href="{{{ URL::to(\'admin/images/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>
            <a href="{{{ URL::to(\'admin/images/\' . $id . \'\' ) }}}" class="iframe btn btn-xs btn-danger delete-image"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')
//        ->remove_column('id')
        ->make();
//             <a href="#" class="iframe btn btn-xs btn-danger " data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash-o"></i> Delete</a>

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $image = new Images;
        return view('admin.gallery.create')->with('image', $image);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Images::find($id);
        return view('admin.gallery.edit')->with('image', $image);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_id = $request->get('image_id');
        $data = $request->all();
        if ( is_numeric( $image_id ) )
        {
            // Validation //
            $validation = Validator::make( $request->all(), [
                'name'         => 'sometimes|min:3|max:100',
                'rating'       => 'sometimes|min:1|max:2',
                'email'        => 'sometimes|min:3|max:100',
                'location'     => 'sometimes|min:3|max:100',
                'date'         => 'sometimes|max:10',
                'comment'      => 'sometimes|min:3|max:1000',
                'camera'       => 'sometimes|min:3|max:100',
                'userfile'     => 'sometimes|image|mimes:jpeg,png,jpg|min:1|max:' . $this->maxFileSize
            ]);
            $img = Images::find($data['image_id']);
            $msg = 'Image updated.';
        } else
        {
            // Validation //
            $validation = Validator::make( $request->all(), [
                'name'         => 'required|min:3|max:100',
                'rating'       => 'required|min:1|max:2',
                'email'        => 'required|min:3|max:100',
                'location'     => 'required|min:3|max:100',
                'date'         => 'required|max:10',
                'comment'      => 'sometimes|min:3|max:1000',
                'camera'       => 'sometimes|min:3|max:100',
                'userfile'     => 'required|image|mimes:jpeg,png,jpg|min:1|max:' . $this->maxFileSize
            ]);
            $img = new Images;
            $msg = 'Uploaded new image.';
        }
        // Check if it fails //
        if( $validation->fails() )
        {
            return redirect()->back()->withInput()->with('errors', $validation->errors() );
        }

        $large_size  = \Config::get('gallery.large_size');
        $medium_size = \Config::get('gallery.medium_size');
        $small_size  = \Config::get('gallery.small_size');

        // if user choose a file, replace the old one //
        if( $request->hasFile('userfile') )
        {
            $file             = $request->file('userfile');
            $destination_path = self::get_upload_path();

            $filename         = str_random(6).'_'.$file->getClientOriginalName();
            $mid_size         = 'm_' . $filename;
            $thumb            = 't_' . $filename;
            $file->move( $destination_path, $filename );
            $img->file        = $destination_path . $filename;

            Img::make( $img->file )->orientate()->fit(  $large_size[0] , $large_size[1]  )->save( $destination_path . $filename );
            Img::make( $img->file )->orientate()->fit( $medium_size[0] , $medium_size[1] )->save( $destination_path . $mid_size );
            Img::make( $img->file )->orientate()->fit(  $small_size[0] , $small_size[1]  )->save( $destination_path . $thumb );


            // save image data into database //
            $img->file        = $destination_path . $filename;
            $data = Img::make( $img->file )->exif();

            if ( isset( $data['Model'] ) && strlen( $data['Model'] ) > 0 )
            {
                $img->camera = $data['Model'];
            } else {
                $img->camera = $request->get('camera');
            }
        }

        $img->name     = $request->get('name');
        $img->rating   = $request->get('rating');
        $img->email    = $request->get('email');
        $img->location = $request->get('location');
//        $img->date     = $request->get('date');
        $img->date     = date('m/d/Y', strtotime( $request->date ) );
        $img->taken_at = date('Y-m-d', strtotime( $request->date ) );
        $img->comment  = $request->get('comment');
//        $img->camera   = $request->get('camera');
        $img->save();
        return redirect( $this->redirectTo )->with('success', $msg );

    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Images::find($id);
        $files = self::get_image_names( $image->file );
        foreach( $files as $file )
        {
            \File::delete($file);
            if ( \File::exists( $file ) )
            {
                die('file not deleted properly');
            }
        }

        $likes = \App\ImageLike::where('image_id', $id)->delete();
        $comments = \App\Comment::where('image_id', $id)->delete();
        $image->delete();

//        return redirect( $this->redirectTo )->with('message','You just deleted an image!');

    }

    // ------------------------------------------------------------------------

    public function api_bulk_resize()
    {
        if ( ! \Auth::user()->hasRole( [ 'super_admin' ] ) )
        {
            return redirect( $this->redirectTo )->with('message', 'You do not have permission to do that.' );
        }



        $size   = self::get_image_sizes();
//        $images = Images::where('name','jamica')->get();
//        $images = [ Images::find(339) ];
//        $images[] = Images::find(211);
//        $images = [ $images ];

        $images = Images::get();
        $i=0;
        foreach ($images as $img)
        {
            $i++;
            $thumb = str_replace( 'uploads/', 'uploads/m_', $img->file );
            echo 'Sharpening thumbnail (#'.$i.')<br>File : ' . $thumb;
            Img::make( $thumb )->sharpen(15)->orientate()->save();

continue;

            $data = Img::make( $img->file )->iptc();
            $data2 = Img::make( $img->file )->exif();
//$data = serialize( $data );

p_dump($data);
//$data = unserialize($data);
p_dump($data2);
die();
            $destination_path = self::get_upload_path();
            $filename = str_replace( $destination_path, '', $img->file );
//            $start = substr( $filename, 0, 6 );
//            $end   = substr( $filename, 7 );


            $mid_size         = 'm_' . $filename;
            $thumb            = 't_' . $filename;
            //$img->file        = $destination_path . $filename;
//p_dump($start, $end);die();
            Img::make( $img->file )->orientate()->save();

echo 'processing ' . $img->file . '<br>';

            Img::make( $destination_path . $mid_size )->orientate()->save();
echo 'processing ' . $destination_path . $mid_size . '<br>';
            Img::make( $destination_path . $thumb )->orientate()->save();
echo 'processing ' . $destination_path . $thumb . '<br>';
            continue;
//            Img::make($img->file)->orientate()->fit(  $large_size[0] , $large_size[1]  )->save( $destination_path . $filename );
//            Img::make($img->file)->orientate()->fit( $medium_size[0] , $medium_size[1] )->save( $destination_path . $mid_size );
//            Img::make($img->file)->orientate()->fit(  $small_size[0] , $small_size[1]  )->save( $destination_path . $thumb );

//            $file = $img->file;
//            $path = self::get_upload_path();

            echo 'Date : ' . $img->date . '<br>';
            $img->date = date('m/d/Y', strtotime( $img->date ) );
            $img->taken_at = date('Y-m-d', strtotime( $img->date ) );
            echo 'New Date : ' . $img->taken_at . '<br>';
            $img->save();
/*
            Img::make($file)->fit(  $size['large_size'][0] , $size['large_size'][1]  )->save( $file_names['full_size'] );
            Img::make($file)->fit( $size['medium_size'][0] , $size['medium_size'][1] )->save( $file_names['mid_size'] );
            Img::make($file)->fit(  $size['small_size'][0] , $size['small_size'][1]  )->save( $file_names['thumb'] );
*/
        }
        die();
        return redirect( $this->redirectTo )->with('message','Successfully resized all images!');
    }

    // ------------------------------------------------------------------------
    // HELPER METHODS
    // ------------------------------------------------------------------------

    public static function get_image_names( $file )
    {
        $path = self::get_upload_path();

        $mid_size = $path . 'm_';
        $thumb    = $path . 't_';
        return [
          'full_size' => $file,
          'mid_size'  => str_replace( $path, $mid_size, $file ),
          'thumb'     => str_replace( $path, $thumb, $file ),
        ];
    }

    // ------------------------------------------------------------------------

    public static function get_upload_path()
    {
        return 'uploads/';
        $path = \Config::get('blog.uploads');
        return $path['webpath'];
    }

    // ------------------------------------------------------------------------

    public static function get_image_sizes()
    {
        $sizes = [];
        $sizes['large_size']  = \Config::get('gallery.large_size');
        $sizes['medium_size'] = \Config::get('gallery.medium_size');
        $sizes['small_size']  = \Config::get('gallery.small_size');
        return $sizes;
    }


    public function getCameraSuggestions( Request $request )
    {
        $cameras = Images::lists('camera');
        $query   = strtolower( urldecode( $request->get('q') ) );

        $array   = array();

        foreach ($cameras as $key => $value)
        {
            $key = strtolower( $value );
            if ( in_array( $value, $array ) OR in_array( $key, $array ) ) continue;
            $array["{$key}"] = $value;
        }

        $cameras = array_where($array, function ($key, $value) use ( $query ) {
            return str_contains( $value, $query ) or str_contains( $key, $query );
        });

        $array = [];
        foreach ($cameras as $key => $value)
        {
            $array[]['camera'] = $value;
        }

        die( json_encode( $array ) );
    }

    // ------------------------------------------------------------------------

}

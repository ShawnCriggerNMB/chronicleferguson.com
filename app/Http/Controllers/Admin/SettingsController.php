<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Eusonlito\LaravelMeta\Meta;

class SettingsController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = \Config::get('blog');

        $settings['bad_words'] = str_replace('|', ',', $settings['bad_words']);
        return view('admin.settings.index')->with('settings', $settings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $config = new \Larapack\ConfigWriter\Repository('blog');
        $config->set('title', $request->get('title') );
        $config->set('seo_meta_description', $request->get('seo_meta_description') );
        $config->set('ga_analytics', $request->get('ga_analytics') );

        $config->set('uploads', array(
            'storage' => 'local',
            'webpath' => $request->uploads,
        ) );

        $config->set('social_links', array(
            'facebook'  => $request->facebook,
            'twitter'   => $request->twitter,
            'instagram' => $request->instagram,
        ) );

        $bad_words = $request->get('bad_words');
        $bad_words = str_replace( ', ', '|', $bad_words );
        $bad_words = str_replace( ',', '|', $bad_words );

        $config->set('bad_words', $bad_words);

        $config->set('email_from', $request->email_from);
        $config->set('email_name', $request->email_name);
//        $config->set('contact_message', $request->contact_message);
        $config->set('contact_subject', $request->contact_subject);

        $config->save(); // save those settings to the config file once done editing
        return \Redirect::to('admin/settings')->with('success', 'Settings updated!' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    // ------------------------------------------------------------------------

    public function run_migration(Request $request)
    {
        if( ! $request->user()->hasRole( [ 'super_admin' ] ) )
        {
            abort( 404 );
        }
        \Artisan::call('migrate');
    }

    // ------------------------------------------------------------------------

    public function phpinfo(Request $request)
    {
        if( ! $request->user()->hasRole( [ 'super_admin' ] ) )
        {
            abort( 404 );
        }
        return view('admin.settings.phpinfo');
    }

}

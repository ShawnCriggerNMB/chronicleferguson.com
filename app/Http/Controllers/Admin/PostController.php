<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\PostFormFields;
use App\Http\Requests;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;
use App\Post;
use Michelf\MarkdownExtra;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Img;
use yajra\Datatables\Datatables;
use DB;

class PostController extends Controller
{
	/**
	* Display a listing of the posts.
	*/
	public function index()
	{

		return view('admin.post.index'); //->withPosts( Post::all() );
	}

	// ------------------------------------------------------------------------

    public function getPosts()
    {
        $posts = Post::select(array('posts.id', 'posts.title', 'posts.slug', 'posts.is_draft', 'users.name', 'posts.published_at' ))->leftJoin('users', 'users.id', '=', 'posts.author_id');

        return Datatables::of($posts)
        ->edit_column('published_at', '{{ $published_at->format("j-M-y g:ia") }}')
        ->edit_column('is_draft', '{{ is_draft( $is_draft ) }}')
        ->add_column('actions', '
            <div class="btn-group">
            <a href="{{{ URL::to(\'admin/post/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>
            <a href="{{{ URL::to(\'admin/post/\' . $id . \'/destroy\' ) }}}" class="iframe btn btn-xs btn-danger delete-post"><i class="fa fa-trash-o"></i> Delete</a>
            <a href="{{{ URL::to(\'blog/\' . $slug . \'\' ) }}}" class="iframe btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a>
            </div>
            ')
        ->remove_column('id')
        ->remove_column('slug')
        ->remove_column('author_id')
        ->make();
	}

	// ------------------------------------------------------------------------

	/**
	* Show the new post form
	*/
	public function create()
	{
		$data = $this->dispatch(new PostFormFields());

		return view('admin.post.edit', $data)->with('mode', 'create');
	}

	// ------------------------------------------------------------------------

	/**
	* Store a newly created Post
	*
	* @param PostCreateRequest $request
	*/
	public function store(PostCreateRequest $request)
	{
		$post = Post::create( $request->postFillData() );
		$post->syncTags( $request->get('tags', [] ) );

		return redirect()->route('admin.post.index')->withSuccess('New Post Successfully Created.');
	}

	// ------------------------------------------------------------------------

	/**
	* Show the post edit form
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
	$data = $this->dispatch(new PostFormFields($id));
	return view('admin.post.edit', $data)->with('mode', 'edit');
	}

	// ------------------------------------------------------------------------

	/**
	* Update the Post
	*
	* @param PostUpdateRequest $request
	* @param int  $id
	*/
	public function update(PostUpdateRequest $request, $id)
	{
		$post = Post::findOrFail($id);
		$post->fill( $request->postFillData() );
		$post->save();
		$post->syncTags( $request->get('tags', [] ) );
/*
		if ( 'continue' === $request->action )
		{
			return redirect()->back()->withSuccess('Post saved.');
		}
*/

//		p_dump( $request->all() );die();
		return redirect()->route('admin.post.index')->withSuccess('Post saved.');
	}

	// ------------------------------------------------------------------------

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function destroy($id)
	{
		$post = Post::findOrFail($id);
		$post->tags()->detach();
		$post->delete();

		return redirect()->route('admin.post.index')->withSuccess('Post deleted.');
	}

	// ------------------------------------------------------------------------

	public function getPreview()
	{
		$content = \Request::input('content');
		echo MarkdownExtra::defaultTransform( $content );
	}
	// ------------------------------------------------------------------------

	public function handleUpload(Request $request)
	{
		if ( ! $request->hasFile( 'file0' ) )
		{
			die('bad fileupload');
			\Response::json( 500, 'File not uploaded' );
			exit(0);
		}
		$large_size = \Config::get('gallery.large_size');
		$uploaded   = array();
        $dest_path  = '/uploads/blog/';
        $path       = public_path() . $dest_path;

		for ($i=0; $i < 10; $i++)
		{
			if ( ! $request->hasFile("file{$i}" ) ) continue;
			$file = $request->file("file{$i}");
			if ( ! $file->isValid() ) continue;

			$filename = str_replace( ' ', '-', $file->getClientOriginalName() );
	        $filename = str_random(6) . '_' . strtolower( $filename );

	        try {
	        	$file->move( $path, $filename );
	        } catch (Exception $e) {
	        	die("Couldn't rename file");
	        }

//	        Img::make( $file->getRealPath() )->fit(  $large_size[0] , $large_size[1]  )->save( $path . $filename );
	        $uploaded[] = $dest_path . urlencode( $filename );
	    }

	    return response()->json( $uploaded );

	}
	// ------------------------------------------------------------------------

}

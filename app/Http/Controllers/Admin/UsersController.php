<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User as User;
use App\Role;
use App\Permission;
use yajra\Datatables\Datatables;
use DB;

class UsersController extends Controller
{
    /**
     * User Model
     * @var User
     */
    protected $user;
    /**
     * Role Model
     * @var Role
     */
    protected $role;
    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    // ------------------------------------------------------------------------

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    // ------------------------------------------------------------------------

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $users = User::select(array('users.id', 'users.name','users.email','roles.display_name', 'users.created_at', 'users.updated_at', 'role_user.role_id'))
        ->leftJoin('role_user', 'role_user.role_id', '=', 'roles.id')
        ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id');
dump($users);
die();
*/
        return view('admin.users.index');
    }

    // ------------------------------------------------------------------------

    /**
     * Pulls Users from Database for Datatables display
     * @return STDOUT
     */
    public function getUsers()
    {
        $fields = [ 'users.id', 'users.name','users.email', 'roles.name as role_name', 'users.created_at', 'users.updated_at' ];

        $users = User::leftJoin('role_user', 'role_user.user_id', '=', 'users.id')->
                        leftJoin('roles', 'roles.id', '=', 'role_user.role_id')->
                        select( $fields );

        return Datatables::of($users)
//        ->addColumn('check', '<input type="checkbox" name="selected_users[]" value="{{ $id }}">')
        ->edit_column('created_at', '{{ $created_at->format("Y-m-d h:i:s") }}')
        ->edit_column('updated_at', '{{ $updated_at->format("Y-m-d h:i:s") }}')
        ->add_column('actions', '
            <div class="btn-group">
            <a href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>
            @if($name == \'admin\')
            @else
            <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger delete-user"><i class="fa fa-trash-o"></i> Delete</a>
            @endif
            </div>
            ')
//        ->remove_column('id')
        ->remove_column('role_id')
        ->make();
    }


    // ------------------------------------------------------------------------
    // @TODO: Finish below methods.
    // ------------------------------------------------------------------------

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->all();
        $permissions = $this->permission->all();
        // Selected groups
        $selectedRoles = \Input::get('roles' );
        // Selected permissions
        $selectedPermissions = \Input::old('permissions', array());
//dd($selectedRoles);die();
        $user_roles = \Input::old('roles');
        $user = new User;
        // Title
        $title = 'Create User';
//            $title = Lang::get('admin/users/title.user_update');
        // mode
        $mode = 'create';
		return \View::make('admin/users/create_edit', compact('user', 'user_roles', 'roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    // ------------------------------------------------------------------------

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		\Validator::extend('alpha_spaces', function($attribute, $value)
		{
		    return preg_match('/^[\pL\s]+$/u', $value);
		});

    	$user_id  = (int) \Input::get('user_id');

    	if ( $user_id > 0 )
    	{
	    	$user     = \App\User::find( $user_id );

	        $rules    = array(
	            'name'                  => 'required|alpha_spaces', //|unique:users,name,'.$user_id,
	            'email'                 => 'required|email|unique:users,email,'.$user_id,
	            'password'              => 'min:4|confirmed',
	            'password_confirmation' => 'min:4',
	        );
	        $redirect = 'admin/users/' . $user_id . '/edit';
	        $verb = 'edited';
	    } else {
	    	// create user logic.
	    	$user     = new \App\User;

	        $rules    = array(
	            'name'                  => 'required|alpha_spaces', //|unique:users,name',
	            'email'                 => 'required|email|unique:users,email',
	            'password'              => 'required|min:4|confirmed',
	            'password_confirmation' => 'required|min:4',
	        );
	        $redirect = 'admin/users/create';
	        $verb = 'created';
	    }

        // Validate the inputs
        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->passes())
        {
//            p_dump($_POST);die();
//            $oldUser              = clone $user;
            $user->name           = \Input::get( 'name' );
            $user->email          = \Input::get( 'email' );
//            $user->confirmed      = \Input::get( 'confirm' );
            $password             = \Input::get( 'password' );
            $passwordConfirmation = \Input::get( 'password_confirmation' );
            if ($password)
            {
                $user->password = bcrypt ( $password );
//                $user->password_confirmation = $passwordConfirmation;
            }
            $user->comments  = \Input::Get( 'comment' );
            $user->user_type = \Input::Get( 'user_type' );

            $user->zipcode   = \Input::Get( 'zipcode' );
            $user->phone     = \Input::Get( 'phone' );
            $user->institution  = \Input::Get( 'institution' );
            $user->organization = \Input::Get( 'organization' );

            $interests = \Input::get('interests');
            if ( is_string( $interests ) )
            {
                $user->interests = $interests;
            } elseif ( is_array( $interests ) )
            {
                $user->interests = implode( '|', $interests );
            }

            /*
            if($user->confirmed == null)
            {
                $user->confirmed = $oldUser->confirmed;
            }
            */
            // Save if valid. Password field will be hashed before save
            $user->save();

            //$user->attachRole(\Input::get( 'roles' )); // parameter can be an Role object, array, or id

            // Save roles. Handles updating.
            $user->saveRoles(\Input::get( 'roles' ));
        } else {
            return \Redirect::to( $redirect )->withErrors($validator);
        }
        // if error found on model
        if(empty($user->errors)) {
            // Redirect to the new user page
            return \Redirect::to( $redirect )->with('success', 'Successfully ' . $verb . ' user');
        } else {
            return \Redirect::to( $redirect )
                ->withInput()
                ->withErrors($user->errors);
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        if ( $user->id )
        {
            $roles = $this->role->all();
            $user_roles = DB::table('role_user')->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')->where('user_id', $id)->get();
            $permissions = $this->permission->all();
            $user_roles = array_shift( $user_roles );
            $user->interests = explode( '|', $user->interests );

            // Title
            $title = 'Update';
//            $title = Lang::get('admin/users/title.user_update');
            // mode
            $mode = 'edit';
            return \View::make('admin/users/create_edit', compact('user', 'user_roles', 'roles', 'title', 'mode'));
        }
        else
        {
            $user->interests = array();
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
//        return view('admin.users.create_edit');
    }

    // ------------------------------------------------------------------------

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {

        $current = \Auth::user()->id;

        if ( ! is_object( $user ) && is_numeric( $user ) )
        {
            $user = User::find($id)->get();
        }

        if ( ! is_object( $user ) )
        {
            return Redirect::to('admin/users')->with('error', 'User does not exist!' );
            exit(0);
        }

        if ( (int) $current === (int) $user->id )
        {
            return Redirect::to('admin/users')->with('error', 'You can not delete your account!');
            exit(0);
        }

        $user->delete();
    }

    // ------------------------------------------------------------------------
    // API FUNCTIONS
    // ------------------------------------------------------------------------


    public function getUsersSuggest(Request $request)
    {
        $q = $request->get('q');

        $users  = DB::table('users')->
                        select('name', 'email')->
                        where('name', 'like', $q . '%')->
                        orWhere('email', 'like', $q . '%')->
                        get();
        $users = json_encode( $users );
        die( $users );
    }


}

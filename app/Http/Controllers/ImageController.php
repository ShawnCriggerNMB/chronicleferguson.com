<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Image;
use App\ImageLike;
use App\Comment as Comments;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Img;
use Eusonlito\LaravelMeta\Meta as Meta;

class ImageController extends Controller
{
//	use User;
	// ------------------------------------------------------------------------

	/**
	 * On successful creation/update of item, path to redirect to.
	 * @access private
	 * @var string
	 */
	private $redirectTo  = '/gallery';

	/**
	 * Max File size in bytes to allow uploads
	 * @access private
	 * @var integer
	 */
	private $maxFileSize = 10240;

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
     * Displays main gallery page
     * @param  Request $request
     * @return STDOUT
     */
	public function index(Request $request)
	{
        $data      = array();
        $hatchbuck = $request->session()->get( 'hatchbuck', FALSE );
        $crm_user  = $request->session()->get( 'user', FALSE );

        $sort      = $request->input('sort');
        $limit     = $request->input('limit');

        if ( ! $limit )
        {
            $limit = 12;
        }

        if ( TRUE === $hatchbuck && FALSE != $crm_user)
        {
            \Auth::login( $crm_user );
        }
        $data['limit']       = (int) $limit;
        $data['sort']        = self::get_gadget_sorts( TRUE );
        $data['sort_string'] = self::get_gadget_sorts();
        $data['large_size']  = \Config::get('gallery.large_size');
        $data['medium_size'] = \Config::get('gallery.medium_size');
        $data['small_size']  = \Config::get('gallery.small_size');

        $temp              = Image::lists('location');
        $data['locations'] = self::build_options( $temp );

        $temp              = Image::lists('camera');
        $data['cameras']   = self::build_options( $temp );

        $temp              = Image::lists('name');
        $data['photographers'] = self::build_options( $temp );

        $images = Image::AddSort( $sort )->paginate( $limit );
		foreach ($images as &$image)
        {
            $image->thumb = str_replace('uploads/' , 'uploads/m_', $image->file);
			$image->small = str_replace('uploads/' , 'uploads/t_', $image->file);
		}

//        Meta::title('Photo Gallery | Chronicle Ferguson');

		return view('images.list')->with('images', $images)->with($data);
	}

    // ------------------------------------------------------------------------

    public static function build_options( $array = array() )
    {
        $options = [];
        foreach ($array as $value)
        {
            if ( in_array( $value, $options ) ) continue;
            $url = urlencode( $value );
            $options["{$url}"] = $value;
        }
        return $options;
    }

    // ------------------------------------------------------------------------

    /**
     * Displays Image Upload form
     * @return STDOUT
     */
	public function create()
	{
//        Meta::title('Upload Photo | Chronicle Ferguson');
		return view('images.create');
	}

    // ------------------------------------------------------------------------

    /**
     * Displays Gallery Image using $id value to pull the image.
     * @param  integer  $id
     * @param  Request $request
     * @return STDOUT
     */
	public function show($id, Request $request)
	{
		$sort   = $request->input('sort');

		$images = Image::AddSort( $sort )->get();
        $count  = count( $images );

        $success = false;
        for ($i=0; $i < $count; $i++)
        {
            if ( $images[ $i ]->id != $id ) continue;
            $image = $images[$i];
            // Previous image
            if ( 0 === $i )
            {
                $image->prev = FALSE;
            } else {
                $image->prev = $images[$i-1]->url();
            }
            // Next image
            if ( $i == $count-1 )
            {
                $image->next = FALSE;
            } else {
                $image->next = $images[$i+1]->url();
            }
            $success = true;
        }

        $image->profile_image = self::profile_image( $image->email );

        $image->likes = \DB::table('image_likes')->where('image_id', $id)->count();
        $image->total_comments = \DB::table('comments')->where('image_id', $id)->count();
//        $comments = Comments::where('image_id', $id)->get();
        $image->liked = 0;
        $comments = $image->comments();
        if ( \Auth::check() )
        {
            $user_id = \Auth::user()->id;
            $image->liked = \DB::table('image_likes')->where('image_id', $id)->where('user_id', $user_id)->count();
        }

        $image->posted_at = self::posted_at( $image->updated_at );

        // set comment updated at to relative time string
        foreach ($comments as &$comment)
        {
            if ( ! isset( $comment->updated_at ) ) continue;

            $comment->posted_at     = self::posted_at( $comment->updated_at );
            $comment->profile_image = self::profile_image( $comment->user, 35 );
            $comment->name = $comment->user->name;
        }

//        Meta::title('Photo | Chronicle Ferguson');
//      Meta::meta( 'image', asset($image->file) );
        $temp = substr( $image->comment, 0, 100 );
//        Meta::set( 'description', $temp . '...' );
        $data = array();

        $data['sort']  = self::get_gadget_sorts( TRUE );
        $data['sort_string'] = self::get_gadget_sorts();
        $data['limit'] = (int) $request->get('limit');
/*
        if ( $data['limit'] > 12 )
        {
            if ( ! $data['sort'] )
            {
                $data['sort'] .= '?';
            } else {
                $data['sort'] .= '&';
            }

            $data['sort'] .= 'limit=' . $data['limit'];
        }
*/
        $image->prev .= $data['sort'];
        $image->next .= $data['sort'];

		return view('images.show')->with('image', $image)->with('comments', $comments)->with($data);
	}

    // ------------------------------------------------------------------------

    /**
     * Get results by page
     *
     * @param int $page
     * @param int $limit
     * @return StdClass
     */
    public function getByPage($page = 1, $limit = 10)
    {
      $results = (object) array();
      $results->page = $page;
      $results->limit = $limit;
      $results->totalItems = 0;
      $results->items = array();

      $this->model = new Image;

      $imgs = $this->model->skip($limit * ($page - 1))->take($limit)->get();

      $results->totalItems = $this->model->count();
      $results->items = $imgs->all();

      return $results;
    }

    // ------------------------------------------------------------------------

    static public function get_gadget_sorts( $add_limit = FALSE )
    {
        $sort_string  = '?';

        $sorts        = [];
        $sort         = \Request::get('sort');
        $camera       = \Request::get('camera');
        $location     = \Request::get('location');
        $chronology   = \Request::get('chronology');
        $photographer = \Request::get('photographer');
        $limit        = (int) \Request::get('limit');

        if ( TRUE === $add_limit && $limit > 0 )
        {
            $sorts['limit'] = $limit;
        }

        //if ( ! $sort ) return $sort_string;

        switch ( $sort )
        {
            case 'chronology':
                $dir          = \Input::get('chronology');
                $sorts['sort'] = 'chronology';
                $sorts['chronology'] = $dir;
                break;
            case 'photographer':
                if ( NULL === $photographer )
                {
                    $sorts['sort'] = 'photographer';
                    break;
                }
                $sorts['sort'] = 'photographer';
                $sorts['photographer'] = $photographer;
                break;
            case 'both':
                $sort_string .= 'sort=both&amp;location=' . $location . '&amp;camera=' . $camera;
                return $sort_string;
            case 'location':
                $sorts['sort'] = 'location';
                $sorts['location'] = $location;
                break;
            case 'camera':
                $sorts['sort'] = 'camera';
                $sorts['camera'] = $camera;
                break;
            case 'allDates' :
                $sorts['sort'] = 'allDates';
                break;
            case 'dates' :
                $start  = \Input::get('start_date');
                $end    = \Input::get('end_date');
                $sorts['sort'] = 'dates';
                $sorts['start_date'] = $start;
                $sorts['end_date'] = $end;
                break;
            default:
                break;
        }

        if ( count( $sorts ) === 0 ) return;

        return '?' . http_build_query( $sorts );

        return $sort_string;
    }

    // ------------------------------------------------------------------------

    /**
     * Store a newly created resource in storage.
     *
     * @param  \app\Http\Request\ImageUploadRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $is_logged = \Auth::check();

        $rules = [
            'name'         => 'required|min:3|max:100',
            'email'        => 'required|email|min:3|max:100',
            'location.0'   => 'required|min:3|max:100',
            'date.0'       => 'required|max:10|date_format:"m/d/Y"',
            'comment'      => 'sometimes|min:3|max:1000',
            'userfile.0'   => 'required|image|mimes:jpeg,png,jpg|min:1|max:' . $this->maxFileSize,
        ];

        if ( $is_logged )
        {
            unset( $rules['name'], $rules['email'] );
            $user = array(
                'name'  => \Auth::user()->name,
                'email' => \Auth::user()->email,
            );
        } else {
            $user = array(
                'name'  => $request->get('name'),
                'email' => $request->get('email'),
            );
        }
        $messsages = array();
        $messsages["userfile.0.required"] = "The image file is required.";
        $messsages["date.0.required"]     = "The date field is required.";
        $messsages["date.0.date_format"]  = "The date field does not match the format mm/dd/YYYY.";
        $messsages["location.0.required"] = "The location field is required.";

        $messsages["date.0.max"]     = "The date field can only be :max chars long";
        $messsages["location.0.min"] = "The location field has to be atleast :min chars long";
        $messsages["location.0.max"] = "The location field can not be :max chars long, please shorten your location input.";

        $messsages["userfile.0.mimes"] = "The image file must be a file of type: :values.";
        $messsages["userfile.0.max"]   = "The image file may not be greater than :max kilobytes.";
        $messsages["date.0.max"]       = 'The date field can only be :max chars long';
        $messsages["location.0.min"]   = "The location field has to be :min chars long";
        $messsages["location.0.min"]   = "The location field has to be :min chars long";

        $z = 1;
        for ($i=1; $i < 100; $i++)
        {
            $z++;
            $rules['userfile.' . $i] = 'sometimes|image|mimes:jpeg,png,jpg|min:1|max:' . $this->maxFileSize;
            $messsages["userfile.{$i}.image"] = "Image file #{$z} must be a image.";
            $messsages["userfile.{$i}.mimes"] = "Image file #{$z} must be a file of type: :values.";
            $messsages["userfile.{$i}.max"]   = "Image file #{$z} may not be greater than :max kilobytes.";
        }

        $z = 1;
        for ($i=1; $i < 100; $i++)
        {
            $z++;
            if ( ! $request->hasFile('userfile.' . $i ) ) continue;
            $messsages["date.{$i}.max"]     = "The date field #{$z} can only be :max chars long";
            $messsages["date.{$i}.date_format"]  = "The date field #{$z} does not match the format mm/dd/YYYY.";
            $messsages["location.{$i}.min"] = "The location field #{$z} has to be atleast :min chars long";
            $messsages["location.{$i}.max"] = "The location field #{$z} can not be :max chars long";

            $rules['date.'.$i]     = 'sometimes|max:10|date_format:"m/d/Y"';
            $rules['location.'.$i] = 'sometimes|min:3|max:100';
        }


        $validation = Validator::make($request->all(), $rules, $messsages );

        // Check if it fails //
        if( $validation->fails() )
        {
            return redirect()->back()->withInput()->with('errors', $validation->errors() );
        }
        $large_size  = \Config::get('gallery.large_size');
        $medium_size = \Config::get('gallery.medium_size');
        $small_size  = \Config::get('gallery.small_size');

        $files = $request->file('userfile');

        $_location    = $request->input( 'location.0' );
        $_date        = $request->input( 'date.0' );
        $_date        = date("m/d/Y", strtotime( $_date ) );
        $_taken       = date("Y-m-d", strtotime( $_date ) );
        $i            = 0;

        foreach ( $files as $the_file )
        {
        	$image = new Image;

            $file             = $the_file;

            $destination_path = 'uploads/';

            $filename         = str_random(6).'_'.$file->getClientOriginalName();
            $full_size        = 'f_' . $filename;
            $mid_size         = 'm_' . $filename;
            $thumb            = 't_' . $filename;

            $file->move( $destination_path, $filename );
            $image->file      = $destination_path . $filename;

            Img::make($image->file)->orientate()->fit(  $large_size[0] , $large_size[1]  )->save( $destination_path . $full_size );
            Img::make($image->file)->orientate()->fit( $medium_size[0] , $medium_size[1] )->save( $destination_path . $mid_size );
            Img::make($image->file)->orientate()->fit(  $small_size[0] , $small_size[1]  )->save( $destination_path . $thumb );


	        // save image data into database //
	        $image->file        = $destination_path . $filename;
	        $image->rating      = 0;

            if ( ! $request->input( 'location.' . $i ) )
            {
                $image->location = $_location;
            } else {
                $image->location = $request->input( 'location.' . $i );
            }

            if ( ! $request->input( 'date.' . $i ) )
            {
                $image->date     = $_date;
                $image->taken_at = $_taken;
            } else {
                $image->date     = $request->input( 'date.' . $i );
                $image->date     = date("m/d/Y", strtotime( $image->date ) );
                $image->taken_at = date("Y-m-d", strtotime( $image->date ) );
            }

            if ( ! $is_logged )
            {
                $image->name        = $request->input('name');
                $image->email       = $request->input('email');
            } else {
                $image->name = \Auth::user()->name;
                $image->email = \Auth::user()->email;
            }


            $image->comment      = self::clean_comments( $request->get('comment') );
	        $image->camera      = 'Not specified';

			$data = Img::make( $image->file )->exif();

			if ( isset( $data['Model'] ) )
			{
				$image->camera = $data['Model'];
			}

	        $image->save();
            $i++;
	    }

        $user = (object) $user;
        $this->_send_hatchbuck( $user );
/*
        if ( ! class_exists( 'Hatchbuck' ) )
        {

        } else {

            $hatch = new Hatchbuck();
            $contact = $hatch->_get_hatchbuck_user( $user );
            $tags = array(
                'Photographer',
            );
            $this->_update_hatchbuck_user( $tags, $contact );

        }
*/
        return redirect( $this->redirectTo )->with('message','You just uploaded an image!');
    }

    // ------------------------------------------------------------------------
    // JAVASCRIPT METHODS
    // ------------------------------------------------------------------------

    /**
     * Accessed via AJAX to like the current image.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageLike  $model
     * @return \Illuminate\Http\Response
     */
    public function like_image(Request $request, ImageLike $model)
    {
        $id = $request->image_id;
        $ip = fetch_users_ip();
        $json = array( 'status' => 'error', 'count' => 0 );

        if ( ! \Auth::check() )
        {
            $json  = array( 'status' => 'error', 'count' => 0, 'message' => 'You must be logged in to like a image!' );
            return \Response::json( $json );
        }

        $user_id  = (int) \Auth::user()->id;
        $json  = array( 'status' => 'error', 'count' => 0, 'message' => 'You have already liked this image!' );
        $liked = $model->where('user_ip', $ip)->where('image_id', $id)->first();

        if ( isset( $user_id ) && ( FALSE === $liked OR NULL === $liked ) )
        {

            $like = new ImageLike;
            $like->image_id = (int) $request->image_id;
            $like->user_ip  = $ip;
            $like->user_id  = (int) $user_id;
            $like->save();
            $count = $model->where('image_id', $id)->count();
            $json = array( 'status' => 'success', 'count' => $count, 'message' => 'Liked image!' );
        }
        return \Response::json( $json );
    }

    // ------------------------------------------------------------------------

    /**
     * Accessed via AJAX to unlike the current image.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageLike  $model
     * @return \Illuminate\Http\Response
     */
    public function unlike_image(Request $request, ImageLike $model)
    {
        $id = $request->image_id;
        $ip = fetch_users_ip();

        $json  = array( 'status' => 'error', 'count' => 0, 'message' => 'You must be logged in to un-like a image!' );
        $liked = FALSE;
        if ( \Auth::check() )
        {
            $user_id = \Auth::user()->id;

            $json  = array( 'status' => 'error', 'count' => 0, 'message' => 'You have not already liked this image!' );
            $liked = $model->where('user_id', $user_id)->where('image_id', $id)->first();
        }

        if ( isset( $user_id ) && $liked )
        {
            $like = $model->where('image_id', $id)->where('user_id', $user_id)->delete();
            $count = $model->where('image_id', $id)->count();
            $json = array( 'status' => 'success', 'count' => $count, 'message' => 'Unliked image!' );
        }
        return \Response::json( $json );
    }

    // ------------------------------------------------------------------------

    /**
     * Accessed via AJAX to remove the posted comment.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function remove_comment(Request $request)
    {
        $comment_id = $request->comment_id;
        $image_id   = $request->image_id;

        $json  = array( 'status' => 'error', 'message' => 'You must be logged in to delete a comment!' );
        $comment = FALSE;

        if ( \Auth::check() )
        {
            $user_id = \Auth::user()->id;

            $json  = array( 'status' => 'error', 'message' => 'Comment does not exist!' );
            $comment = Comments::where('id', $comment_id)->where('user_id', $user_id)->where('image_id', $image_id)->first();
        }

        if ( isset( $user_id ) && $comment )
        {
            $delete = Comments::where('id', $comment_id)->where('image_id', $image_id)->where('user_id', $user_id)->delete();
            $json = array( 'status' => 'success', 'message' => 'Comment deleted.' );
        }
        return \Response::json( $json );
    }

    // ------------------------------------------------------------------------

    /**
     * Posts comment into Database.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment $model
     * @return \Illuminate\Http\Response
     */
    public function post_comment(Request $request)
    {
        if ( ! Auth::check() )
        {
            $json  = array( 'status' => 'error', 'message' => 'You must be logged in to post a comment!' );
            return \Response::json( $json );
            die();
        }

        // Declare the rules for the form validation
        $rules = array(
            'comment' => 'required|min:3'
        );
        // Validate the inputs
        $validator = \Validator::make( $request->all(), $rules );

        $json  = array( 'status' => 'error', 'message' => 'You must enter a comment!' );

        if ( $validator->passes() )
        {
            $db = new Comments;
            $db->image_id = (int) $request->image_id;
            $db->user_id  = (int) \Auth::user()->id;
            $db->comment  = self::clean_comments( $request->comment );

            $db->save();
            $comment = [
                'id'       => $db->id,
                'image_id' => $db->image_id,
                'user_id'  => $db->user_id,
                'name'     => \Auth::user()->name,
                'profile_image' => self::profile_image( \Auth::user()->email, 35 ),
                'posted_at' => self::posted_at( $db->updated_at ),
                'comment'  => $db->comment,
            ];
            $comment = (object) $comment;
            $json = array( 'status' => 'success', 'message' => 'Comment posted!', 'comment' => $comment );
        } else {
            $messages = $validator->messages();
            $json  = array( 'status' => 'error', 'message' => $messages->first('comment') );
        }
        return \Response::json( $json );
    }

    // ------------------------------------------------------------------------
    // HELPER METHODS
    // ------------------------------------------------------------------------

    /**
     * Removes HTML tags, normalizes line endings, etc from user comments.
     * @static
     * @param  string $comment
     * @return string
     */
    static public function clean_comments( $comment = NULL )
    {
        $comment = str_replace( array( '<br>', '<br/>', '<br />' ), PHP_EOL,  $comment );
        $comment = strip_tags( $comment );
        $comment = self::remove_bad_words( $comment );
        return $comment;
    }

    // ------------------------------------------------------------------------

    /**
     * Returns gravatar image link
     * @static
     * @param  string  $email
     * @param  integer $width
     * @return string
     */
    public static function profile_image( $email = '', $width = 60 )
    {
        $default  = urlencode( url( '/images/CF_Logo_Black_x500.png' ) );

        $original   = $email;

        if ( is_string( $email ) )
        {
            $email = \DB::table('users')->where('email', $email)->first();
        }

        if ( is_object( $email ) && isset( $email->avatar ) && ( strlen( $email->avatar ) > 0 ) )
        {
            return $email->avatar;
        }


        if ( is_object( $email ) && isset( $email->email ) )
        {
            $email = $email->email;
        } else {
            $email = $default;
        }

        // no avatar found use gravatar fallback
        $image_link = 'http://www.gravatar.com/avatar/%s?d=%s&s=%d';
        return sprintf( $image_link, md5( $email ), $default, $width );
    }


    // ------------------------------------------------------------------------

    /**
     * Returns relative time from
     * @static
     * @param  string $time
     * @return string
     */
    public static function posted_at( $time = '' )
    {
        return \Carbon\Carbon::createFromTimeStamp(strtotime($time))->diffForHumans();
    }

    // ------------------------------------------------------------------------

    /**
     * Removes bad word from comment string
     * @static
     * @param  string $comment
     * @return string
     */
    public static function remove_bad_words( $comment = '' )
    {
        $bad_words = \Config::get('blog.bad_words');
        $bad_words = explode( '|', $bad_words );
        return str_replace( $bad_words, '', $comment );
    }

    // ------------------------------------------------------------------------
    // API
    // ------------------------------------------------------------------------

    /**
     * Send photographer data to Hatchbuck CRM
     *
     * @access private
     * @param  User $user
     * @return boolean
     */
    private function _send_hatchbuck( $user = NULL )
    {
        if ( ! $user ) return FALSE;

        $url       = 'https://app.hatchbuck.com/onlineForm/submit.php';
        $name      = explode( ' ', $user->name );

        $fields = array(
            'formID'                 => '60042712119',
            'simple_spc'             => '60042712119-60042712119',
            'enable303Redirect'      => '0',
            'enableServerValidation' => '1',
            'q1_firstName1'          => isset( $name[0] ) ? $name[0] : '',
            'q3_lastName3'           => isset( $name[1] ) ? $name[1] : '',
            'q4_email'               => $user->email,
            'q5_photographer'        => 'Photographer',
        );

        $fields_string = http_build_query( $fields );

        $cr     = curl_init();
        curl_setopt( $cr, CURLOPT_URL, $url );
        curl_setopt( $cr, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $cr, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $cr, CURLOPT_POST, 1 );
        curl_setopt( $cr, CURLOPT_POSTFIELDS, $fields_string );
        $result = curl_exec( $cr );
        curl_close($cr);

        echo $result;
        exit(0);

        return TRUE;
    }

}
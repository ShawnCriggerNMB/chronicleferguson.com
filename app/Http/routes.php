<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// ------------------------------------------------------------------------
// Route model binding
// ------------------------------------------------------------------------
Route::model('user', 'App\User');
Route::model('role', 'App\Role');


Route::get('/',
  ['as' => 'home', 'uses' => 'PagesController@getHome']);

// ------------------------------------------------------------------------
// Contact Form Routes
// ------------------------------------------------------------------------
Route::get('contact',
  ['as' => 'contact', 'uses' => 'PagesController@contactPage']);
Route::post('contact',
  ['as' => 'contact_store', 'uses' => 'PagesController@store']);

// ------------------------------------------------------------------------
// Donation Form Routes
// ------------------------------------------------------------------------
Route::get('donate',
  ['as' => 'donate', 'uses' => 'DonateController@show']);
Route::get('donation-success',
  ['as' => 'donation-success', 'uses' => 'DonateController@success']);
Route::get('donation-cancel',
  ['as' => 'donation-cancel', 'uses' => 'DonateController@cancel']);
Route::post('donation-ipn',
  ['as' => 'donation-ipn',     'uses' => 'DonateController@ipn']);

// ------------------------------------------------------------------------
// Blog
// ------------------------------------------------------------------------
//Route::get('blog', 'BlogController@index');
//Route::get('blog/{slug}', 'BlogController@showPost');

// ------------------------------------------------------------------------
// Logging in and out
// ------------------------------------------------------------------------
Route::get ( '/login',  'Auth\AuthController@getLogin'  );
Route::post( '/login',  'Auth\AuthController@postLogin' );
Route::get ( '/logout', 'Auth\AuthController@getLogout' );
// ------------------------------------------------------------------------
//Social Login
// ------------------------------------------------------------------------
Route::get( '/login/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuth',
    'as'   => 'auth.getSocialAuth'
]);


Route::get('/login/callback/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuthCallback',
    'as'   => 'auth.getSocialAuthCallback'
]);

Route::get('/send-to-hatchbuck',[
    'uses' => 'Auth\AuthController@hatchbuck_landing',
    'as'   => 'auth.hatchbuck_landing'
]);

// ------------------------------------------------------------------------
// User Auth Routes
// ------------------------------------------------------------------------

Route::group([ 'namespace' => 'Auth' ], function () {

	Route::get ('/register', 'AuthController@getRegister');
	Route::post('/register', 'AuthController@postRegister');

	Route::get ('/profile', 'AuthController@getProfile');
	Route::post('/profile', 'AuthController@postProfile');

	Route::group(['prefix' => 'password'], function () {
		// ------------------------------------------------------------------------
		// Password Reset Routes
		// ------------------------------------------------------------------------
		Route::get ('email', 'PasswordController@getEmail');
		Route::post('email', 'PasswordController@postEmail');
		Route::get ('reset/{code}', 'PasswordController@getReset');
		Route::post('reset', 'PasswordController@postReset');
	});
});

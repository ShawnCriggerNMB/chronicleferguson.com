<?php
	return [
		// SEO Default title and Meta description
		'title' => 'Chronicle Ferguson',
		'seo_meta_description' => '',

		// currently unuse
		'posts_per_page' => 5,
		'page_image'     => 'blog-bg.jpg',

		// upload path information.
		'uploads' => [
			'storage' => 'local',
			'webpath' => '/uploads',
		],

		// links to social media
		'social_links' => [
			'facebook'  => 'https://www.facebook.com/ChronicleFerguson/',
			'twitter'   => 'https://twitter.com/ChronicleLives',
			'instagram' => 'https://www.instagram.com/chronicleferguson/',
		],

		// add Google Analytics code ( UA-XXXX-XXXX type string ) for GA tracking.
		'ga_analytics' => '',

		'bad_words' => 'fuck|shit|nigger',

		'email_from' => 'organizer@chronicleferguson.com',
		'email_name' =>  'Organizer of Chronicle Ferguson',
		'contact_subject' => 'Thank you for contacting us.',

	];
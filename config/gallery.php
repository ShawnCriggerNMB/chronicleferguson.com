<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sizes to resize images to.
    |--------------------------------------------------------------------------
    |
    */

    // full size image
    'large_size'  => [ 940, 700 ],

    // for smaller screens, since the most common mobile device is 360px wide, i went a small amount over.
    'medium_size' => [ 400, 300 ],

    // thumbnail size used for preview image in admin area.
    'small_size'  => [ 50, 50 ],
];
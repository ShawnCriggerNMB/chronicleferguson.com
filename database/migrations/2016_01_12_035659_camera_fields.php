<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CameraFields extends Migration
{
  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('cameras', function (Blueprint $table) {
      $table->increments('id');
      $table->string('camera')->unique();
      $table->string('title');
      $table->string('meta_description');
      $table->string('layout')->default('blog.index');
      $table->boolean('reverse_direction');
      $table->timestamps();
    });

    Schema::create('image_camera_pivot', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('image_id')->unsigned()->index();
      $table->integer('tag_id')->unsigned()->index();
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
    Schema::drop('cameras');
    Schema::drop('image_camera_pivot');

  }
}


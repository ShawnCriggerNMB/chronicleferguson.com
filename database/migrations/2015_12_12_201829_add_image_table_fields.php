<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images',function(Blueprint $table){         

                $table->string('name');
                $table->string('email');
                $table->string('location');
                $table->string('date');
                $table->text('comment');/*Line 14*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('email');
            $table->dropColumn('location');
            $table->dropColumn('date');
            $table->dropColumn('comment');
        });
    }
}

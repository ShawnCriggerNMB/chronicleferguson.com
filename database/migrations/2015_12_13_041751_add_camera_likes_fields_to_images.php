<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCameraLikesFieldsToImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_likes', function(Blueprint $table)
        {
            $table->increments('id');            

            $table->integer('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('images');            

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');          

            $table->timestamps();
        });
        Schema::table('images',function(Blueprint $table){         
                $table->string('camera');
        });


    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('image_likes');
        Schema::table('images',function(Blueprint $table){         
            $table->dropColumn('camera');            
        });        
    }
}

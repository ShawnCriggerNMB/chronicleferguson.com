<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('file');
//			$table->string('caption');
//			$table->text('description');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('rating');
			$table->timestamps();
		});
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');

			$table->text('comment');

			$table->integer('image_id')->unsigned();
			$table->foreign('image_id')->references('id')->on('images');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			//$table->integer('user_id')->unsigned();
			//$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			//$table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
			$table->timestamps();
//			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});

	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('images');
		Schema::dropIfExists('comments');
	}
}

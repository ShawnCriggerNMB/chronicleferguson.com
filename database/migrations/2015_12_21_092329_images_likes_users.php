<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImagesLikesUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->string('comments');
            $table->string('interests');
        });
        Schema::table('images',function(Blueprint $table){
            $table->string('score');
        });
        Schema::table('image_likes',function(Blueprint $table){
            $table->string('user_ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
            $table->dropColumn('comments');
            $table->dropColumn('interests');
        });
        Schema::table('images',function(Blueprint $table){
            $table->dropColumn('score');
        });
        Schema::table('image_likes',function(Blueprint $table){
            $table->dropColumn('user_ip');
        });
    }
}

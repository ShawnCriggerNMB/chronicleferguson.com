<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->string('phone');
            $table->string('zipcode');
            $table->string('institution');
            $table->string('organization');
            $table->string('user_type');
            $table->boolean('confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
            $table->dropColumn('phone');
            $table->dropColumn('institution');
            $table->dropColumn('organization');
            $table->dropColumn('zipcode');
            $table->dropColumn('user_type');
            $table->dropColumn('confirmed');
            $table->dropColumn('confirmation_code');
        });
    }
}

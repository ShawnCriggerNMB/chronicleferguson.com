@extends('admin.layout')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/select2/select2.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/select2/select2-bootstrap.css') }}">
@stop

{{-- Content --}}
@section('content')

<!-- <div class="container"> -->
<div class="col-md-12">

<?php

if ( 'edit' !== $mode )
{
	$is_req = ' required ';
} else {
	$is_req = '';
}

$unique_link = '/api/users/unique_email/';
if ( 'create' !== $mode ) :
	$unique_link = '/api/users/unique_email?user_id=' . $user->id ;
endif;

module_header( 'user', $mode, 'User' );
?>

<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Fields marked with a <strong style="color:#C90000">*</strong> are required.<br>

</div>

<!-- ./ tabs -->
{{-- Create User Form --}}
{!! Form::model($user,['url'=>'/admin/users','autocomplete'=>'off','class'=>'form-horizontal', 'id' => 'user_form', 'data-toggle' => 'validator' ]) !!}

@if ($mode == 'edit')
	<input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}" />
@endif

<!-- username -->
<div class="form-group has-feedback required {{ $errors->has('name') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label" for="name">Name</label>
  <div class="col-md-7">
    {!! Form::input('text', 'name', null, [
      'id'             => 'name',
      'class'          => 'form-control',
      'value'          => old( 'name', $user->name ),
      'required'       => 'required',
      'data-required'  => 'You must fill in the name field.',
      'data-minlength' => '4' ]) !!}
      <span class="form-control-feedback" aria-hidden="true"></span>

    @if ( $errors->first('name') )
      {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>

<!-- Email -->
<div class="form-group has-feedback required {{ $errors->has('email') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label" for="email">Email</label>
  <div class="col-md-7">
    {!! Form::input('text', 'email', null, [
    	'id'            =>'email',
    	'class'         =>'form-control',
    	'required'      => 'required',
    	'value'         => old( 'email', $user->email ),
    	'data-required' => 'You must fill in the email field.',
//    	'data-remote'   => '/api/users/unique_email'
    ]) !!}
    <span class="form-control-feedback" aria-hidden="true"></span>
    @if ( $errors->first('email') )
      {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>
<!-- ./ email -->

<!-- Password -->
<div class="form-group has-feedback required {{ $errors->has('password') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label" for="password">Password</label>
  <div class="col-md-7">
  	<div class="input-group">
  		<input class="form-control" data-minlength="6" type="password" name="password" id="password" value="" <?php echo $is_req; ?> />
  		<span class="input-group-btn">
  			<button class="btn btn-default reveal" type="button" tabindex="-1"><i class="fa fa-eye"></i></button>
  		</span>
  	</div>
  	<span class="form-control-feedback" style="right: 50px;" aria-hidden="true"></span>
    @if ( $errors->first('password') )
      {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>
<!-- ./ password -->

<!-- Password Confirm -->
<div class="form-group has-feedback required {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label" for="password_confirmation">Password Confirm</label>
  <div class="col-md-7">
  	<div class="input-group">
  		<input class="form-control" type="password" data-match="#password" name="password_confirmation" id="password_confirmation" value="" data-error="Password confirmation must match password" <?php echo $is_req; ?> />
  		<span class="input-group-btn">
  			<button class="btn btn-default reveal" type="button" tabindex="-1"><i class="fa fa-eye"></i></button>
  		</span>
  	</div>
  	<span class="form-control-feedback" style="right: 50px;" aria-hidden="true"></span>
    @if ( $errors->first('password_confirmation') )
      {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif

  </div>
</div>
<!-- ./ password confirm -->

<div class="form-group has-feedback {{ $errors->has('zipcode') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label">Zipcode</label>
  <div class="col-md-7">
    <input type="text" class="form-control" name="zipcode" value="{{ old( 'zipcode', $user->zipcode ) }}" data-zip="zip">
  	<span class="form-control-feedback" aria-hidden="true"></span>
    @if ( $errors->first('zipcode') )
      {!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>

<div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label">Phone Number</label>
  <div class="col-md-7">
    <input type="text" class="form-control" name="phone" value="{{ old( 'phone', $user->phone ) }}" data-phone="phone">
  	<span class="form-control-feedback" aria-hidden="true"></span>
    @if ( $errors->first('phone') )
      {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>

<div class="form-group has-feedback {{ $errors->has('institution') ? 'has-error' : '' }}">
	<label class="col-md-2 control-label">Institution</label>
	<div class="col-md-10">
		<input type="text" class="form-control" name="institution" value="{{ old( 'institution', $user->institution ) }}">
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('institution') )
	      {!! $errors->first('institution', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	</div>
</div>

<div class="form-group has-feedback {{ $errors->has('organization') ? 'has-error' : '' }}">
	<label class="col-md-2 control-label">Organization</label>
	<div class="col-md-10">
		<input type="text" class="form-control" name="organization" value="{{ old( 'organization', $user->organization ) }}">
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('organization') )
	      {!! $errors->first('organization', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	</div>
</div>

<div class="form-group has-feedback {{ $errors->has('comment') ? 'has-error' : '' }}">
	<label class="col-md-2 control-label">Comment</label>
	<div class="col-md-10">
		<textarea rows="4" class="form-control" name="comment">{{ old( 'comment', $user->comments ) }}</textarea>
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('comment') )
	      {!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	</div>
</div>

<!-- Groups -->
<div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
	<label class="col-md-2 control-label" for="roles">Roles</label>
	<div class="col-md-6">
		<select class="form-control" name="roles[]" id="roles[]" >
			@foreach ($roles as $role)
			@if ($mode == 'create')
	        <option value="{{ $role->id }}" {{ selected( old('roles'), $role->id ) }}   >{{ $role->display_name }}</option>
			@else
			<option value="{{ $role->id }}" {{ selected( $role->id, $user_roles->id )}} >{{ $role->display_name }}</option>
			@endif
			@endforeach
		</select>

		<span class="help-block">
			Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
		</span>
	</div>
</div>
<!-- ./ groups -->

<div class="form-group has-feedback required {{ $errors->has('user_type') ? 'has-error' : '' }}">
  <label class="col-md-3 control-label">User Type</label>
  <div class="col-md-7">
    <select class="form-control" name="user_type" id="user_type" required>
      <option value="">Please select...</option>
      <option value="photographer" {{ selected ( 'photographer',  old('user_type', $user->user_type) ) }}>Photographer</option>
      <option value="follower"     {{ selected ( 'follower',      old('user_type', $user->user_type) ) }}>Follower</option>
      <option value="sponsor"      {{ selected ( 'sponsor',       old('user_type', $user->user_type) ) }}>Become a Sponser</option>
      <option value="educator"     {{ selected ( 'educator',      old('user_type', $user->user_type) ) }}>Educator</option>
      <option value="media"        {{ selected ( 'media',         old('user_type', $user->user_type) ) }}>Media</option>
    </select>
    <span class="form-control-feedback" aria-hidden="true" style="right: 25px;"></span>
    @if ( $errors->first('user_type') )
      {!! $errors->first('user_type', '<span class="help-block">:message</span>') !!}
    @else
      <span class="help-block with-errors"></span>
    @endif
  </div>
</div>

<div class="form-group has-feedback {{ $errors->has('interests') ? 'has-error' : '' }}">
	<label class="col-md-2 control-label">Interested In</label>
	<div class="col-md-10">
		<select class="form-control select2-multiple" name="interests[]" id="interests" multiple>
			<option value="activist"        {{ selected ( 'activist',   $user->interests )       }}>Activist</option>
			<option value="blogger"         {{ selected (  'blogger',   $user->interests )       }}>Blogger</option>
			<option value="influencer"      {{ selected ( 'influencer', $user->interests )       }}>Influencer</option>
			<option value="journalist"      {{ selected ( 'journalist', $user->interests )       }}>Journalist</option>
			<option value="photojournalist" {{ selected (  'photojournalist', $user->interests ) }}>Photojournalist</option>
			<option value="educator"        {{ selected (  'educator',  $user->interests )       }}>Educator</option>
			<option value="book"            {{ selected (  'book',      $user->interests )       }}>I want the Book!</option>
		</select>
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('interests') )
	      {!! $errors->first('interests', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif

	</div>
</div>

<!-- ./ tabs content -->

<!-- Form Actions -->
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button type="button" class="btn btn-warning close_popup">Cancel</button>
		<button type="reset" class="btn btn-default">Reset</button>
		<button type="submit" class="btn btn-success">Save</button>
	</div>
</div>
<!-- ./ form actions -->
{!! Form::close() !!}
</div>

@stop

@section('scripts')
	<script src="/assets/js/select2/select2.min.js"></script>
@stop

@push('inline-scripts')
<script type="text/javascript">
jQuery(document).ready(function($){
	$(".select2-multiple").select2();
	var form = $("#user_form");

	form.validator({
		html: true,
		custom: {
			phone: function($el) {
				var formats = "(999)999-9999|999-999-9999|9999999999";
				var r = RegExp("^(" +
				               formats
				                 .replace(/([\(\)])/g, "\\$1")
				                 .replace(/9/g,"\\d") +
				               ")$");
				return r.test($el.val());
			},
			zip: function($el) {
				return /^\d{5}(-\d{4})?$/.test($el.val());
			}
		},
		errors: {
			required: 'This field is required.',
			match: 'Does not match.',
			minlength: 'Not long enough.',
			phone: 'Please enter a valid phone number.',
			zip: 'Please enter a valid zip code.',
			remote: 'This email address has already been used.'
		},
		feedback: {
		  success: 'fa fa-check',
		  error: 'fa fa-times'
		}
	});

	$(".reveal").on('click',function() {
	    var $pwd = $("#password, #password_confirmation");
	    if ($pwd.attr('type') === 'password') {
	        $pwd.attr('type', 'text');
	    } else {
	        $pwd.attr('type', 'password');
	    }
	});

});
</script>
@stop
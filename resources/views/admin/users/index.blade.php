@extends('admin.layout')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}" />

<link href="{{ url('/assets/admin/css/dataTables.bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />

@endsection

@section('content')
 <!-- <div class="container"> -->
    <a class="btn btn-primary pull-right" style="margin-top:20px" href="{{ url('/admin/users/create') }}">Create New</a>
    <h2><i class="fa fa-user">&nbsp;</i> Manage Users</h2>
    <hr>

    <div class="row">
      <div class="col-md-12">

	    <table class="table table-bordered" id="users-table">
	        <thead>
	            <tr>
                    <th data-orderable="false" data-sort="false" style="text-align:center;">
                        <input type="checkbox" id="all" name="ids[]" />
                    </th>
                    <th>Name</th>
	                <th>Email</th>
					<th>Role</th>
	                <th>Created At</th>
	                <th>Updated At</th>
	                <th>Actions</th>
	            </tr>
	        </thead>
	    </table>

      </div>
    </div>
<!--  </div> -->
@stop

@section('scripts')
	<script src="/assets/admin/js/jquery.dataTables.min.js"></script>
	<script src="/assets/admin/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">

jQuery(function($) {


    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

  var table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.users.get_users') !!}',

        'aoColumns' : [

            { "data": function(d){
                return '<input style="text-align:center;" type="checkbox" name="id[]" value="'+ d[0] +'" />';
            }, "orderable": false, "searchable":false, "name":"users.id" },
            { "sortable": true, "id":"name" ,"name":"users.name" },
            { "sortable": true, "id":"email", "name":"users.email" },
            { "sortable": true, "id":"role_name", "name":"roles.name" },
            { "sortable": true, "id":"created_at", "name":"users.created_at" },
            { "sortable": true, "id":"updated_at", "name":"users.updated_at" },
            { "sortable": false, "id":"actions", orderable: false },
        ],
        "order": [[2, 'asc']]
    });

    $(document).on('click', '.delete-user', function(e){
        if ( ! confirm('Are you sure you want to delete this user?') ) return false;
        var link = this.href;
        $.ajax({
            type: "DELETE",
            url: link,
            success: function( data ) {
                table.draw();
            }
        });
        return false;
    })


});
</script>
@stop

@push('inline-scripts')

@endpush
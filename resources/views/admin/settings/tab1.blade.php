

	<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Title</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="title" value="{{ old( 'title', $settings['title'] ) }}">
			{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('seo_meta_description') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">SEO Meta description</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="seo_meta_description" value="{{ old( 'seo_meta_description', $settings['seo_meta_description'] ) }}">
			{!! $errors->first('seo_meta_description', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('ga_analytics') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Google Analytics Code</label>
		<div class="col-md-8">
			<input type="text" placeholder="UA-XXXXXXXX-X" class="form-control" name="ga_analytics" value="{{ old( 'ga_analytics', $settings['ga_analytics'] ) }}">
			{!! $errors->first('ga_analytics', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('uploads') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Uploads path</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="uploads" value="{{ old( 'uploads', $settings['uploads']['webpath'] ) }}">
			{!! $errors->first('uploads', '<span class="help-block">:message</span>') !!}
		</div>
	</div>


	<div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Facebook link</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="facebook" value="{{ old( 'facebook', $settings['social_links']['facebook'] ) }}">
			{!! $errors->first('facebook', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('twitter') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Twitter link</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="twitter" value="{{ old( 'twitter', $settings['social_links']['twitter'] ) }}">
			{!! $errors->first('twitter', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('instagram') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Instagram link</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="instagram" value="{{ old( 'instagram', $settings['social_links']['instagram'] ) }}">
			{!! $errors->first('instagram', '<span class="help-block">:message</span>') !!}
		</div>
	</div>


	<div class="form-group {{ $errors->has('bad_words') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Bad Words</label>
		<div class="col-md-8">
			<input type="text" class="form-control" id="bad_words" name="bad_words" value="{{ old( 'bad_words', $settings['bad_words'] ) }}">
            @if ( $errors->has('bad_words') )
            	{!! $errors->first('bad_words', '<span class="help-block with-errors">:message</span>') !!}
            @else
            	<span class="help-block">The bad words filter, are words that are not allowed in comments and are striped out if posted.</span>
            @endif
		</div>
	</div>

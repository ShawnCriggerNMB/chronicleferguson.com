
	<div class="form-group {{ $errors->has('email_from') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">From Email Address</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="email_from" value="{{ old( 'email_from', $settings['email_from'] ) }}">
			{!! $errors->first('email_from', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('email_name') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">From Email Name</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="email_name" value="{{ old( 'email_name', $settings['email_name'] ) }}">
			{!! $errors->first('email_name', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('contact_subject') ? 'has-error' : '' }}">
		<label class="col-md-4 control-label">Contact Form Email Subject</label>
		<div class="col-md-8">
			<input type="text" class="form-control" name="contact_subject" value="{{ old( 'contact_subject', $settings['contact_subject'] ) }}">
			{!! $errors->first('contact_subject', '<span class="help-block">:message</span>') !!}
		</div>
	</div>

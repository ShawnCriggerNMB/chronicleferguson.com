@extends('admin.layout')

@section('styles')
@stop

@section('content')

	<h2><i class="fa fa-cog"> </i> PHP Info</h2>
	<hr>
@include('admin.partials.errors')
@include('admin.partials.success')
	<div class="col-md-12 col-sm-12">
<?php echo phpinfo(); ?>
	</div>
@stop

@section('scripts')
@stop

@push('inline-scripts')
@stop
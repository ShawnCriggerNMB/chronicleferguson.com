@extends('admin.layout')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/tokenfield-typeahead.min.css' ) }}">
<link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/bootstrap-tokenfield.min.css' ) }}">
<style type="text/css">
	.nav-tabs {
    	margin-bottom: 15px;
	}
</style>
@stop

@section('content')

{!! Form::model($settings,['url'=>'/admin/settings','autocomplete'=>'off','class'=>'form-horizontal' ]) !!}

	<h2><i class="fa fa-cog"> </i> Website Settings</h2>
	<hr>
@include('admin.partials.errors')
@include('admin.partials.success')
	<div class="col-md-12 col-sm-12">

	 	<ul id="tabs" class="nav nav-tabs" role="tablist" data-tabs="tabs">
	 		<li role="presentation" class="active ">
	 			<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">General</a>
	 		</li>
	 		<li role="presentation">
	 			<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Email</a>
	 		</li>
	    </ul>
	    <div id="my-tab-content" class="tab-content">
	        <div class="tab-pane fade in active" id="tab1">
	        	@include('admin.settings.tab1')

	        </div>
	        <div class="tab-pane fade in" id="tab2">
	        	@include('admin.settings.tab2')
	        </div>
	    </div>

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<button type="button" class="btn btn-warning close_popup">Cancel</button>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
	</div>
<!-- ./ form actions -->
{!! Form::close() !!}

@stop

@section('scripts')

<script src="{{ url( '/assets/admin/js/bootstrap-tokenfield.min.js' ) }}"></script>
<script type="text/javascript">


jQuery(document).ready(function($){
    $('#tabs').tab();

	$('#bad_words').tokenfield({
	  autocomplete: {
	    source: [],
	    delay: 100
	  },
	  showAutocompleteOnFocus: true
	})

});
</script>
@stop

@push('inline-scripts')
@stop
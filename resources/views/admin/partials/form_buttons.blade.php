    <div class="form-group">
        <div class="col-sm-12 well" style="margin-bottom:10px">
            <button type="reset" class="btn btn-danger pull-left"><span class="glyphicon glyphicon-transfer"></span> Reset Form</button>
            &nbsp;&nbsp;&nbsp;
            <input type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Submit</button>
        </div>
    </div>
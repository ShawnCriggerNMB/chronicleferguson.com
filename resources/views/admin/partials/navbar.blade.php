<ul class="nav navbar-nav">
  <li><a href="/">Home</a></li>
  @if (Auth::check())
    <li @if (Request::is('admin/images*')) class="active" @endif>
      <a href="/admin/images">Gallery</a>
    </li>
    <li @if (Request::is('admin/users*')) class="active" @endif>
      <a href="/admin/users">Users</a>
    </li>
  @endif
</ul>

<ul class="nav navbar-nav navbar-right">
  @if (Auth::guest())
    <li><a href="/login">Login</a></li>
  @else
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
         aria-expanded="false">{{ Auth::user()->name }}
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/logout">Logout</a></li>
      </ul>
    </li>
  @endif
</ul>
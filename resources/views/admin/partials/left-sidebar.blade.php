		<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  </button>
			  	<a class="navbar-brand" href="index.html"><?php echo \Config::get('blog.title'); ?></a>
			</div>
			<div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
				<a href="{{ url('/logout') }}" class="btn btn-danger square-btn-adjust">Logout</a>
			</div>
		</nav>
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li>
						<a href="{{ url('/admin') }}"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
					</li>

					<li>
                        <a {{ \HTML::activeState('admin.images.index') }} href="{{ url('/admin/images') }}"><i class="fa fa-picture-o fa-3x"></i> Images <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                        		<a href="{{ url('/admin/images') }}">Manage Images</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/images/create') }}">Upload Image</a>
                            </li>

<?php if ( \Entrust::hasRole('super_admin') ) : ?>
                            <li>
                                <a href="{{ url('/admin/images/resize') }}">Resize Images</a>
                            </li>
<?php endif; ?>
                        </ul>
                    </li>

					<li>
                        <a {{ \HTML::activeState('admin.users.index') }}  href="#"><i class="fa fa-user fa-3x"></i> Users <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                        		<a href="{{ url('/admin/users') }}">Manage Users</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/users/create') }}">Create User</a>
                            </li>
                        </ul>
                    </li>

					<li>
                        <a {{ \HTML::activeState('admin.post.index') }}  href="#"><i class="fa fa-file-text-o fa-3x"></i> Posts <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                        		<a href="{{ url('/admin/post') }}">Manage Posts</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/post/create') }}">Create Posts</a>
                            </li>
                        </ul>
                    </li>

					<li>
                        <a {{ \HTML::activeState('admin.tag.index') }}  href="#"><i class="fa fa-tags fa-3x"></i> Tags <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                        		<a href="{{ url('/admin/tag') }}">Manage Tags</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/tag/create') }}">Create Tags</a>
                            </li>
                        </ul>
                    </li>

                    @if ( \Entrust::hasRole('super_admin') )
                    <li>
                        <a {{ \HTML::activeState('admin.settings.index') }}  href="#"><i class="fa fa-cog fa-3x"></i> Settings <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a {{ \HTML::activeState('admin.settings.index') }}  href="{{ url('/admin/settings') }}">Settings</a>
                            </li>
                            <li>
                                <a {{ \HTML::activeState('admin.settings.phpinfo') }}  href="{{ url('/admin/settings/phpinfo') }}">PHP Info</a>
                            </li>
                        </ul>
                    </li>
                    @else
					<li>
                        <a {{ \HTML::activeState('admin.settings.index') }}  href="{{ url('/admin/settings') }}"><i class="fa fa-cog fa-3x"></i> Settings</a>
					</li>
                    @endif

				</ul>
			</div>
		</nav>
		<!-- /. NAV SIDE  -->

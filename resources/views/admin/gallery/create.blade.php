@extends('admin.layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.css') !!}">
  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.date.css') !!}">
@stop

{{-- Content --}}
@section('content')
{{-- Create User Form --}}
{!! Form::model($image,['url'=>'/admin/images','autocomplete' => 'false','class'=>'form-horizontal', 'files' => true]) !!}

	<div class="col-md-12 col-sm-12">
    	<h2><i class="fa fa-picture-o">&nbsp;</i> Upload Gallery Image</h2>
  		<hr>

        @include('admin.partials.errors')
        @include('admin.partials.success')

		<div id="name" class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Name</label>
			<div class="col-md-8">
				<input type="text" class="form-control col-md-8 typeahead" name="name" value="{{ old('name') }}" autocomplete="false" spellcheck="false">
				{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div id="email" class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">E-Mail</label>
			<div class="col-md-8">
				<input type="text" class="form-control col-md-8 typeahead" name="email" value="{{ old('email') }}" autocomplete="false" spellcheck="false">
				{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Location</label>
			<div class="col-md-8">
				<input type="text" class="form-control location typeahead" name="location" value="{{ old('location') }}">
				{!! $errors->first('location', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Date Taken</label>
			<div class="col-md-2">
				<input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}">
				{!! $errors->first('date', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Comments</label>
			<div class="col-md-8">
				<textarea class="form-control" name="comment">{{ old('comment') }}</textarea>
				{!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div id="camera" class="form-group {{ $errors->has('camera') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Camera</label>
			<div class="col-md-8">
				<input type="text" class="form-control typeahead" name="camera" value="{{ old('camera') }}">
				{!! $errors->first('camera', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('rating') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Rating</label>
			<div class="col-md-1">
				<select class="form-control" name="rating" id="rating">
<?php
	$rating = (int) old('rating');
	foreach( range( 0, 10) as $i ) :
		$sel = ( $rating === $i ) ? ' selected="selected" ' : '';
		echo "<option {$sel} value=\"{$i}\">{$i}</option>\n";
	endforeach;
?>
				</select>
			</div>
		</div>

		<div class="form-group {{ $errors->has('userfile') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Image</label>
			<div class="col-md-8">
				<input type="file" name="userfile" class="form-control">
				{!! $errors->first('userfile', '<span class="help-block">:message</span>') !!}
			</div>
		</div>



<!-- Form Actions -->
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button type="button" class="btn btn-warning close_popup">Cancel</button>
		<button type="submit" class="btn btn-success">OK</button>
	</div>
</div>

</div>

<!-- ./ form actions -->
{!! Form::close() !!}

@stop

@section('scripts')
<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.date.js') !!}"></script>
<script src="/assets/js/typeahead.bundle.js"></script>
<script type="text/javascript">
$(document).ready(function($){

	$("input").attr('autocomplete', 'false');

	$('.datepicker').pickadate({
		selectYears: 10,
		min: new Date(2000,1,1),
		selectMonths: true,
		format: 'mm/dd/yyyy',
		formatSubmit: 'mm/dd/yyyy'
	});

    // instantiate the bloodhound suggestion engine
    var locations = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local:  ['Ferguson', 'Downtown St. Louis', 'South Side St. Louis', 'Clayton', 'Central West End', 'North St. Louis']
    });

    // initialize the bloodhound suggestion engine
    locations.initialize();
/*
    $('.location').typeahead(
    {
    	highlight:true,
    	minLength:1,
		source:locations.ttAdapter()
    });
*/

	// initialize your element
	var $location_typehead = $('.location').typeahead(null, {
//		hint: true,
		highlight: true,
		minLength: 2,
	    source: locations.ttAdapter(),
	});

	// Get your data source
	var usersSource = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    remote: {
	        url: '/admin/api/get_users/?q=%QUERYSTRING',
	        wildcard: '%QUERYSTRING'
	    }
	});

	// initialize your element
	var $email_typehead = $('#email .typeahead').typeahead(null, {
//		hint: true,
		highlight: true,
		minLength: 5,
	    source: usersSource,
	    display: 'email'
	}).on('typeahead:selected', function(evt, item) {
		console.log(item);
	    $("#name .typeahead").val(item.name);
	});

	// initialize your element
	var $name_typehead = $('#name .typeahead').typeahead(null, {
//		hint: true,
		highlight: true,
		minLength: 5,
	    source: usersSource,
	    display: 'name'
	}).on('typeahead:selected', function(evt, item) {
		console.log(item);
	    $("#email .typeahead").val(item.email);
	});

	// Get your data source
	var camerasSource = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    remote: {
	        url: '/admin/api/get_cameras/?q=%QUERYSTRING',
	        wildcard: '%QUERYSTRING'
	    }
	});

	// initialize your element
	var $email_typehead = $('#camera .typeahead').typeahead(null, {
//		hint: true,
		highlight: true,
		minLength: 5,
		display: 'camera',
		limit: 50,
	    source: camerasSource,
	});


})
</script>
@stop

@push('inline-scripts')
@stop
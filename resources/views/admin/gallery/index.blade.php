@extends('admin.layout')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}" />
<link href="{{ url('/assets/admin/css/dataTables.bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
@stop

@section('content')

<style>
  table.table-bordered.dataTable tbody td {
      vertical-align: middle;
      text-align: center;
  }
  th.sorting {
      text-align: center;
  }
</style>
    <div class="row">
      <div class="col-md-12">
        <a class="btn btn-primary pull-right" style="margin-top:20px" href="{{ url('/admin/images/create') }}">Upload New</a>
        <h2><i class="fa fa-picture-o">&nbsp;</i> Gallery Images</h2>
      <hr>

      @include('admin.partials.errors')
      @include('admin.partials.success')

      <table class="table table-bordered" id="images-table">
          <thead>
              <tr>
                  <th data-orderable="false" data-sort="false" class="center" style="text-align:center;">
                      <input type="checkbox" id="all" name="ids[]" />
                  </th>
                  <th style="width:50px">Thumbnail</th>
                  <th>User</th>
                  <th>Rating</th>
                  <th>Taken At</th>
                  <th>Created At</th>
                  <th>Actions</th>
              </tr>
          </thead>
      </table>

      </div>
    </div>

    <hr>
@stop

@section('scripts')
  <script src="/assets/admin/js/jquery.dataTables.min.js"></script>
  <script src="/assets/admin/js/dataTables.bootstrap.js"></script>

@stop

@push('inline-scripts')
  <script type="text/javascript">

jQuery(function($) {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

  var table = $('#images-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.images.getImages') !!}',

        'aoColumns' : [
            { "data": function(d){
                return '<input style="text-align:center;" type="checkbox" name="id[]" value="'+ d[0] +'" />';
            }, "orderable": false, "searchable":false, "name":"images.id" },
          { "sortable": false, orderable: false, "id":"file" ,"name":"images.file" },
          { "sortable": true, "id":"name", "name":"images.name" },
          { "sortable": true, "id":"rating", "name":"images.rating" },
          { "sortable": true, "id":"taken_at", "name":"images.taken_at" },
          { "sortable": true, "id":"created_at", "name":"images.created_at" },
          { "sortable": false, "id":"actions", orderable: false },
        ],
        "order": [[5, 'desc']]
    });

    $(document).on('click', '.delete-image', function(e){
        if ( ! confirm('Are you sure you want to delete this image') ) return false;
        var link = this.href;
        $.ajax({
            type: "DELETE",
            url: link,
            success: function( data ) {

            }
        });
        table.draw();
        return false;
    })

});
</script>
@endpush
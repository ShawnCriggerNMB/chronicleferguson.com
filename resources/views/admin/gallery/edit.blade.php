@extends('admin.layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.css') !!}">
  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.date.css') !!}">
@stop

{{-- Content --}}
@section('content')
{{-- Create User Form --}}
{!! Form::model($image,['url'=>'/admin/images','autocomplete'=>'off','class'=>'form-horizontal', 'files' => true]) !!}

	<div class="col-md-12">
    	<h2><i class="fa fa-picture-o">&nbsp;</i> Edit Gallery Image</h2>
  		<hr>
  	</div>

	<div class="col-md-8 col-sm-12">
		<input type="hidden" name="image_id" value="{{ $image->id }}" />
		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Name</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="name" value="{{ $image->name }}">
				{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">E-Mail</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="email" value="{{ $image->email }}">
				{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Location</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="location" value="{{ $image->location }}">
				{!! $errors->first('location', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Date Taken</label>
			<div class="col-md-6">
				<input type="text" class="form-control datepicker" name="date" value="{{ $image->date }}">
				{!! $errors->first('date', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Comments</label>
			<div class="col-md-6">
				<textarea class="form-control" name="comment">{{ $image->comment }}</textarea>
				{!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('camera') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Camera</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="camera" value="{{ $image->camera }}">
				{!! $errors->first('camera', '<span class="help-block">:message</span>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('rating') ? 'has-error' : '' }}">
			<label class="col-md-2 control-label">Rating</label>
			<div class="col-md-6">
				<select class="form-control" name="rating" id="rating">
<?php
	foreach( range( 0, 10) as $i ) :
		$selected = ( $image->rating == $i ) ? ' selected="selected" ' : '';
		echo "<option {$selected} value=\"{$i}\">{$i}</option>\n";
	endforeach;
?>
				</select>
			</div>
		</div>



<!-- Form Actions -->
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button type="button" class="btn btn-warning close_popup">Cancel</button>
		<button type="submit" class="btn btn-success">OK</button>
	</div>
</div>

</div>
	<div class="col-md-4 col-sm-12">

		<div class="thumbnail">
			<a href="/{{ $image->file }}" class="modal-link" target="_blank">
				<img src="/{{ str_replace('uploads/', 'uploads/m_', $image->file ) }}" />
			</a>
		</div>
		<input type="file" name="userfile">
	</div>

<!-- ./ form actions -->
{!! Form::close() !!}

@stop

@section('scripts')
	<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.js') !!}"></script>
	<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.date.js') !!}"></script>

<script type="text/javascript">
$(document).ready(function($){

  $('.datepicker').pickadate({
    selectYears: 20,
    min: new Date(2000,1,1),
    selectMonths: true,
    format: 'mm/dd/yyyy',
    formatSubmit: 'mm/dd/yyyy'
  });

});
</script>

@stop

@push('inline-scripts')

@stop
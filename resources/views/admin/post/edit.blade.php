@extends('admin.layout')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}" />
<link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/js/jquery-ui/jquery-ui.min.css' ) }}">
  <link href="/assets/pickadate/themes/default.css" rel="stylesheet">
  <link href="/assets/pickadate/themes/default.date.css" rel="stylesheet">
  <link href="/assets/pickadate/themes/default.time.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/tokenfield-typeahead.min.css' ) }}">
	<link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/bootstrap-tokenfield.min.css' ) }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/markdown-editor/css/bootstrap-markdown-editor.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/markdown-editor/css/bootstrap-markdown-editor.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/ace-builds/monokai.css') }}">
	<style type="text/css">

		#page-inner {
			min-height: 1100px;
		}

		.tokenfield .token {
		    background-image: -webkit-linear-gradient(top, #428bca 0, #2d6ca2 100%);
		    background-image: linear-gradient(to bottom, #428bca 0, #2d6ca2 100%);
		    background-repeat: repeat-x;
		    background-color: #428bca;
		    border-color: #2b669a;
		    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff428bca', endColorstr='#ff2d6ca2', GradientType=0);
		    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
		    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
		    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15), 0 1px 1px rgba(0, 0, 0, 0.075);
		    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.15), 0 1px 1px rgba(0, 0, 0, 0.075);
		    color: #fff;
		    cursor: default;
		}
		.tokenfield .token .close {
		    color: #fff;
		    opacity: 0.8 filter: alpha(opacity=80);
		}
		.tokenfield .token.token-invalid {
		    color: #a94442 !important;
		    background-color: #f2dede !important;
		    background-image: none !important;
		    border: none;
		    border-bottom: 1px dotted #a94442 !important;
		}
		.tokenfield .token.token-invalid .close {
		    color: #600 !important;
		}
		.tokenfield .twitter-typeahead .form-control {
		    display: none;
		}

		.ui-autocomplete {
		    position: absolute;
		    top: 100%;
		    left: 0;
		    z-index: 1000;
		    float: left;
		    display: none;
		    min-width: 160px;
		    _width: 160px;
		    padding: 4px 0;
		    margin: 2px 0 0 0;
		    list-style: none;
		    background-color: #ffffff;
		    border-color: #ccc;
		    border-color: rgba(0, 0, 0, 0.2);
		    border-style: solid;
		    border-width: 1px;
		    -webkit-border-radius: 5px;
		    -moz-border-radius: 5px;
		    border-radius: 5px;
		    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		    -webkit-background-clip: padding-box;
		    -moz-background-clip: padding;
		    background-clip: padding-box;
		    *border-right-width: 2px;
		    *border-bottom-width: 2px;
		}
		.ui-menu-item > a.ui-corner-all {
		    display: block;
		    padding: 3px 15px;
		    clear: both;
		    font-weight: normal;
		    line-height: 18px;
		    color: #555555;
		    white-space: nowrap;
		}
		.ui-state-hover, &.ui-state-active {
		      color: #ffffff;
		      text-decoration: none;
		      background-color: #0088cc;
		      border-radius: 0px;
		      -webkit-border-radius: 0px;
		      -moz-border-radius: 0px;
		      background-image: none;
		    }
	</style>
@stop

{{-- Content --}}
@section('content')

<!-- <div class="container"> -->
<div class="col-md-12">
<?php module_header( 'file-text-o', $mode, 'Post' ); ?>

	<div class="alert alert-warning alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  Fields marked with a <strong style="color:#C90000">*</strong> are required.<br>
	</div>

<?php if ( isset( $id ) ) : ?>
	<form class="form-horizontal" id="form" role="form" method="POST" action="{{ url('/admin/post/' . $id ) }}">
	  	<input type="hidden" name="id" value="{{ $id }}">
	  	<input type="hidden" name="_method" value="PUT">
<?php else : ?>
    <form class="form-horizontal" id="form" role="form" method="POST" action="{{ route('admin.post.store') }}">
<?php endif; ?>
	  <input type="hidden" name="_token" value="{{ csrf_token() }}">

	  @include('admin.post._form')

</div>

<!-- ./ tabs content -->

<!-- Form Actions -->
@include('admin.partials.form_buttons')
<!-- ./ form actions -->
{!! Form::close() !!}
</div>

@stop

@section('scripts')
	<script src="{{ url( '/assets/admin/js/jquery-ui/jquery-ui.js' ) }}"></script>
	<script src="{{ url( '/assets/admin/js/bootstrap-charcounter.js' ) }}"></script>
	<script src="{{ url( '/assets/admin/js/bootstrap-tokenfield.min.js' ) }}"></script>
	<script src="/assets/js/ace-builds/src-min/ace.js"></script>
	<script src="/assets/js/ace-builds/src-min/theme-monokai.js"></script>
	<script src="/assets/js/markdown-editor/js/bootstrap-markdown-editor.js"></script>
	<script src="/assets/pickadate/picker.js"></script>
	<script src="/assets/pickadate/picker.date.js"></script>
	<script src="/assets/pickadate/picker.time.js"></script>

@stop

@push('inline-scripts')
<script type="text/javascript">
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

	$("#publish_date").pickadate({
		format: "mmm-d-yyyy"
	});
	$("#publish_time").pickatime({
		format: "h:i A"
	});

	$("#meta_description").charCount({
	    allowed: 160,
	    warning: 140,
	    counterText: 'Characters left: '
	});

    $('#tabs').tab();
/*
     var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: '/admin/api/fetch_tags',
                remote: {
                    url: '/admin/api/fetch_tags/%QUERY',
                    wildcard: '%QUERY'
                }
            });
*/

    $('#remote .typeahead').bind('typeahead:select', function(ev, suggestion) {
        $('.tags_id').val(suggestion.id);
    });

	$('#remove .typeahead').on('tokenfield:createtoken', function (event) {
	    var existingTokens = $(this).tokenfield('getTokens');
	    $.each(existingTokens, function(index, token) {
	        if (token.value === event.attrs.value)
	            event.preventDefault();
	    });
	});

/*
	$('#tags').tokenfield({
		showAutocompleteOnFocus: true,
        createTokensOnBlur: true,
		typeahead: [null, { source: engine.ttAdapter() }]
	});
*/


	$('#tags').tokenfield({
		autocomplete: {
            //define callback to format results
            source: function(req, add){
                //pass request to server
                $.getJSON("/admin/api/fetch_tags/", req, function(data) {
                    var suggestions = [];
                    $.each(data, function(i, val){
	                    suggestions.push(val.name);
    	            });
                	add(suggestions);
                });
            },
			delay: 100
		},
		showAutocompleteOnFocus: true,
		delimiter: [',',' ', '-', '_']
	});

/*
	  autocomplete: {
	    source: [],
	    delay: 100
	  },
	  showAutocompleteOnFocus: true
*/

	$('#editor').markdownEditor({
		height : '300px',
		fullscreen: false,
	  	imageUpload: true,
	  	theme: 'monokai',
	  	uploadPath: '/admin/api/handle_upload',
	  	preview: true,
	  	onPreview: function (content, callback) {
		    $.ajax({
		      url: '/admin/api/parse_preview',
		      type: 'POST',
		      dataType: 'html',
		      data: {content: content},
		    })
		    .done(function(result) {
		      // Return the html:
		      callback(result);
		    });

		}
	});

	$('#editor').markdownEditor('setContent', $("#content").val() );

	$("#content").fadeOut();
	$("#editor").fadeIn();

	$("#form").submit(function(e){
		var markdownContent = $('#editor').markdownEditor('content');
		$("#content").val( markdownContent );
	});

	var form = $("#user_form");

	form.validator({
		html: true,
		custom: {
			phone: function($el) {
				var formats = "(999)999-9999|999-999-9999|9999999999";
				var r = RegExp("^(" +
				               formats
				                 .replace(/([\(\)])/g, "\\$1")
				                 .replace(/9/g,"\\d") +
				               ")$");
				return r.test($el.val());
			},
			zip: function($el) {
				return /^\d{5}(-\d{4})?$/.test($el.val());
			}
		},
		errors: {
			required: 'This field is required.',
			match: 'Does not match.',
			minlength: 'Not long enough.',
			phone: 'Please enter a valid phone number.',
			zip: 'Please enter a valid zip code.',
			remote: 'This email address has already been used.'
		},
		feedback: {
		  success: 'fa fa-check',
		  error: 'fa fa-times'
		}
	});

});
</script>
@stop
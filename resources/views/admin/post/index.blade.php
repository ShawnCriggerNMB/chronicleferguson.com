@extends('admin.layout')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}" />

<link href="{{ url('/assets/admin/css/dataTables.bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />

@endsection

@section('content')
 <!-- <div class="container"> -->
    <a class="btn btn-primary pull-right" style="margin-top:20px" href="{{ url('/admin/post/create') }}">Create New</a>
    <h2><i class="fa fa-file-text-o">&nbsp;</i> Manage Posts</h2>
    <hr>

    <div class="row">
      <div class="col-md-12">

       <table id="posts-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Title</th>
              <th>Draft</th>
              <th>Author</th>
              <th>Published</th>
              <th data-sortable="false">Actions</th>
            </tr>
          </thead>
          <tbody>
<?php /*
            @foreach ($posts as $post)
              <tr>
                <td data-order="{{ $post->published_at->timestamp }}">
                  {{ $post->published_at->format('j-M-y g:ia') }}
                </td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->subtitle }}</td>
                <td>
                  <a href="/admin/post/{{ $post->id }}/edit"
                     class="btn btn-xs btn-info">
                    <i class="fa fa-edit"></i> Edit
                  </a>
                  <a href="/blog/{{ $post->slug }}"
                     class="btn btn-xs btn-warning">
                    <i class="fa fa-eye"></i> View
                  </a>
                </td>
              </tr>
            @endforeach
*/ ?>
          </tbody>
        </table>
      </div>
    </div>
<!--  </div> -->
@stop

@section('scripts')
	<script src="/assets/admin/js/jquery.dataTables.min.js"></script>
	<script src="/assets/admin/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">

jQuery(function($) {


    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
  var table = $('#posts-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.post.getPosts') !!}',

        'aoColumns' : [
        { "sortable": true, "id":"title", "name":"posts.title" },
        { "sortable": true, "id":"is_draft", "name":"posts.is_draft" },
        { "sortable": true, "id":"name", "name":"users.name" },
        { "sortable": true, "id":"published_at", "name":"posts.published_at" },
        { "sortable": false, "id":"actions", orderable: false },
        ]
    });

    $(document).on('click', '.delete-post', function(e){
        if ( ! confirm('Are you sure you want to delete this post') ) return false;
        var link = this.href;
        $.ajax({
            type: "DELETE",
            url: link,
            success: function( data ) {

            }
        });
        table.draw();
        return false;
    })

});
</script>
@stop

@push('inline-scripts')

@endpush
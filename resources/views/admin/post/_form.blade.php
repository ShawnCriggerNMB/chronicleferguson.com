<div class="row">

	<fieldset>
		<div class="col-md-12 well">
			<div class="form-group required">
<!--
				<label for="title" class="col-md-2 control-label">Title</label>
-->
				<div class="col-md-12">
					<input type="text" class="form-control" name="title" id="title" value="{{ $title }}" placeholder="Post title" required>
		      </div>
			</div>

		    <div class="form-group">
<!--
		    	<label for="subtitle" class="col-md-2 control-label">Subtitle</label>
-->
		    	<div class="col-md-12">
		    		<input type="text" class="form-control" name="subtitle" id="subtitle" value="{{ $subtitle }}" placeholder="Subtitle" >
		    	</div>
		    </div>


		    <div class="form-group">
<!--
		    	<label for="content" class="col-md-2 control-label required">Content</label>
-->
		    	<div class="col-md-12">
		    		<div id="editor"></div>
		    		<textarea class="form-control " name="content" rows="14" id="content">{{ $content }}</textarea>
		    	</div>
		    </div>

		</div>
	</fieldset>

	<fieldset>

		<div class="col-md-12">
			<div class="form-group required">
				<label for="publish_date" class="col-md-2 control-label">Pub Date</label>
				<div class="col-md-4">
	                <div class="input-group">
	                	<span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
						<input class="form-control required" name="publish_date" id="publish_date" type="text" value="{{ $publish_date }}" required>
	                </div>
				</div>
			</div>

			<div class="form-group required">
				<label for="publish_time" class="col-md-2 control-label">Pub Time</label>
				<div class="col-md-4">
	                <div class="input-group">
	                	<span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>
						<input class="form-control required" name="publish_time" id="publish_time" type="text" value="{{ $publish_time }}" required>
	                </div>
				</div>
			</div>

	        <div class="form-group">
	            <label for="tags" class="col-md-2 control-label">Tags</label>
	            <div class="col-md-4">
	                <div class="input-group" id="remote">
	                	<span class="input-group-addon"><i class="fa fa-tags fa-fw"></i></span>
	                    <input type="text" class="form-control" value="{{ implode( ', ', $allTags ) }}" name="tags_name" id="tags" placeholder="Choose post tags, seperate entries with tab, enter or semicolon ;" />
	                </div>
	                	<input type="hidden" class="tags_id" value="{{ implode( ', ', $allTags ) }}" name="tags_id">
	                	<span class="help-block">Separate entries with tab, enter or semicolon <b>;</b></span>
	            </div>
	        </div>

<?php /*
			<div class="form-group">
				<label for="tags" class="col-md-2 control-label">Tags</label>
				<div class="col-md-10">

        			<input class="form-control typeahead" type="text" placeholder="Choose a Subreddit" name="subreddit_name">
        			<input type="hidden" class="subreddit_id" value="" name="subreddit_id">

					<select name="tags[]" id="tags" class="form-control" multiple>
					@foreach ($allTags as $tag)
						<option @if (in_array($tag, $tags)) selected @endif value="{{ $tag }}">{{ $tag }}</option>
					@endforeach
					</select>
				</div>
			</div>
*/ ?>
			<input type="hidden" name="layout" value="{{ $layout }}" />
<?php /*
    <div class="form-group">
      <label for="layout" class="col-md-2 control-label">
        Layout
      </label>
      <div class="col-md-10">
        <input type="text" class="form-control" name="layout"
               id="layout" value="{{ $layout }}">
      </div>
    </div>
*/ ?>

    		<div class="form-group">
				<label for="meta_description" class="col-md-2 control-label">Meta Description</label>
				<div class="col-md-10">
					<textarea class="form-control" name="meta_description" id="meta_description" rows="2">{{ $meta_description }}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="is_draft" class="col-md-2 control-label">Draft</label>
				<div class="col-md-10">
					<select name="is_draft" id="is_draft" class="form-control">
						<option {{ selected( '0', $is_draft ) }} value="0">No</option>
						<option {{ selected( '1', $is_draft ) }} value="1">Yes</option>
					</select>
				</div>
			</div>

		</div>

	</fieldset>

</div>
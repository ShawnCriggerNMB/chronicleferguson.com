@extends('admin.layout')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}" />

<link href="{{ url('/assets/admin/css/dataTables.bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />

@endsection

@section('content')
 <!-- <div class="container"> -->
    <a class="btn btn-primary pull-right" style="margin-top:20px" href="{{ url('/admin/post/create') }}">Create New</a>
    <h2><i class="fa fa-file-text-o">&nbsp;</i> Manage Posts</h2>
    <hr>

    <div class="row">
      <div class="col-md-12">

       <table id="posts-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Published</th>
              <th>Title</th>
              <th>Subtitle</th>
              <th data-sortable="false">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($posts as $post)
              <tr>
                <td data-order="{{ $post->published_at->timestamp }}">
                  {{ $post->published_at->format('j-M-y g:ia') }}
                </td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->subtitle }}</td>
                <td>
                  <a href="/admin/post/{{ $post->id }}/edit"
                     class="btn btn-xs btn-info">
                    <i class="fa fa-edit"></i> Edit
                  </a>
                  <a href="/blog/{{ $post->slug }}"
                     class="btn btn-xs btn-warning">
                    <i class="fa fa-eye"></i> View
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
<!--  </div> -->
@stop

@section('scripts')
	<script src="/assets/admin/js/jquery.dataTables.min.js"></script>
	<script src="/assets/admin/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">

jQuery(function($) {


    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
/*
  var table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.users.get_users') !!}',

        'aoColumns' : [
        { "sortable": true, "id":"name" ,"name":"users.name" },
        { "sortable": true, "id":"email", "name":"users.email" },
        { "sortable": true, "id":"roles", "name":"roles" },
        { "sortable": true, "id":"created_at", "name":"users.created_at" },
        { "sortable": true, "id":"updated_at", "name":"users.updated_at" },
        { "sortable": false, "id":"actions", orderable: false },
        ]
    });

    $(document).on('click', '.delete-user', function(e){
        if ( ! confirm('Are you sure you want to delete this user?') ) return false;
        var link = this.href;
        $.ajax({
            type: "DELETE",
            url: link,
            success: function( data ) {
                table.draw();
            }
        });
        return false;
    })
*/

});
</script>
@stop

@push('inline-scripts')

@endpush
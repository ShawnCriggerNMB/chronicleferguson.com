<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo Meta::meta('title'); ?> Admin</title>

<?php
    echo Meta::tagMetaName( 'robots' );

    echo Meta::tagMetaProperty( 'site_name', Config::get('blog.title') );
    echo Meta::tagMetaProperty( 'url', Request::url() );
    echo Meta::tagMetaProperty( 'locale', 'en_EN' );

    echo Meta::tag( 'title' );
    echo Meta::tag( 'description' );

?>
  <link rel="shortcut icon" href="{!! url('/images/CF_Favicon_V2.ico') !!}">
  <link href="{{ url( '/css/bootstrap.min.css' ) }}" rel="stylesheet">
  <link href="{{ url( '/css/font-awesome.min.css' ) }}" rel="stylesheet">
  <link href="{{ url( '/assets/admin/css/admin-theme.css' ) }}" rel="stylesheet">

  @yield('styles')

  <!--[if lt IE 9]>
    <script src="{{ url('/assets/js/html5shiv.js') }}"></script>
    <script src="{{ url('/assets/js/respond.min.js') }}"></script>
  <![endif]-->
</head>
<body>

  <div id="wrapper">
    @include('admin.partials.left-sidebar')

    <div id="page-wrapper">
      <div id="page-inner">
        @yield('content')
<!--
        <div class="row">
          <div class="col-md-12">
            <h2>Blank Page</h2>
            <h5>Welcome Jhon Deo , Love to see you back. </h5>
          </div>
        </div>
        <hr>
-->

      </div>
    <!-- /. PAGE INNER  -->
    </div>
  <!-- /. PAGE WRAPPER  -->
  </div>

<script type="text/javascript" src="{!! url('/js/jquery-2.1.4.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/admin/js/jquery.metisMenu.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/admin/js/app.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/validator.js') !!}"></script>

@yield('scripts')
<!-- App scripts -->
@stack('inline-scripts')

</body>
</html>
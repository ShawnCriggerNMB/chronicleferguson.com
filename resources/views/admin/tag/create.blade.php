@extends('admin.layout')

@section('content')
  <div class="col-md-12 col-sm-12">
  <!-- <div class="container"> -->
    <?php module_header( 'file-tags', 'create', 'Create Tag' ); ?>

    @include('admin.partials.errors')
    @include('admin.partials.success')

    <form class="form-horizontal" role="form" method="POST" action="/admin/tag">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="form-group">
        <label for="tag" class="col-md-3 control-label">Tag</label>
        <div class="col-md-3">
          <input type="text" class="form-control" name="tag" id="tag" value="{{ $tag }}" autofocus>
        </div>
      </div>

      @include('admin.tag._form')

      <div class="form-group">
        <div class="col-md-7 col-md-offset-3">
          <button type="submit" class="btn btn-primary btn-md">
            <i class="fa fa-plus-circle"></i>
              Add New Tag
          </button>
        </div>
      </div>

    </form>
  </div>
@stop

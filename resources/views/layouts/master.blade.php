<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="/assets/css/app.css">
    <link href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css" rel="stylesheet">

<?php
  echo Meta::tag( 'title' );

  $social_links = array('facebook' => NULL, 'instagram' => NULL, 'twitter' => NULL );

?>

<script type="text/javascript">
//    document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';
    (function (){var url = window.location; var oImg = document.createElement("img");oImg.setAttribute('src','https://app.hatchbuck.com/TrackWebPage?ACID=1992&URL=' + url);})();
</script>
</head>
<body class="@yield('bodyclass')">

@include('partials.topbar')

@yield('content')

@yield('subfooter')

@include('partials.footer')
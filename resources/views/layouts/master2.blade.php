<!DOCTYPE html>
<html lang="en" class="@yield('bodyclass')">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link type="text/plain" rel="author" href="http://chronicleferguson.com/humans.txt">
    <title><?php echo Meta::meta('title'); ?></title>

<?php
    echo Meta::tagMetaName( 'robots' );
    echo Meta::tagMetaProperty( 'site_name', 'Chronicle Ferguson' );
    echo Meta::tagMetaProperty( 'url', Request::url() );
    echo Meta::tagMetaProperty( 'locale', 'en_EN' );

    echo Meta::tag( 'title' );
    echo Meta::tag( 'description' );
    echo Meta::tag( 'image' );

    if ( ! \Request::is( '/gallery/*' ) OR \Request::is( 'gallery/create' ) ) :
        # Set default share picture after custom section pictures
        Meta::meta( 'image', asset('/images/CF_Logo_White_x500.png' ) );
    endif;

    $social_links = config('blog.social_links');

?>

    <link rel="shortcut icon" href="{!! asset('/images/CF_Favicon_V2.ico') !!}">
    <!-- Compressed CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/6.2.1/foundation.min.css">

    <link href="{!! asset('/css/font-awesome.min.css') !!}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('/css/app.css') }}" media="all" rel="stylesheet" type="text/css" />

<?php if ( is_blog() ) : ?>
    <link href="{!! asset('/css/clean-blog.css') !!}" media="all" rel="stylesheet" type="text/css" />
<?php endif; ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{ asset('/assets/js/html5shiv.js') }}"></script>
      <script src="{{ asset('/assets/js/respond.min.js') }}"></script>
    <![endif]-->

<script type="text/javascript">
//    document.cookie='resolution='+Math.max(screen.width,screen.height)+'; path=/';
    (function (){var url = window.location; var oImg = document.createElement("img");oImg.setAttribute('src','https://app.hatchbuck.com/TrackWebPage?ACID=1992&URL=' + url);})();
</script>

</head>
  <body class="@yield('bodyclass')">

<div class="off-canvas-wrapper">
  <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
  <!-- Close button -->
  <button class="close-button" aria-label="Close menu" type="button" data-close>
    <span aria-hidden="true">&times;</span>
  </button>
  <!-- Close button -->

   <!-- Menu -->
    <ul class="vertical menu">
      <li><a href="#">Foundation</a></li>
      <li><a href="#">Dot</a></li>
      <li><a href="#">ZURB</a></li>
      <li><a href="#">Com</a></li>
      <li><a href="#">Slash</a></li>
      <li><a href="#">Sites</a></li>
    </ul>

  </div><!-- e d div#offCanvas -->
  <div class="off-canvas-content" data-off-canvas-content>


  <div class="top-bar">
    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li class="menu-text">
            <a class="logo" href="/"><h1>Chronicle :: Ferguson</h1></a>
        </li>
      </ul>
    </div>
    <div class="top-bar-right">
      <button class="menu-icon" type="button" data-open="offCanvasRight"></button>
      <span class="title-bar-title">Menu</span>

      <ul class="menu">
              <li>
                  <a href="/about"  class="{{ set_active('about') }}">About</a>
              </li>

              <li>
                  <a href="/gallery"  class="{{ set_active('gallery') }}">View Gallery</a>
              </li>

              <li>
                  <a href="/store" class="{{ set_active('store') }}">Store</a>
              </li>

              <li>
                   <a href="/blog" class="{{ set_active('blog') }}">Blog</a>
              </li>

              <li>
                  <a class="{{ set_active('contact') }}" href="{{ url('/contact') }}">Contact Us</a>
              </li>

              <li class="yellow-bordered solid-yellow">
                  <a href="https://www.generosity.com/education-fundraising/chronicle-ferguson-a-history-making-photo-book">Buy Book</a>
              </li>
              <li class="yellow-bordered solid-black">
                  <a class="{{ set_active('donate') }}"  href="{{ url('/donate') }}">Donate</a>
              </li>

      </ul>
    </div>
  </div>


<?php /*
                <div class="row text-right">
                    <ul class="sub-navigation">
<style type="text/css" media="screen">

a.top-facebook , a.top-twitter , a.top-instagram {
    color: #edc70f;
}

li.yellow-bordered.solid-yellow {
    border:1px solid #edc70f;
    color: #FFFFFF;
    background: #edc70f;
    margin-right: 0;
    padding: 5px;
}

li.yellow-bordered.solid-black a,
li.yellow-bordered.solid-yellow a {
    color: #FFFFFF;
}

li.yellow-bordered.solid-yellow a:hover {
    color: #000000;
}

li.yellow-bordered.solid-black {
    border:1px solid #edc70f;
    color: #FFFFFF;
    background: #000000;
    padding: 5px;
}

#footer a:hover, .social-icons a:hover, ul.sub-navigation li a:hover, ul.main-navigation li a:hover {
    color: #FFFFFF;
    text-decoration: none;
    border-bottom: 1px solid #edc70f;

}

</style>
@if (Auth::guest())
                            <li>
                                <a href="/login"  class="{{ set_active('login') }}">LOG IN OR REGISTER
                                </a>
                            </li>
@else
                            <li>
                                <a href="/logout"  class="{{ set_active('logout') }}">LOG OUT</a>
                            </li>
@endif

                        <li>
                            <a class="top-facebook" href="{{ $social_links['facebook'] }}">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>

                            <a class="top-twitter" href="{{ $social_links['twitter'] }}">
                                <i class="fa fa-twitter"></i>
                            </a>

                        </li>
                        <li>
                            <a class="top-instagram" href="{{ $social_links['instagram'] }}">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                	<ul class="main-navigation text-right">

                        <li>
                            <a href="/about"  class="{{ set_active('about') }}">About</a>
                        </li>

                        <li>
                            <a href="/gallery"  class="{{ set_active('gallery') }}">View Gallery</a>
                        </li>

                        <li>
                            <a href="/store" class="{{ set_active('store') }}">Store</a>
                        </li>

                        <li>
                             <a href="/blog" class="{{ set_active('blog') }}">Blog</a>
                        </li>

                        <li>
                            <a class="{{ set_active('contact') }}" href="{{ url('/contact') }}">Contact Us</a>
                        </li>

                        <li class="yellow-bordered solid-yellow">
                            <a href="https://www.generosity.com/education-fundraising/chronicle-ferguson-a-history-making-photo-book">Buy Book</a>
                        </li>
                        <li class="yellow-bordered solid-black">
                            <a class="{{ set_active('donate') }}"  href="{{ url('/donate') }}">Donate</a>
                        </li>

                	</ul>
                </div>
            </div>
            <div class="small-12 medium-2 large-2 columns">
        </div>
*/ ?>
@yield('page-header')

        <div class="row {!! content_class() !!}">
        	<div id="main-content" class="large-12 columns">
            	@yield('content')

    </div>
  </div>
</div>
    @yield('footer')
    <script type="text/javascript" src="{!! asset('/js/jquery-2.1.4.min.js') !!}"></script>

    <!-- Compressed JavaScript -->
    <script src="https://cdn.jsdelivr.net/foundation/6.2.1/foundation.min.js"></script>

<!--
    <script type="text/javascript" src="{!! asset('/js/bootstrap.min.js') !!}"></script>
-->
    @include('admin.partials.ga_analytics')
    @yield('inline-scripts')

    <script type="text/javascript">
    /*
        jQuery(document).ready(function($){
            $("#wrap").css('min-height', $(window).height() );
            $(window).resize(function() {
                $("#wrap").css('min-height', $(window).height() );
            });
        });
        */
    </script>

  </body>
</html>
@if( Session::has('errors') )
  <div class="alert alert-danger" role="alert" align="center">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <ul>
     @foreach($errors->all() as $error)
        <li>{{$error}}</li>
     @endforeach
  </ul>
  </div>
@endif
@if( Session::has('message') )
  <div class="alert alert-success" role="alert" align="center">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>
      <i class="fa fa-check-circle fa-lg fa-fw"></i> Success.
    </strong>
  {{ Session::get('message') }}
  </div>
@endif
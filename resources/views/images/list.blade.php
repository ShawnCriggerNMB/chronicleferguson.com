@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'gallery')
@section('content')

   <h1 class="line-thru"><span>Gallery</span></h1>
@include('images.error-notification')
<style type="text/css">
  .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
    max-height: 229px;
    max-width: 350px;
    width: 100%;
  }
</style>

<div class="row">

  <div class="dropdown pull-right">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      PER PAGE
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
<?php

	$limit = (int) $limit;
	$limit_str = NULL;

	if ( $limit > 0 )
	{
		$limit_str = '&amp;limit=' . $limit;
	}

	foreach (range(1, 10) as $number) :
    	$val = 12 * $number;
		$q   = ( $sort_string == '' ) ? '?limit=' . $val : $sort_string . '&amp;limit=' . $val;
		$sel = ( $val === $limit ) ? ' class="active" ' : '';
?>
    <li><a {{ $sel }} href="{{ url('/gallery/'.$q ) }}">{{ $val }}</a></li>
  <?php endforeach; ?>

    </ul>
  </div>

  <div class="dropdown" style="width:40%">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      FILTER BY
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      <li class="dropdown-submenu"><a tabindex="-1" href="#">DATE</a>
        <ul class="dropdown-menu">
           <form action="{{ url('/gallery/') }}" method="GET" style="position:relative">
           <input type="hidden" name="sort" value="dates" />
           <li><a class="btn" href="{{ url('/gallery/?sort=allDates' . $limit_str ) }}">ALL DATES</a></li>
           <li class="dropdown-header">DATE RANGE</li>
           <li><input name="start_date" type="date" /></li>
           <li class="dropdown-header">TO</li>
           <li><input name="end_date" type="date" /></li>
           <li><input type="submit" value="SEARCH" style="margin-top:10px" /></li>
           </form>
        </ul>
      </li>
      <li><a class="location" href="{{ url('/gallery/?sort=location'. $limit_str ) }}">LOCATION</a></li>
      <li><a class="photographer" href="{{ url('/gallery/?sort=photographer'. $limit_str ) }}">PHOTOGRAPHER</a></li>
      <li class="dropdown-submenu"><a tabindex="-1" href="#">CHRONOLOGY</a>
        <ul class="dropdown-menu">
           <li><a class="btn" href="{{ url('/gallery/?sort=chronology&amp;chronology=asc'. $limit_str ) }}">FIRST</a></li>
           <li><a class="btn" href="{{ url('/gallery/?sort=chronology&amp;chronology=desc'. $limit_str ) }}">RECENT</a></li>
        </ul>
      </li>
      <li><a class="camera" href="{{ url('/gallery/?sort=camera'. $limit_str ) }}">CAMERA</a></li>
    </ul>

<?php
	$camera = \Input::get('camera');
	$hide = ( ! \Input::get('camera') ) ? 'display:none;' : '';
?>
    <select class="camera-option" style="height: 31px;border-radius: 0;{{ $hide }}">
    	<option value="">Please select camera</option>
<?php
    	foreach ($cameras as $key => $value) :
    		$sel = ( $camera == $key OR $camera == $value ) ? ' selected ' : '';

    		echo "<option value=\"{$key}\" {$sel}>{$value}</option>\n";
    	endforeach;
?>
    </select>
<?php
  $location = \Input::get('location');
  $hide = ( ! \Input::get('location') ) ? 'display:none;' : '';
?>
    <select class="location-option" style="height: 31px;border-radius: 0;{{ $hide }}">
      <option value="">Please select location</option>
<?php
      foreach ($locations as $key => $value) :
        $sel = ( $location == $key OR $location == $value ) ? ' selected ' : '';

        echo "<option value=\"{$key}\" {$sel}>{$value}</option>\n";
      endforeach;
?>
    </select>
<?php
	$photographer = \Input::get('photographer');
	$hide = ( ! \Input::get('photographer') ) ? 'display:none;' : '';
?>
    <select class="photographer-option" style="height: 31px;border-radius: 0;{{ $hide }}">
    	<option value="">Please select photographer</option>
<?php
    	foreach ($photographers as $key => $value) :
    		$sel = ( $photographer == $key OR $photographer == $value ) ? ' selected ' : '';

    		echo "<option value=\"{$key}\" {$sel}>{$value}</option>\n";
    	endforeach;
?>
    </select>

  </div>

</div>
@if (session('status'))
    <div class="alert alert-success" style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>
          <i class="fa fa-check-circle fa-lg fa-fw"></i> {{ session('status') }}
        </strong>
    </div>
@endif
@if (session('success'))
    <div class="alert alert-success" style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>
          <i class="fa fa-check-circle fa-lg fa-fw"></i> {{ session('success') }}
        </strong>
    </div>
@endif
@if (session('message'))
    <div class="alert alert-success" style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>
          <i class="fa fa-check-circle fa-lg fa-fw"></i> {{ session('message') }}
        </strong>
    </div>
@endif

   <div class="row" style="margin-top:20px">
      @if(count($images) > 0)
         <div class="col-md-12 text-center" >
            @include('images.error-notification')
         </div>
      @endif
      @forelse($images as $image)
         <div class="col-fhd-2 col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="thumbnail">
               <a href="{{ url('/gallery/' . $image->id . $sort) }}">
                  <img src="{{asset($image->thumb)}}" />
               </a>
               <div class="caption">
                  <p>{!! substr($image->comment, 0,100) !!}</p>
               </div>
            </div>
         </div>
      @empty
         <p>No images yet, <a href="{{ url('/gallery/create') }}">add a new one</a>?</p>
      @endforelse
   </div>
   <div align="center">{!! $images->appends(['sort' => Request::input('sort'),'camera' => Request::input('camera'),'location' => Request::input('location'),'chronology' => Request::input('chronology'),'photographer' => Request::input('photographer'),'limit' => Request::input('limit')  ])->render() !!}</div>
@stop

@section('footer')
   <footer id="footer">
      <h2><span><a href="/gallery/create">UPLOAD PHOTOS</a></span></h2>
      <p>To find out more information signup for<br>updates Chronicle Ferguson</p>
   </footer>
@stop

@section('inline-scripts')
<script type="text/javascript">

function insertParam(key, value) {
	key = escape(key);
	value = escape(value);

	var kvp = document.location.search.substr(1).split('&');
	if (kvp == '')
	{
		document.location.search = '?' + key + '=' + value;
	}
	else {
		var i = kvp.length; var x; while (i--) {
			x = kvp[i].split('=');

			if (x[0] == key) {
				x[1] = value;
				kvp[i] = x.join('=');
				break;
			}
		}
		if (i < 0) { kvp[kvp.length] = [key, value].join('='); }
		document.location.search = kvp.join('&');
	}
}

function get_limit_string()
{
	return '&limit={{ $limit }}';
}

jQuery(document).ready(function($){

	$(".camera").click(function(e){
		$(".camera, .location-option, .photographer-option").fadeOut();
		$(".camera-option").fadeIn();
		e.preventDefault();
		return false;
	});

	$(".camera-option").change(function(e){
		var val = $(this).val();
		if ( '' == val ) return false;
		window.location = '/gallery?sort=camera&camera=' + val + get_limit_string();
	});

	$(".location").click(function(e){
  		$(".camera, .location-option, .photographer-option").fadeOut();
    	$(".location-option").fadeIn();
    	e.preventDefault();
    	return false;
  	});

	$(".location-option").change(function(e){
		var val = $(this).val();
		if ( '' == val ) return false;
		console.warn( '/gallery?sort=location&location=' + val + get_limit_string() );
		window.location = '/gallery?sort=location&location=' + val + get_limit_string();
	});

	$(".photographer").click(function(e){
		$(".camera, .location-option, .photographer-option").fadeOut();
		$(".photographer-option").fadeIn();
		e.preventDefault();
		return false;
	});

	$(".photographer-option").change(function(e){
    	var val = $(this).val();
    	if ( '' == val ) {
    		window.location = '/gallery?sort=photographer' + get_limit_string();
    		return false;
    	}
    	window.location = '/gallery?sort=photographer&photographer=' + val + get_limit_string();
    });

});

</script>


@stop

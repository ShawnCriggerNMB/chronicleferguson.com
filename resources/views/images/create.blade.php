@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'gallery-upload')

@section('header-styles')
  <link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/tokenfield-typeahead.min.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ url( '/assets/admin/css/bootstrap-tokenfield.min.css' ) }}">

  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.css') !!}">
  <link rel="stylesheet" type="text/css" href="{!! url('/assets/pickadate/compressed/themes/default.date.css') !!}">

@stop


@section('content')

   <h1 class="subtitle fancy"><span>Upload Photos</span></h1>
   @include('images.error-notification')

   <div class="row">
      <div class="col-lg-7 col-md-7">
         <p class="uppercase black-text-shadow">
         We are looking for photos that tell your story, whatever that looks like.
         whether it's artistic or informative, realistic or abstract. hopeful or stark.
         we want to tell the ferguson story from a perspective that hasn't been seen before - yours.
         </p>

         <p class="uppercase black-text-shadow">
         <strong>Submitting</strong> your photos to chronicle ferguson authorizes us to display
         them on our website and use them in our promotional material. but does not give
         us the right to sell your work. if your photo is chosen for possible inclusion in
         the book or art gallery showing we'll get in touch to discuss that with you.
         </p>

         <p class="uppercase black-text-shadow">
         please use the form below to submit your photo. we prefer sizes around 1MB but can accept
         anything up to 10MB.
         </p>
      </div>
      <div class="col-lg-4 col-lg-offset-1 col-md-4">

   {!! Form::open(['url'=>'/gallery', 'method'=>'POST', 'files'=>'true', 'autocomplete' => 'off']) !!}
@if ( ! \Auth::check() )
      <div class="form-group">
         <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="NAME">
      </div>

      <div class="form-group">
         <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-MAIL">
      </div>
@endif
      <div class="file-row">
         <div class="row form-group">
            <div class="col-xs-4 col-md-5">
               <span class="btn btn-default btn-file" data-clone="0">
                  CHOOSE AN IMAGE <input type="file" class="form-control" name="userfile[]" data-clone="0" multiple>
               </span>
            </div>
            <div class="col-xs-7">
               <input type="text" disabled="disabled" class="form-control" id="filename_0" name="filename" placeholder="JPG" style="text-transform:uppercase">
            </div>
         </div>
        <div class="form-group">
           <div class="row">
              <div class="col-xs-4 col-md-5 col-lg-7">
                 <input type="text" class="form-control span location" name="location[]" value="{{ old('location.0') }}" placeholder="LOCATION" data-provide="typeahead">
              </div>
              <div class="col-xs-8 col-md-7 col-lg-5">
                 <input type="text" class="form-control datepicker" name="date[]" value="{{ old('date.0') }}" placeholder="mm/dd/YYYY">
              </div>
           </div>
        </div>
      </div>
      <div class="form-group">
         <button class="upload-clone">Upload more images</button>
      </div>

      <div class="form-group">
         <textarea name="comment" rows="5" placeholder="COMMENTS" class="form-control">{{ old('comment') }}</textarea>
      </div>

      <button type="submit" class="btn pull-right">UPLOAD</button>

   {!! Form::close() !!}
      </div>
@stop

@section('footer')

   <footer id="footer">
      <h2><span><a href="/gallery">VIEW GALLERY</a></span></h2>
      <p>To find out more information signup for<br>updates Chronicle Ferguson</p>
   </footer>

@stop

@section('inline-scripts')
<script type="text/javascript" src="{!! url('/assets/admin/js/typeahead.bundle.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/bootstrap3-typeahead.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/pickadate/compressed/picker.date.js') !!}"></script>

   <script type="text/javascript">

   var template_1 = '<div class="row form-group"><div class="col-xs-4">';
   var template_2 = '</span></div><div class="col-xs-8">';
   var locations  = null;
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            inputNum = $(this).data('clone'),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label, inputNum]);
    });

    $(document).ready( function() {

        // instantiate the bloodhound suggestion engine
        locations = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          local:  ['Ferguson', 'Downtown St. Louis', 'South Side St. Louis', 'Clayton', 'Central West End', 'North St. Louis']
        });

        // initialize the bloodhound suggestion engine
        locations.initialize();

        $('.location').typeahead(
        {
          source:locations.ttAdapter()
        });
        $(document).on('focus', ':input[data-provide]', function(event){
          // instantiate the bloodhound suggestion engine
          locations = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local:  ['Ferguson', 'Downtown St. Louis', 'South Side St. Louis', 'Clayton', 'Central West End', 'North St. Louis']
          });

          // initialize the bloodhound suggestion engine
          locations.initialize();
          $('.location').typeahead('destroy');

          $('.location').typeahead(
          {
            source:locations.ttAdapter()
          });
        });

        $('.datepicker').pickadate({
          selectYears: true,
          selectMonths: true,
          format: 'mm/dd/yyyy',
          formatSubmit: 'mm/dd/yyyy'
        });

        $(document).on('focus', 'input.datepicker', function(event){

          $('.datepicker').pickadate({
            selectYears: true,
            selectMonths: true,
            format: 'mm/dd/yyyy',
            formatSubmit: 'mm/dd/yyyy'
          });

        });

         $(document).on('fileselect', '.btn-file :file', function(event, numFiles, label, inputNum) {
            console.log(inputNum);
              $("#filename_" + inputNum).val( label );
          });

         // Ferguson, Downtown St. Louis, South Side St. Louis, Clayton, Central West End, North St. Louis.

          var clones = 0;

          $(".upload-clone").click(function(event){
               event.preventDefault();
               if ( clones >= 3 ) {
                  $(this).remove();
                  return;

               }
               clones++;
               var temp = '<span class="btn btn-default btn-file" data-clone="' + clones + '">CHOOSE AN IMAGE ';
               temp = temp + '<input type="file" class="form-control" name="userfile['+clones+']" data-clone="' + clones + '">';
               var appended = template_1 + temp + template_2;

               temp = '<input type="text" disabled="disabled" class="form-control" id="filename_'+clones+'" name="filename" placeholder="JPG" style="text-transform:uppercase"></div></div>';
               appended = appended + temp;


               var temp_loc = '<div class="form-group"><div class="row"><div class="col-xs-7"><input type="text" class="form-control span location" name="location['+clones+']" placeholder="LOCATION" data-provide="typeahead"></div>';
               temp_loc = temp_loc + '<div class="col-xs-5"><input type="text" class="form-control datepicker" name="date['+clones+']" placeholder="DATE"></div></div></div>';
               appended = appended + temp_loc;
               var file_row = $(".file-row");
//               var loc_row = $(".loc-row");
               file_row.append( appended );
//               loc_row.append(temp_loc);

               return false;
          });


      });
   </script>
@stop
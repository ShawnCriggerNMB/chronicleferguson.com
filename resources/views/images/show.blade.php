@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'gallery-single')
@section('content')
@section('header-styles')

	<meta name="_token" content="{{ csrf_token() }}" />
<style type="text/css">



</style>
@endsection

@section('inline-styles')

	div.comments-area {
		margin-top: 30px;
	}

	a.like-image {
	    background-image: url('/images/CF_Logo_Black_x500.png');
	    display: block;
	    /*
	    width: 50px;
	    height: 50px;
	    */
	    background-size: 25px;
	    background-repeat: no-repeat;
	    padding-left: 30px;
	    margin-top: 20px;
	    padding-top: 1px;
	    font-size: 18px;
	    color: #000;
	}

	a.like-image:hover{
		background-image: url('/images/CF_Logo_White_x500.png');
		color:#FFF;
	}

.commentBox{
/*
	width:350px;
*/
	padding:3px;
	background:#ECEFF5;
	margin-left:18px;
	margin-top:3px;
	text-align:left;
	}
.commentPanel	{
/*
	width:350px;
*/
	padding:3px;
	background:#ECEFF5;
	margin-left:18px;
	margin-top:3px;
	padding-top:5px;
	margin-top: 10px;
	font-size:10px;
}
.postedComments{float:left; margin-left:6px; width:290px; text-align:justify;}

.commentMark{
	float:left;
/*
	width:295px;
*/
	margin-left:5px;
	margin-top: 5px;
	font-size:11px
}

a.c_delete{color:#3B5998;
	font-size:11px;}

.friends_area{
    width:500px;
	vertical-align:top;
    min-height:60px;
	padding-bottom:12px;
	padding-top:10px;
    border-bottom: #999999 solid 1px;
    }

.name strong, .commentPanel strong,
.name a,.commentPanel a{
	color:#3B5998;
	font-size:11px;
	margin:3px 3px 3px 6px;
}
.name em{
	color:#333;
	font-style:normal;
	width:110px;
	font-size:11px;
}
.name span{
	color:#777777;
	font-size:11px;
}

a.delete{

	color:#666666;
	padding:5px;
	text-decoration:none;
	-moz-background-clip:border;
	-moz-background-inline-policy:continuous;
	-moz-background-origin:padding;
	background:transparent none repeat scroll 0 0;
	border:0 none;
	color:#333333;
	cursor:pointer;
/*
	font-family:'Lucida Grande',Tahoma,Verdana,Arial,sans-serif;
*/
	font-size:11px;
	font-weight:bold;
	margin:0;
	outline-color:-moz-use-text-color;
	outline-style:none;
	outline-width:medium;
	white-space:nowrap;
}
a.delete:hover{
	opacity:1;filter:alpha(opacity = 100);-ms-filter:'alpha(opacity = 100)';
	border:solid #B1B1B1 1px;
	background:#EEEEEE;
	padding:5px;
	}

.bgwidth { width: 100%; }
.bgheight { height: 100%; }

@endsection

	<h1 class="subtitle fancy"><span>Gallery</span></h1>

<?php if ( $sort ) : ?>
	<a href="/gallery{{ $sort }}" class="btn btn-default" style="margin-bottom:10px;">BACK TO SEARCH RESULTS</a>
<?php else : ?>
	<a href="/gallery" class="btn btn-default" style="margin-bottom:10px;">BACK TO GALLERY</a>
<?php endif; ?>
	<form class="form-horizontal">

      @if ($image->prev)
        <a class="prev-image" href="{!! $image->prev !!}">
        	<i class="fa fa-chevron-left">&nbsp;</i>
        </a>
      @endif
      <img src="{{ asset($image->file) }}" class="full-size" />
      @if ($image->next)
      	<a class="next-image" href="{!! $image->next !!}">
      		<i class="fa fa-chevron-right"></i>
      	</a>
      @endif

      <input type="hidden" name="image_id" id="image_id" value="{{ $image->id }}" />

	</form>

	<div class="container-fluid">
		<div class="row comments-area">
			<div class="col-md-6 col-sm-12">
				<label style="float:left" class="name">
					<img src="{{ $image->profile_image }}" style="float:left;margin-top:-14px" width="50" alt="Profile image of {{ $image->name }}" class="img-circle" />
					<strong>{{ $image->name }}</strong><span style="color:#B7B8B8;">&nbsp;|&nbsp;</span>
					<em>{{ $image->comment }}</em><span style="color:#B7B8B8;">&nbsp;|&nbsp;{{ date( 'm/d/y', strtotime( $image->date ) ) }}&nbsp;-&nbsp;{{ $image->location }}</span>
					<br clear="all" />
					<span style="margin-left:7px; color:#666666; font-size:11px">{{ $image->posted_at }}</span>
					<a href="javascript: void(0)" id="post_id{{ $image->id }}" class="showCommentBox">Comments</a>

					@if ( \Auth::check() )
						<span id="like-panel-{{ $image->id }}">
						@if ( $image->liked > 0)
							<a href="javascript: void(0)" id="post_id{{ $image->id }}" class="Unlike">Unlike</a>
						@else
							<a href="javascript: void(0)" id="post_id{{ $image->id }}" class="LikeThis">Like</a>
						@endif
						</span>
					@endif

			   </label>
			    <br clear="all" />
				<div class="commentPanel" align="left">

					<img src="{!! url( '/images/like.png') !!}" style="float:left;" alt="" />

					<span id="like-stats-{{ $image->id }}"  data-imageId="{{ $image->id }}"> {{ $image->likes }}</span> people like this.
					<span id="like-loader-{{ $image->id }}" data-imageId="{{ $image->id }}">&nbsp;</span>
				</div>
<?php /*
				<div class="commentPanel" id="collapsed-{{ $image->id }}>" align="left">
					<img src="{{ url('/images/cicon.png') }}" style="float:left;" alt="" />
					<a href="javascript: void(0)" class="ViewComments">
					View all {{ $image->total_comments }} comments
					</a>
					<span id="loader-{{ $image->id }}">&nbsp;</span>
				</div>
*/ ?>
				<div id="comment-posted-{{ $image->id }}" data-imageId="{{ $image->id }}">
				@foreach ($comments as $row)
					<div class="commentPanel hide" align="left" id="comment-{{ $row->id }}">
						<img src="{{ $row->profile_image }}" width="35" class="CommentImg" style="float:left;" alt="" />
						<label class="postedComments">
							<strong>{{ $row->name }}</strong>
							{{ $row->comment }}

						</label>
						<br clear="all" />
						<span style="margin-left:45px; color:#666666; font-size:11px">{{ $row->posted_at }}</span>
@if ( ( \Auth::check() AND \Auth::user()->id === $row->user_id ) OR \Entrust::hasRole( ['admin', 'super_admin'] ) )
					  	<a href="#" class="delete" data-commentId="{{ $row->id }}"> Remove</a>
@endif
					</div><!-- end .commentPanel.hide -->
				@endforeach

@if ( \Auth::check() )
					<div class="commentBox" align="right" id="commentBox-{{ $image->id }}" >
						<img src="{{ url('/images/CF_Logo_Black_x500.png') }}" width="40" class="CommentImg" style="margin-top:7px;float:left;" alt="" />
						<label id="record-{{ $image->id }}" class="pull-left">
							<textarea class="form-control commentMark" id="commentMark-{{ $image->id }}" name="comment" cols="60"></textarea>
						</label>
						<br clear="all" />
						<a id="SubmitComment" class="btn btn-default" style="margin-left:45px"> COMMENT</a>
					</div>
@endif

				</div> <!-- end div.comment-posted-{{ $image->id }} -->
	   		</div> <!-- end div.col-md-6.col-sm-12 -->

			<div class="col-md-4 col-md-offset-1 col-xs-12">
				<i style="padding-right:10px;" class="fa fa-calendar-check-o" data-toggle="tooltip" data-placement="left" title="Date photograph was taken"></i>{{ date( 'm/d/y', strtotime( $image->date ) ) }}<br/>
				<i style="padding-right:10px;" class="fa fa-location-arrow" data-toggle="tooltip" data-placement="left" title="Location photograph was taken at"></i>{{ $image->location }}<br/>
				<i style="padding-right:10px;" class="fa fa-camera" data-toggle="tooltip" data-placement="left" title="Camera photograph was taken with"></i>{{ $image->camera }}<br/>
			</div>

		</div> <!-- end div.row -->
		</div>
	</div>
@stop

@section('footer')

	<footer id="footer">
		<h2><span><a href="/gallery/create">UPLOAD PHOTOS</a></span></h2>
		<p>To find out more information signup for<br>updates Chronicle Ferguson</p>
	</footer>

@stop
@section('inline-scripts')
<script src="/assets/js/autogrow.min.js"></script>
<script type="text/javascript">
/*
jQuery(window).load(function($) {

	var theWindow        = jQuery(window),
	    $bg              = jQuery("img.full-size"),
	    aspectRatio      = $bg.width() / $bg.height();

	function resizeBg() {

		if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
		    $bg
		    	.removeClass()
		    	.addClass('bgheight');
		} else {
		    $bg
		    	.removeClass()
		    	.addClass('bgwidth');
		}

	}

	theWindow.resize(resizeBg).trigger("resize");

});
*/
jQuery(document).ready(function($){
/*
	var max_width  = $(window).width();
	var max_height = $(window).height();

	$("img.full-size").css({
		'maxWidth' : max_width,
		'maxHeight': max_height
	});

	$(window).resize(function() {
		max_width  = $(window).width();
		max_height = $(window).height();

		$("img.full-size").css({
			'maxWidth' : max_width,
			'maxHeight': max_height
		});

	});
	*/

  $('[data-toggle="tooltip"]').tooltip()

	$("textarea").autogrow();

	var imageId = $("#image_id").val();
	$(".commentPanel").fadeIn().removeClass('hide');
	$("#post_id" + imageId).remove();

	$(".showCommentBox").click(function(e){
		var imageId = $("#image_id").val();
		$(".commentPanel").fadeIn().removeClass('hide');
		$("#post_id" + imageId).remove();
		e.preventDefault();
		return false;
	})
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
	});

	$("body").on("click",".delete", function(e){
		var imageId = $("#image_id").val();
		$("#like-loader-"+imageId).html('<img src="/images/loader.gif" alt="" />');
		var commentId = $(this).data('commentid');

		$.ajax({
			type: "POST",
			url: '/api/gallery/remove-comment',
			data: {
				image_id: imageId,
				comment_id: commentId

			},
			success: function( data ) {
				$("#comment-" + commentId).remove();
				$("#like-loader-"+imageId).html('');

			}
		});

		e.preventDefault();
		return false;
	});

	$("body").on("click",".Unlike", function(e){
		var imageId = $("#image_id").val();
		$("#like-loader-"+imageId).html('<img src="/images/loader.gif" alt="" />');

		$.ajax({
			type: "POST",
			url: '/api/gallery/unlike-image',
			data: {
				image_id: imageId

			},
			success: function( data ) {
				$('#like-stats-'+imageId).html( data.count );
				$('#like-panel-'+imageId).html('<a href="javascript: void(0)" id="post_id'+imageId+'" class="LikeThis">Like</a>');
				$("#like-loader-"+imageId).html('');
				if ( typeof data.message === 'string' )
				{
//					alert(data.message);
				}
			}
		});

		e.preventDefault();
		return false;
	});

	$("body").on("click",".LikeThis", function(e){

		var imageId = $("#image_id").val();
		$("#like-loader-"+imageId).html('<img src="/images/loader.gif" alt="" />');

		$.ajax({
			type: "POST",
			url: '/api/gallery/like-image',
			data: {
				image_id: imageId

			},
			success: function( data ) {
				$('#like-stats-'+imageId).html( data.count );
				$('#like-panel-'+imageId).html('<a href="javascript: void(0)" id="post_id'+imageId+'" class="Unlike">Unlike</a>');
				$("#like-loader-"+imageId).html('');
				if ( typeof data.message === 'string' )
				{
//					alert(data.message);
				}

			}
		});

		e.preventDefault();
		return false;
	});

	$("body").on("click","#SubmitComment", function(e){

		var imageId = $("#image_id").val();
		$("#like-loader-"+imageId).html('<img src="/images/loader.gif" alt="" />');

		$.ajax({
			type: "POST",
			url: '/api/gallery/post-comment',
			data: {
				image_id: imageId,
				comment : $('.commentMark').val()
			},
			success: function( data ) {
//				$('#like-stats-'+imageId).html( data.count );
//				$('#like-panel-'+imageId).html('<a href="javascript: void(0)" id="post_id'+imageId+'" class="Unlike">Unlike</a>');

				var template = '<div class="commentPanel" align="left">' +
						'<img src="'+ data.comment.profile_image + '" width="35" class="CommentImg" style="float:left;" alt="" />' +
						'<label class="postedComments">'+ data.comment.comment +'</label>' +
						'<br clear="all" />' +
						'<span style="margin-left:43px; color:#666666; font-size:11px">'+ data.comment.posted_at +'</span>' +
					  	'<a href="#" class="delete" data-commentId="'+ data.comment.id + '"> Remove</a>' +
					'</div>';

				$('.commentMark').val('');

				$( "#commentBox-" + imageId ).before( template );

//				$( template ).appendTo( "#comment-posted-" + imageId );
				$("#like-loader-"+imageId).html('');


			}
		});

		e.preventDefault();
		return false;
	});


});
</script>
@stop
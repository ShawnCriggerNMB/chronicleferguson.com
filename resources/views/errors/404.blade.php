@extends('layouts.master')

@section('content')

<!-- content -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-lg-12">
            <div class="centering text-center error-container">
                <div class="text-center">
                    <h2 class="without-margin">Don't worry. It's <span class="text-success"><big>404</big></span> error only.</h2>
                    <h4 class="text-success">Page not found</h4>
                </div>
                <div class="text-center">
                    <h3><small>Choose an option below</small></h3>
                </div>
                <hr>
                <ul class="pager">
                    <li><a href="/">&larr; Home</a></li>
                    <li><a href="/gallery">Gallery</a></li>
                    <li><a href="/contact-us">Contact US &rarr;</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@stop
@extends('layouts.master')

@section('content')

<!-- content -->
        <!-- content -->
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-lg-12">
                    <div class="centering text-center">
                        <div class="text-center">
                            <h2 class="without-margin">Don't worry. It's <span class="text-danger"><big>503</big></span> error only.</h2>
                            <h4 class="text-danger">Something is broken, but we will fix it soon</h4>
                        </div>
                        <div class="text-center">
                            <h3><small>Choose an option below</small></h3>
                        </div>
                        <hr>
                <ul class="pager">
                    <li><a href="/">&larr; Home</a></li>
                    <li><a href="/gallery">Gallery</a></li>
                    <li><a href="/contact-us">Contact US &rarr;</a></li>
                </ul>
                    </div>
                </div>
            </div>
        </div>
@stop
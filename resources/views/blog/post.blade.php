@extends('layouts.master')
<?php
  $css_class = 'post-' . $post->id
?>
@section('bodyclass', 'blog {!! $css_class !!}')
@section('content')

  {{-- The Post --}}
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
          <h2 class="post-title">{{ $post->title }}</h2>
          @if ($post->subtitle)
            <h3 class="post-subtitle">{{ $post->subtitle }}</h3>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 blog-content">
          {!! $post->content_html !!}
        </div>
      </div>
    </div>
  </article>

  {{-- The Pager --}}
  <div class="container">
    <div class="row">
      <ul class="pager">
        @if ($tag && $tag->reverse_direction)
          @if ($post->olderPost($tag))
            <li class="previous">
              <a href="{!! $post->olderPost($tag)->url($tag) !!}">
                <i class="fa fa-long-arrow-left fa-lg"></i>
                Previous {{ $tag->tag }} Post
              </a>
            </li>
          @endif
          @if ($post->newerPost($tag))
            <li class="next">
              <a href="{!! $post->newerPost($tag)->url($tag) !!}">
                Next {{ $tag->tag }} Post
                <i class="fa fa-long-arrow-right"></i>
              </a>
            </li>
          @endif
        @else
          @if ($post->newerPost($tag))
            <li class="previous">
              <a href="{!! $post->newerPost($tag)->url($tag) !!}">
                <i class="fa fa-long-arrow-left fa-lg"></i>
                Next Newer {{ $tag ? $tag->tag : '' }} Post
              </a>
            </li>
          @endif
          @if ($post->olderPost($tag))
            <li class="next">
              <a href="{!! $post->olderPost($tag)->url($tag) !!}">
                Next Older {{ $tag ? $tag->tag : '' }} Post
                <i class="fa fa-long-arrow-right"></i>
              </a>
            </li>
          @endif
        @endif
      </ul>
    </div>

  </div>@stop

@section('footer')

  <footer id="footer">
    <h2><span><a href="/gallery/create">UPLOAD PHOTOS</a></span></h2>
    <p>To find out more information signup for<br>updates Chronicle Ferguson</p>
  </footer>

@stop


@section('inline-scripts')
<script type="text/javascript" src="{!! url('/assets/js/autogrow.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/validator.js') !!}"></script>
<script type="text/javascript">
  jQuery(document).ready(function($){
    ("textarea").autogrow();
  });
</script>
@stop
@extends('layouts.master')

@section('content')
<div class="orbit" role="region" aria-label="Favorite Space Pictures">
    <ul class="orbit-container">
        <li class="orbit-slide is-active">
            <img src="/assets/images/home-hero-unit.png"></li>
    </ul>
</div>

<div class="row small-up-12 medium-up-12 large-up-12 under-dog">
    <h1 class="text-center accented">WHAT IS CHRONICLE :: FERGUSON?</h1>
    <p class="text-center">
        CHRONICLE::FERGUSON is starting out as a crowd sourced book of photographs on the subject of<br/>
        "Ferguson". We have been researching and collaborating with numerous groups and individuals<br/>
        to create a social documenton what really happened in the wake of Michael Brown’s death.
    </p>
    <a href="#" class="button learn-more">LEARN MORE</a>
</div>

<div class="row buy-book-row">
  <div class="small-12 medium-6 large-6 columns yellow-box">
        <h2>THE BOOK</h2>
        <p>
          The Community Building and Design curriculum <br/>
          engages high school students of varied interests to<br/>
          become catalysts for cultural innovation and social <br/>
          change. Through interdisciplinary methods of <br/>
          contextualization, this curriculum challenges students<br/>
          to reflect on the history of inequality in the urban<br/>
          United States.<br/>
      </p>
      <a href="#" class="button buy-book">LEARN MORE</a>
  </div>
  <div class="small-12 medium-6 large-6 columns">

      <img class="book-cover" src="/assets/images/home-book-cover.png" alt="The completed photobook">

  </div>
</div>

<section class="testimonials">
  <div class="row">
    <div class="small-12 medium-12 large-12 columns testimonial">
        <blockquote>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nrehenderit in
        </blockquote>
        <cite>Lorem Ipsum, USA</cite>
    </div>
  </div>
</section>


<section class="upload-row">
  <div class="row medium-uncollapse large-collapse" style="">
    <div class="small-12 medium-6 large-6 columns upload-media">
        <img src="/assets/images/upload-leftside.png" alt="Police standing Guard">
    </div>
    <div class="small-12 medium-6 large-6 columns yellow-box" style="min-height: 498px;">
          <h2>UPLOAD MEDIA</h2>
          <p>
          The Community Building and Design curriculum engages<br>
          high school students of varied interests to become catalysts <br>
          for cultural innovation and social change. Through<br>
          interdisciplinary methods of contextualization, this curriculum<br>
          challenges students to reflect on the history of inequality <br>
          in the urban United States.<br>
        </p>
        <a href="#" class="button buy-book">UPLOAD</a>
    </div>
  </div>
</section>

@endsection
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Password Reset</h2>

        <div>
            To reset your password, complete this <a href="{{ URL::to('password/reset', array($token)) }}">form.</a><br>

            <br>If the above link did not get sent, please copy and paste the link below into your browser.<br><br>
            {{ URL::to('password/reset', array($token)) }}
            <br>
            <br>
        </div>
    </body>
</html>
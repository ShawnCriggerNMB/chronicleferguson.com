@extends('layouts.master')

@section('header-styles')a
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/select2/select2.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/js/select2/select2-bootstrap.css') }}">
	<style type="text/css">
	.form-control-feedback { top: 10px; }
	.select2-container--default .select2-results > .select2-results__options {
    	max-height: 225px;
    	overflow-y: auto;
	}
	</style>
@stop

@section('content')


<section class="login">
	<div class="row">


	<form class="form-horizontal" id="signup" role="form" method="POST" action="{{ url('/register') }}" data-toggle="validator">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<!-- name -->
		<div class="row">
		  <div class="small-12 medium-12 columns">
		      <label class="show-for-sr" for="name">NAME</label>
		      <input type="name" name="name" placeholder="NAME" value="{{ old('name') }}" class="{{ $errors->has('name') ? 'error' : '' }}">
		  </div>
		</div>

		<!-- Email -->
		<div class="row">
			<div class="small-12 medium-12 columns required {{ $errors->has('email') ? 'has-error' : '' }}">
			<label class="show-for-sr" for="email">EMAIL</label>
		    {!! Form::input('text', 'email', null, [
		    	'id'=>'email',
		    	'class'=>'',
		    	'placeholder' => 'EMAIL',
		    	'required' => 'required',
		    	'data-required' => 'You must fill in the email field.',
		    	'data-remote'=> '/api/users/unique_email'
		    ]) !!}
		    <span class="form-control-feedback" aria-hidden="true"></span>
		    @if ( $errors->first('email') )
		      {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
		    @else
		      <span class="help-block with-errors"></span>
		    @endif
		  </div>
		</div>
		<!-- ./ email -->

		<!-- Password -->
		<div class="row">
			<div class="small-12 medium-12 columns required {{ $errors->has('password') ? 'has-error' : '' }}">
			<label class="show-for-sr" for="PASSWORD">PASSWORD</label>
		  	<div class="input-group">
		  		<input class="" data-minlength="6" type="password" name="password" id="password" value="" data-error="Minimum of 6 characters" required />
		  		<span class="input-group-btn">
		  			<button class="btn btn-default reveal" type="button" tabindex="-1"><i class="fa fa-eye"></i></button>
		  		</span>
		  	</div>
		  	<span class="form-control-feedback" style="right: 50px;" aria-hidden="true"></span>
		    @if ( $errors->first('password') )
		      {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
		    @else
		      <span class="help-block with-errors"></span>
		    @endif
		  </div>
		</div>
		<!-- ./ password -->

		<!-- Password Confirm -->
		<div class="form-group has-feedback required {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
		  <label class="col-md-3 control-label" for="password_confirmation">Password Confirm</label>
		  <div class="col-md-7">
		  	<div class="input-group">
		  		<input class="form-control" type="password" data-match="#password" name="password_confirmation" id="password_confirmation" value="" data-error="Password confirmation must match password" required />
		  		<span class="input-group-btn">
		  			<button class="btn btn-default reveal" type="button" tabindex="-1"><i class="fa fa-eye"></i></button>
		  		</span>
		  	</div>
		  	<span class="form-control-feedback" style="right: 50px;" aria-hidden="true"></span>
		    @if ( $errors->first('password_confirmation') )
		      {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
		    @else
		      <span class="help-block with-errors"></span>
		    @endif

		  </div>
		</div>

	<div class="form-group has-feedback {{ $errors->has('zipcode') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Zipcode</label>
	  <div class="col-md-7">
	    <input type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}" data-zip="zip">
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('zipcode') )
	      {!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	  </div>
	</div>

	<div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Phone Number</label>
	  <div class="col-md-7">
	    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" data-phone="phone">
	  	<span class="form-control-feedback" aria-hidden="true"></span>
	    @if ( $errors->first('phone') )
	      {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	  </div>
	</div>

	<div class="form-group {{ $errors->has('institution') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Institution</label>
	  <div class="col-md-7">
	    <input type="text" class="form-control" name="institution" value="{{ old('institution') }}">
	    {!! $errors->first('institution', '<span class="help-block">:message</span>') !!}
	  </div>
	</div>

	<div class="form-group {{ $errors->has('organization') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Organization</label>
	  <div class="col-md-7">
	    <input type="text" class="form-control" name="organization" value="{{ old('organization') }}">
	    {!! $errors->first('organization', '<span class="help-block">:message</span>') !!}
	  </div>
	</div>

	<div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Comment</label>
	  <div class="col-md-7">
	    <textarea class="form-control" name="comment">{{ old('comment')}}</textarea>
	    {!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
	  </div>
	</div>

	<div class="form-group has-feedback required {{ $errors->has('_user_type') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">User Type</label>
	  <div class="col-md-7">
	    <select class="form-control" name="_user_type" id="_user_type" required>
	      <option value="">Please select...</option>
	      <option value="photographer" {{ selected ( 'photographer',  old('_user_type') ) }}>Photographer</option>
	      <option value="follower"     {{ selected ( 'follower',      old('_user_type') ) }}>Follower</option>
	      <option value="sponsor"      {{ selected ( 'sponsor',       old('_user_type') ) }}>Become a Sponser</option>
	      <option value="educator"     {{ selected ( 'educator',      old('_user_type') ) }}>Educator</option>
	      <option value="media"        {{ selected ( 'media',         old('_user_type') ) }}>Media</option>
	    </select>
	    <span class="form-control-feedback" aria-hidden="true" style="right: 25px;"></span>
	    @if ( $errors->first('_user_type') )
	      {!! $errors->first('_user_type', '<span class="help-block">:message</span>') !!}
	    @else
	      <span class="help-block with-errors"></span>
	    @endif
	  </div>
	</div>

	<div class="form-group {{ $errors->has('interests') ? 'has-error' : '' }}">
	  <label class="col-md-3 control-label">Interested In</label>
	  <div class="col-md-7">
	    <select class="form-control select2-multiple" name="interests[]" id="interests" multiple>
	      <option value="activist"        {{ selected ( 'activist',   (array) old('interests') )       }}>Activist</option>
	      <option value="blogger"         {{ selected ( 'blogger',    (array) old('interests') )       }}>Blogger</option>
	      <option value="influencer"      {{ selected ( 'influencer', (array) old('interests') )       }}>Influencer</option>
	      <option value="journalist"      {{ selected ( 'journalist', (array) old('interests') )       }}>Journalist</option>
	      <option value="photojournalist" {{ selected ( 'photojournalist',  (array) old('interests') ) }}>Photojournalist</option>
	      <option value="educator"        {{ selected ( 'educator',   (array) old('interests') )       }}>Educator</option>
	      <option value="book"            {{ selected ( 'book',       (array) old('interests') )       }}>I want the Book!</option>
	    </select>
	  </div>
	</div>

      <div class="form-group">
        <div class="col-md-7 col-md-offset-4">
          <button type="submit" class="btn btn-primary">Sign Up</button>
          or <a class="btn btn-facebook" href="/login/facebook"><i class="fa fa-facebook"></i> | SignUp with Facebook</a>
        </div>

      	</div>
		</form>


	</div><!-- end div.row -->
</section><!-- end section.login -->
@stop


@section('inline-scripts')
	<script type="text/javascript" src="{!! url('/assets/js/select2/select2.min.js') !!}"></script>
	<script type="text/javascript" src="{!! url('/assets/js/validator.js') !!}"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$(".select2-multiple").select2();
		var form = $("#signup");

		form.validator({
			html: true,
			custom: {
				phone: function($el) {
					var formats = "(999)999-9999|999-999-9999|9999999999";
					var r = RegExp("^(" +
					               formats
					                 .replace(/([\(\)])/g, "\\$1")
					                 .replace(/9/g,"\\d") +
					               ")$");
					return r.test($el.val());
				},
				zip: function($el) {
					return /^\d{5}(-\d{4})?$/.test($el.val());
				}
			},
			errors: {
				required: 'This field is required.',
				match: 'Does not match.',
				minlength: 'Not long enough.',
				phone: 'Please enter a valid phone number.',
				zip: 'Please enter a valid zip code.'
			},
			feedback: {
			  success: 'fa fa-check',
			  error: 'fa fa-times'
			}
		});
		$(".reveal").on('click',function() {
		    var $pwd = $("#password, #password_confirmation");
		    if ($pwd.attr('type') === 'password') {
		        $pwd.attr('type', 'text');
		    } else {
		        $pwd.attr('type', 'password');
		    }
		});

	});
	</script>
@stop
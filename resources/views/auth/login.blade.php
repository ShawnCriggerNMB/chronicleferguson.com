@extends('layouts.master')

@section('content')


  <section class="login">
    <div class="row">

    <div class="small-0  medium-3 large-3 columns">&nbsp;</div>

    <div class="small-12 small-centered medium-centered medium-6 large-centered large-6 columns">
      <h1 class="accented">Log In</h1>
      <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
          <div class="small-12 medium-12 columns">
              <label class="show-for-sr" for="email">EMAIL</label>
              <input type="email" name="email" placeholder="EMAIL" value="{{ old('email') }}">
          </div>
        </div>

        <div class="row">
          <div class="small-12 medium-12 columns">
              <label class="show-for-sr" for="password">PASSWORD</label>
              <input type="password" name="password" placeholder="PASSWORD">
          </div>
        </div>

        <div class="row">
          <div class="small-12 medium-12 columns">
              <label for="remember" id="remember_me">
                <input type="checkbox" name="remember"> Remember Me
              </label>
          </div>
        </div>

        <div class="row">
          <div class="small-12 medium-6 large-6 columns">
              <button type="submit" class="small-6 button secondary">LOG IN</button>
          </div>

          <div class="small-12 medium-6 large-6 columns">
              <a href="/register" style="margin-left:25px" class="small-6 button secondary">SIGN UP NOW</a>
          </div>
        </div>

        <div class="row">
          <div class="small-12 medium-12 large-12 columns">
              <a href="/password/email" class="small-12 grey-text">
                FORGOT YOUR PASSWORD?
              </a>
          </div>
        </div>

        <div class="row">
          <div class="small-12 medium-12 large-12 columns">
            <a class="btn btn-facebook" href="/login/facebook"><i class="fa fa-facebook"></i> Continue with Facebook</a>
          </div>
        </div>

      </form>
    </div>
    <div class="small-0  medium-3 large-3 columns">&nbsp;</div>
  </div>
</section>

@endsection
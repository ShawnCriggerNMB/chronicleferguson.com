@extends('layouts.master')

@section('header-styles')
	<link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
	<link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
 			<div class="panel panel-default">
 				<div class="panel-heading">User Profile</div>
 				<div class="panel-body">

 @if (count($errors) > 0)
 					<div class="alert alert-danger">
    					<strong>Whoops!</strong> There were some problems with your input.<br><br>
    					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
@endif
@if (Session::has('success'))
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>
      <i class="fa fa-check-circle fa-lg fa-fw"></i> Success.
    </strong>
    {{ Session::get('success') }}
  </div>
@endif


					<form class="form-horizontal" role="form" method="POST" action="/profile">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="" value="{{ $user->email }}" disabled="disabled" >
							</div>
						</div>

						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="name" class="form-control" name="" value="{{ $user->name }}" >
								{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
								{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
							</div>
						 </div>

						<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
								{!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('zipcode') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Zipcode</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="zipcode" value="{{ $user->zipcode }}">
								{!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Phone Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
								{!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('institution') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Institution</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="institution" value="{{ $user->institution }}">
								{!! $errors->first('institution', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('organization') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Organization</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="organization" value="{{ $user->organization }}">
								{!! $errors->first('organization', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Comment</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="comment" value="{{ $user->comment }}">
								{!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{ $errors->has('user_type') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">User Type</label>
							<div class="col-md-6">
								<select class="form-control" name="user_type" id="user_type" >
									<option value="photographer"{{ $user->user_type == 'photographer' ? ' selected="selected"' : '' }}>Photographer</option>
									<option value="follower"{{ $user->user_type == 'follower' ? ' selected="selected"' : '' }}>Follower</option>
									<option value="sponsor"{{ $user->user_type == 'sponsor' ? ' selected="selected"' : '' }}>Sponsor</option>
									<option value="educator"{{ $user->user_type == 'educator' ? ' selected="selected"' : '' }}>Educator</option>
									<option value="media"{{ $user->user_type == 'media' ? ' selected="selected"' : '' }}>Media</option>
								</select>
							</div>
						</div>


						<div class="form-group {{ $errors->has('interests') ? 'has-error' : '' }}">
							<label class="col-md-4 control-label">Interested In</label>
							<div class="col-md-6">
								<select class="form-control select2-multiple" name="interests[]" id="interests[]" multiple>
									<option value="activist"{{ $user->interests == 'activist' ? ' selected="selected"' : '' }}>Activist</option>
									<option value="blogger"{{ $user->interests == 'blogger' ? ' selected="selected"' : '' }}>Blogger</option>
									<option value="influencer"{{ $user->interests == 'influencer' ? ' selected="selected"' : '' }}>Influencer</option>
									<option value="journalist"{{ $user->interests == 'journalist' ? ' selected="selected"' : '' }}>Journalist</option>
									<option value="photojournalist"{{ $user->interests == 'photojournalist' ? ' selected="selected"' : '' }}>Photojournalist</option>
									<option value="educator"{{ $user->user_type == 'educator' ? ' selected="selected"' : '' }}>Educator</option>
									<option value="book"{{ $user->interests == 'book' ? ' selected="selected"' : '' }}>I want the Book!</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
						 			Save
								</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('inline-scripts')
    <script src="{{ url('assets/js/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			$(".select2-multiple").select2();
			//$('.select2').select2();

		});

	</script>
@endsection
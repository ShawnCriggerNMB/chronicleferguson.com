@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', '')
@section('content')
<style type="text/css">
    .kickstarter {
        text-align: center;
    }
    .kickstarter.img {
        margin-top:60px;
    }

    .kickstarter p {
        margin: 20px 0 10px;
        font-size: 28px;
    }

</style>

    <div class="col-sm-12 col-xs-12 col-lg-12 kickstarter">
        <p>Thank you for making a donation and helping the mission of CHRONICLE :: come to light. <BR>
        </p>
    </div><!-- .column 1 -->

</div>
@stop

@section('footer')
@stop

@section('inline-scripts')

@stop
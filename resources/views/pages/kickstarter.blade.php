@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'home height-1000')
@section('content')
<style type="text/css">
    .kickstarter {
        text-align: center;
    }
    .kickstarter.img {
        margin-top:60px;
    }

    .kickstarter p {
        margin: 20px 0 10px;
        font-size: 28px;
    }

</style>

    <div class="col-sm-12 col-xs-12 col-lg-12">
        <h1 class="subtitle fancy" style="color:#000;margin-top:-80px; margin-bottom:40px"><span >BUY BOOK</span></h1>
    </div>

    <div class="col-sm-12 col-xs-12 col-lg-12 kickstarter">
        <img src="/images/kickstarter-logo-text.png" height="111" class="kickstarter-logo" />
        <p>KICKSTARTER COMING SOON. PLEASE SIGNUP TO OUR EMAIL LIST<BR>
        TO STAY INFORMED. BOOK PRE-ORDERS START 01/14/16
        </p>
    </div><!-- .column 1 -->

</div>
@stop

@section('footer')
@stop

@section('inline-scripts')

@stop
@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'supporters')
@section('content')
<style type="text/css">
    .supporters h2 {
        line-height: initial;
        font-size: 30px;
        height: 30px;
        display: block;
    }

    .round-sponsor {
        border-radius: 50%;
        text-transform: uppercase;
        color:#FFF;
        background: #000;
        text-align: center;
        font-size: 22px;
        padding-top: 35px;
    }

/* centered columns styles */
.row-centered {
    text-align:center;
}

[class*="col-centered-"] {
    display: block;
    float:none;
    /* reset the text-align */
    text-align:left;
    /* inline-block space fix */
    margin-right:-4px;
}

/* Default BS breakpoints */
@media (min-width: 768px) {
  .col-centered-sm {
    display: inline-block
  }
}

@media (min-width: 992px) {
  .col-centered-md {
    display: inline-block
  }
}

@media (min-width: 1200px) {
  .col-centered-lg {
    display: inline-block
  }
}
/*
    .row-centered { text-align: center; }

    .col-centered { display: inline-block; float:none; text-align: left; margin: 10px; }
*/


    #main-content {
        padding-top:30px;
    }
/*
    .supporters h2 {
      position: relative;
      text-align: center;
      font-size: 35px;
    }

    .supporters h2 span {
      padding: 0 15px;
      position: relative;
      z-index: 1;
      font-size:1em;
    }

    .supporters h2:before {
      background: #FFF;
      content: "";
      display: block;
      height: 3px;
      position: absolute;
        top: 94%;
      width: 100%;
    }

    .supporters h2:before {
      left: 0;
    }
*/

@media screen and (min-width: 769px) and (max-width: 1750px) {
  .fancy span:after {
        max-width: 100%
    }
}

h2 {
  font-size: 33px;
  margin-top: 30px;
  text-align: center;
  text-transform: uppercase;
}
h2.double:before {
  /* this is just to undo the :before styling from above */
  border-top: none;
}
h2.double:after {
  border-bottom: 1px solid blue;
  -webkit-box-shadow: 0 1px 0 0 red;
  -moz-box-shadow: 0 1px 0 0 red;
  box-shadow: 0 1px 0 0 red;
  content: "";
  margin: 0 auto;
  /* this centers the line to the full width specified */
  position: absolute;
  top: 45%;
  left: 0;
  right: 0;
  width: 95%;
  z-index: -1;
}
h1.line-thru,
h2.line-thru {
  display: table;
  white-space: nowrap;
}

h1.line-thru:after,
h2.line-thru:before,
h2.line-thru:after {
  border-top: 3px solid #FFF;
  content: '';
  display: table-cell;
  position: relative;
  top: 0.5em;
  width: 45%;
}

h1.line-thru {
    text-align: left;
}

h1.line-thru:after{
    width: 100%;
}

h2.line-thru:before {
  right: 1.5%;
}

h1.line-thru:after,
h2.line-thru:after {
  left: 1.5%;
}

h2.row-3.line-thru:before {
  right: 3.5%;
}
h2.row-3.line-thru:after {
  left: 3.5%;
}
h2.row-4.line-thru:before {
  right: 4.5%;
}
h2.row-4.line-thru:after {
  left: 4.5%;
}


#sponsor_list {
    font-size: 1.2em;
    margin-top: 3em;
}

p.supporters-right {
    margin-top: 40px;
    font-size: 16px;
}

#sponsor_list h2 {
    text-align: left;
    margin-top: 0;
    border-bottom: 1px solid white;
    width: 40%;
    line-height: 15px;
}

.logo-row-3 {
    width:150px;
    height:150px;
    padding-top:30px;
    display: inline-block;
    margin-left: 15px;
    margin-right: 15px;
}

.logo-row-4 {
    width: 100px;
    height: 100px;
    font-size: 16px;
    padding-top: 18px;
    display: inline-block;
    margin-left: 15px;
    margin-right: 15px;
}

.row-5 {
    width: 80px;
    height: 80px;
    font-size: 12px;
    padding-top: 13px;
    display: inline-block;
    margin-left: 7.5px;
    margin-right: 7.5px;
}


h1.fancy {
  position: relative;
  text-align: center;
}

h1.fancy span {
  padding: 0 15px;
  position: relative;
  z-index: 1;
  font-size:3em;
}


#footer .fancy span:after{
    max-width: 100%;
}

.footer-text {
    margin-top:90px;
}


#footer .fancy span:after { display: none}

#footer h2:before {
    bottom: -240%;
    top: initial;
}
@media (min-width: 992px) {
  #footer h2::before {
      bottom: -139%;
      top: initial;
  }
  #footer h2 span {
    font-size: 2em;
  }
  .footer-text {
    margin-top: 60px;
  }
}

@media (max-width: 767px) {
  #footer h2 span {
    font-size: 1em;
  }
  #footer h2::before {
      bottom: -23%;
      top: initial;
  }
  .footer-text {
    margin-top: 15px;
    font-size: 12px;
  }

}

@media (min-width: 768px) {
  #footer h2 span {
    font-size: 1.5em;
  }
  #footer h2::before {
      bottom: -62%;
      top: initial;
  }
  .footer-text {
    margin-top: 20px;
  }

}

@media (min-width: 992px) {
  #footer h2::before {
      bottom: -139%;
      top: initial;
  }
  #footer h2 span {
    font-size: 2em;
  }
  .footer-text {
    margin-top: 70px;
  }
}

@media (min-width: 1200px) {
  #footer h2 span {
    font-size: 3em;
  }
}

@media (min-width: 1219px) {

    .logo-row-3 {
        width:150px;
        height:150px;
        padding-top:30px;
        display: inline-block;
        margin-left: 40px;
        margin-right: 40px;
    }

    .logo-row-4 {
        width: 100px;
        height: 100px;
        font-size: 16px;
        padding-top: 18px;
        display: inline-block;
        margin-left: 30px;
        margin-right: 30px;
    }

    .row-5 {
        width:100px;
        height:100px;
        font-size:16px;
        padding-top:18px;
        display:inline-block;
        margin-left: 15px;
        margin-right: 15px;
    }
}


</style>

</div>
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <h1 class="line-thru"><span>ESSENTIAL SPONSORS</span></h1>
        </div>
    </div>
    <div class="row-centered">
        <div class="col-sm-6 col-xs-12 col-lg-2 col-centered col-centered-sm">
            <div class="round-sponsor" style="margin:0 auto; width:200px; height:200px; font-size:30px">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
    </div>
    <div class="row-centered">
        <div class="col-centered col-centered-sm col-xs-12 col-md-6">
            <h2 class="line-thru">PROJECT SPONSORS</h2>
        </div>
    </div>

    <div class="row-centered">
        <div class="col-sm-12 col-centered col-centered-sm" style="text-align:center">
            <div class="round-sponsor logo-row-3">
                YOUR <br>LOGO<br> HERE
            </div>
            <div class="round-sponsor logo-row-3" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor logo-row-3" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
<!--
        <div class="col-sm-3 col-xs-4 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-3" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-3 col-xs-4 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-3" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-3 col-xs-4 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-3" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
-->
    </div>
    <div class="row-centered">
        <div class="col-centered col-centered-sm col-xs-12 col-md-8">
            <h2 class="line-thru row-3">$10,000</h2>
        </div>
    </div>

    <div class="row-centered" style="margin:0 auto; text-align:center">
        <div class="col-sm-12 col-centered col-centered-sm" style="text-align:center">
            <div class="round-sponsor logo-row-4">
                YOUR <br>LOGO<br> HERE
            </div>
            <div class="round-sponsor logo-row-4" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor logo-row-4" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor logo-row-4" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor logo-row-4" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
    <!--

        <div class="col-sm-2 col-xs-2 col-lg-2 col-centered-md col-centered-sm col-md-offset-1">
            <div class="round-sponsor row-4">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-2 col-xs-2 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-4">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-2 col-xs-2 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-4">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-2 col-xs-2 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-4">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
        <div class="col-sm-2 col-xs-2 col-lg-2 col-centered-md col-centered-sm">
            <div class="round-sponsor row-4">
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
    </div>

    -->
    <div class="row-centered">
        <div class="col-centered col-centered-sm col-xs-12 col-md-9">
            <h2 class="row-3 line-thru">$5,000</h2>
        </div>
    </div>

    <div class="row-centered" style="margin:0 auto; text-align:center">
        <div class="col-sm-12 col-centered col-centered-sm" style="text-align:center">
            <div class="round-sponsor row-5">
                YOUR <br>LOGO<br> HERE
            </div>
            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>

            <div class="round-sponsor row-5" >
                YOUR <br>LOGO<br> HERE
            </div>
        </div>
    </div>
    <div class="row-centered">
        <div class="col-centered col-centered-sm col-xs-12 col-md-10">
            <h2 class="row-4 line-thru">$2,500</h2>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-6 col-xs-12 col-lg-4" id="sponsor_list">
            <h2>SUPPORTERS</h2>

            <ul style="text-align:left">
                <li>f-stop Gear</li>
                <li>The Dark Room Photo Gallery and Wine Bar</li>
            </ul>
        </div><!-- .column 1 -->
        <div class="col-sm-6 col-xs-12 col-lg-8">
            <p class="supporters-right">USING CROWD-SOURCED IMAGES WE HOPE TO GENERATE AN ARTISTIC AND HISTORICALLY ACCURATE BOOK THAT WILL SHOW NEW PERSPECTIVES AND FACILITATE NEW CONVERSATIONS. COPIES OF THE BOOK WILL BE DONATED TO LOCAL SCHOOLS AND LIBRARIES TO PRESERVE THE STORY FOR YEARS TO COME. WE ARE ASKING FOR YOUR ASSISTANCE IN FULFILLING OUR MISSION,INVOKING POSITIVE PARTICIPATION FROM THE COMMUNITY AND ITS LEADERS, AND INSPIRING THE CHANGE WE NEED.
            ANY CONTRIBUTIONS TO HELP MAKE THIS MOMENTOUS PROJECT POSSIBLE ARE GREATLY APPRECIATED NOT ONLY BY US BUT THE COMMUNITY AS WELL.
            </p>
        </div><!-- column 2 -->
    </div>
</div>


@stop

@section('footer')

   <footer id="footer" class="container-fluid">
    <div class="row">
          <h2 class=""><span><a href="https://www.generosity.com/education-fundraising/chronicle-ferguson-a-history-making-photo-book">DONATE TODAY</a></span></h2>
          <p class="footer-text">To find out more information signup TO<br>Chronicle Ferguson as a sponsor</p>
    </div>
   </footer>

@stop

@section('inline-scripts')

@stop
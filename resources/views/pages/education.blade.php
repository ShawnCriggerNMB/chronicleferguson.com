@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'educational height-1000')
@section('content')
<style type="text/css">
    .educational h2 {
    line-height: initial;
    font-size: 2em;
    height: 30px;
    display: block;
    }

    .top_slice,
    .bottom_slice {
        height: 150px;
    }

    .top_slice {
        background: url('/images/top_burn.png') no-repeat;
        background-size: 100% 100%;
/*        background-size: cover; */
        margin-left: -15px;
        margin-right: -15px;
    }

    .bottom_slice {
        background: url('/images/bottom_burn.png') no-repeat;
        background-size: 100% 100%;
/*        background-size: cover; */
        margin-left: -40px;
        margin-right: -40px;
    }

    .educational .middle-section {
        padding-top:0;
    }

</style>

<div class="top_slice">&nbsp;</div>
    <div class="col-sm-12 col-xs-12 col-lg-12">
        <h1 class="subtitle fancy" style="margin-top:-80px; margin-bottom:40px"><span style="color:#FFF">Educational</span></h1>
    </div>

    <div class="col-sm-12 col-xs-12 col-lg-6 educational">
        <h2>CHRONICLE::FERGUSON CURRICULUM</h2>
        <p>
The Community Building and Design curriculum engages high school students of varied interests to become catalysts for cultural innovation and social change. Through interdisciplinary methods of contextualization, this curriculum challenges students to reflect on the history of inequality in the urban United States. A significant portion of this curriculum is dedicated to understanding and connecting themes of the past to the present.
</p><p>
The Community Building and Design curriculum facilitates space for creative minds to not just ask questions, but also pursue them. Fundamental to this curriculum is its problem-based component, which challenges students to address a real-world problem within their local communities through an innovative and creative approach.
</p><p>
The photobook created by Chronicle :: will be used as an example of one of the infinite ways to making change through an innovative approach.
        </p>
    </div><!-- .column 1 -->
    <div id="educational" class="educational col-sm-12 col-xs-12 col-lg-6">
<h2>Program Goals</h2>
<p>
The objective of this course is to spark discussion and action within young people—through an interdisciplinary means of exploration to develop investment and involvement within one’s own community.
</p><p>
<h2>Educational Goals</h2>
The learning objectives are for students to grasp an understanding of history relating to inequality in the urban United States and of how such inequalities impact institutionalized spaces, communities and individuals.
Additionally, students are expected to learn research methodology to develop and implement original projects to explore or address social issue(s) of choice.
        </p>
    </div><!-- column 2 -->

</div>
@stop

@section('footer')
   <footer id="footer" class="container-fluid" style="margin-top:0;padding-top:0">
   <div class="bottom_slice">&nbsp;</div>
    <div class="row-centered">
          <h2 class=""><span><a href="/">NEWSLETTER SIGNUP</a></span></h2>
          <p class="footer-text">To find out more information signup TO<br>Updates from Chronicle Ferguson.</p>
    </div>
   </footer>
@stop

@section('inline-scripts')

@stop
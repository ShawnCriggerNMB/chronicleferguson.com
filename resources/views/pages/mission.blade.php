@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'mission height-1300')
@section('content')
<style type="text/css">
    #educational h2 {
    line-height: initial;
    font-size: 2rem;
    height: 30px;
    display: block;
    }


#footer {
    clear: both;
}

.top_slice,
.bottom_slice {
    height: 150px;
}

.top_slice {
    background: url('/images/mission_top.png') no-repeat;
    background-size: 100% 100%;
/*    background-size: cover; */
    margin-left: -15px;
    margin-right: -15px;
}

.bottom_slice {
    background: url('/images/bottom.png') no-repeat;
    background-size: 100% 100%;
/*    background-size: cover; */
    margin-left: -40px;
    margin-right: -40px;
}

.mission .middle-section {
    padding-top:0;
}

.row.m50 {
    margin-top: 50px;
    margin-bottom: 50px;
}

.circle-graph,.line-graph { margin-top : 30px; margin-bottom: 30px }
</style>
<div class="top_slice">&nbsp;</div>
    <div class="col-sm-12 col-xs-12 col-lg-12">
        <h1 class="subtitle fancy" style="margin-top:-80px; margin-bottom:40px"><span style="color:#FFF">Mission</span></h1>
    </div>

    <div class="row">
        <div class="col-sm-12 col-xs-12 col-lg-12">
            <h3>WHAT IS CHRONICLE::FERGUSON?</h3>
        </div>
    </div>

    <div class="row" style="margin-bottom:50px">
        <div class="col-sm-6 col-xs-12 col-lg-6">
            <p>CHRONICLE::FERGUSON is working toward publishing a crowd sourced book of
    photographs on the subject of "Ferguson". We have been researching and
    collaborating with numerous groups and individuals tocreate a social documenton
    what really happened in the wake of Michael Brown’s death. Our main objectives are to
    compile a large photo database from first person accounts, generate a chronological
    photo narrative, create a curriculum to accompany the book for schools, and offer a
    workshop for youth to learn the importance of sharing their lives through photography.
            </p>
        </div><!-- .column 1 -->
        <div class="col-sm-6 col-xs-12 col-lg-6">
            <p>This book, CHRONICLE::FERGUSON, will comprise work from potentially hundreds of
    photographers, and is founded upon the idea of both telling a story and answering a
    need. As a registered 501(c)3 nonprofit venture, copies of the publication will be
    donated to all public schools and libraries with in the city and county of St. Louis. This
    is because it is paramount for the message to not only begenerated, but to possess
    the basic ability to be received. Our book will beare source for future generations to
    look back upon and study what happened here in Ferguson and St. Louis.
            </p>
        </div><!-- column 2 -->

    </div>

    <div class="row">

        <div class="col-sm-4 col-xs-12 col-lg-4 mission-advisory">
            <h3 class="name">SANTIAGO BIANCO</h3>
            <p>
                <img class="profile-img" src="/images/Santiago.png" alt="profile img" />
                Santiago Bianco, a St. Louis photographer, envisioned a book
                that would tell the story of what happened in Ferguson.
                Santiago is the spokesperson for CHRONICLE::FERGUSON.
                He has been on the ground in Ferguson
                and can relate to those contributing their images to the
                book.He is in charge of sourcing content from
                photographers around St. Louis. Now that the
                foundation for the project has been set, he maintains
                its focus to ensure that its vision is fulfilled.
            </p>
        </div>
        <div class="col-sm-4 col-xs-12 col-lg-4 mission-advisory">
            <h3 class="name">STEVE SOMMERS</h3>
            <p>
            <img class="profile-img" src="/images/Steve.png" alt="profile img" />
                self-employed, entrepreneur
                with over thirty years of experience in a variety of
                fields. Recently though, he has focused his efforts on
                community building through his work as Curator of
                TEDx Gateway Arch. Being a St.Louis native and having
                been involved in numerous peaceful protests and
                demonstrations, Steve is well aware of the issues
                facing this community. Steve serves as the business
                end of CHRONICLE::FERGUSON, and brings his
                expertise in community building, sponsorship, and
                fundraising.
            </p>
        </div>
        <div class="col-sm-4 col-xs-12 col-lg-4 mission-advisory">
            <h3 class="name">JASON GRAY</h3>
            <p>
            <img class="profile-img" src="/images/Jason.png" alt="profile img" />
                Jason Gray is an independent
                curator and photographer in St.
                Louis, who has contributed exhibitions to The Dark
                Room Photo Gallery and Wine Bar,the Kranzberg
                ArtsCenter, and the International Photography Hall
                of Fame and Museum, among others. For CHRONICLE::FERGUSON,
                Mr. Gray contributes his expertise
                concerning image sequencing, image licensing, arts
                writing and data management.
            </p>
        </div>
    </div>

    <div class="row m50">
        <div class="col-sm-5 col-xs-12 col-lg-5 mission-advisory">
            <h3>CHRONICLE::FERGUSON Advisory Committee</h3>
            <ul>
                <li><strong>PATRICK SLY</strong> - Executive Vice President, Emerson</li>
                <li><strong>MICHAEL BUTLER</strong> - Missouri State Representative</li>
                <li><strong>DR. MICHAEL PRESSIMONE</strong> - President, Fontbonne University</li>
                <li><strong>VANITY GEE</strong> - Director of Community Programs, Craft Alliance</li>
                <li><strong>LEON WILLIAMSON</strong> - Radio One</li>
                <li><strong>ELLEN CURLEE</strong> - Independent Arts Consultant</li>
                <li><strong>MALISSA SHAW</strong> - President, Urban League Young Professionals</li>
            </ul>
        </div>
        <div class="col-sm-3 col-xs-12 col-lg-3 mission-advisory">
            <img src="/images/Circle_graph.png" alt="Circle graph" class="circle-graph" />
        </div><!-- column 2 -->
        <div class="col-sm-4 col-xs-12 col-lg-4 mission-advisory">
            <img src="/images/Line_graph1.png" alt="Line graph" class="line-graph" />
        </div><!-- column 2 -->

    </div>
@stop

@section('footer')
   <footer id="footer" class="container-fluid" style="margin-top:0;padding-top:0">
   <div class="bottom_slice">&nbsp;</div>
    <div class="row-centered">
          <h2 class=""><span><a href="/contact">CONTACT US</a></span></h2>
          <p class="footer-text">To find out more information signup TO<br>Updates from Chronicle Ferguson.</p>
    </div>
   </footer>
@stop
@section('inline-scripts')

@stop
@extends('layouts.master')
@section('bodyclass', 'contact-us')
@section('content')

<section class="contact-us">
  <div class="row expanded" style="">
    <div class="small-12 medium-6 large-6 columns upload-media">
        <img class="sub-15" src="/assets/images/contact-leftside.png" alt="Protestors Demenstrating ">
    </div>
    <div class="small-12 medium-6 large-6 columns" style="padding-top: 80px; min-height: 498px;">
          <h2 class="accented">Have a question?<br/>Contact Us.</h2>
          <p class="black-text">
          We frequently work out of Trex, a coworking space <br/>
          located in downtown stl.
          </p>
          <p class="grey-text">
            911 Washington Avenue<br/>
            St. Louis, MO 63101
          </p>

{!! Form::open(array('route' => 'contact_store', 'class' => '', 'data-toggle' => 'validator' )) !!}

  <div class="row">
    <div class="small-12 medium-8 columns">
        <label class="show-for-sr" for="name">Name</label>
        <input type="text" name="name" placeholder="NAME">
    </div>
  </div>
  <div class="row">
    <div class="small-12 medium-8 columns">
      <label class="show-for-sr" for="email">Email</label>
      <input type="email" name="email" placeholder="EMAIL">
    </div>
  </div>
  <div class="row">
    <div class="small-12 medium-8 columns">
        <label class="show-for-sr" for="message">Message</label>
        <textarea name="message" rows="6" placeholder="MESSAGE"></textarea>
    </div>
  </div>
  <div class="row">
    <div class="small-12 medium-8 columns">
      <input type="submit" value="SEND" class="button secondary" />
    </div>
  </div>


  {!! Form::close() !!}

  </div>
  </div>
</section>


  @stop


@section('inline-scripts')
<script type="text/javascript" src="{!! url('/assets/js/autogrow.min.js') !!}"></script>
<script type="text/javascript" src="{!! url('/assets/js/validator.js') !!}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		("textarea").autogrow();
	});
</script>
@stop
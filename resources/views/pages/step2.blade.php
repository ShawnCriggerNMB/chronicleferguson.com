@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'home height-1000')
@section('content')

<div class="col-lg-9 col-lg-offset-2 big-text clearfix">
    <h2>Thank you for signing up!</h2>
</div>
<div class="col-lg-9 col-lg-offset-2 big-text-excerpt clearfix">
    <span>Describe yourself: check all that apply.</span>
</div>

<div class="col-lg-7 col-lg-offset-4 register-form">
@if (count($errors) > 0)
    <div class="row">
        <div class="alert alert-danger col-sm-8">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<link rel="stylesheet" href="/assets/js/prettyCheckable/dist/prettyCheckable.css">

<style type="text/css">


.interests
{
    position: absolute;
    border: 0px;
    width: 100%;
    height: 2em;
    right: -160px;
    margin-top:0 !important;
/*    top: 0; */
}

.register-wrapper label {
    margin-bottom: 0;
}

.prettyradio>a{
  position: absolute;
  right:-35px;
}
</style>
    <div class="col-sm-3 col-xs-12 col-lg-2 register-wrapper">
    <form id="registration-form" name="registration-form" action="{{ url('/signup-complete') }}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
@if ( \Auth::check() )
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
@endif

        <ul class="register-navigation">
            <li data-id="1" class="button-activist">
                <label for="activist">Activist
                    <input style="margin:0; margin-top:-24px !important" id="" type="radio" class="interests" name="activist" value="activist" data-id="1" />
                </label>
            </li>
            <li data-id="2" class="button-blogger">
                <label for="blogger">Blogger
                    <input style="margin:0; margin-top:-24px !important" id="blogger" type="radio" class="interests" name="blogger" value="blogger" data-id="2" />
                </label>
            </li>
            <li data-id="3" class="button-influencer">
                <label for="influencer">Influencer</label>
                <input style="margin:0; margin-top:-24px !important" id="influencer" type="radio" class="interests" name="influencer" value="influencer" data-id="3" />
            </li>
            <li data-id="4" class="button-journalist">
                <label for="journalist">Journalist</label>
                <input style="margin:0; margin-top:-24px !important" id="journalist" type="radio" class="interests" name="journalist" value="journalist" data-id="4" />
            </li>
            <li data-id="5" class="button-photojournalist">
                <label for="photojournalist">Photojournalist</label>
                <input style="margin:0; margin-top:-24px !important" id="photojournalist" type="radio" class="interests" name="photojournalist" value="photojournalist" data-id="5" />
            </li>
            <li data-id="6" class="button-Educator">
                <label for="Educator">Educator</label>
                <input style="margin:0; margin-top:-24px !important" id="Educator" type="radio" class="interests" name="educator" value="educator" data-id="6" />
            </li>
            <li data-id="7" class="button-book">
                <label for="book">I want the Book!</label>
                <input style="margin:0; margin-top:-24px !important" id="book" type="radio" class="interests" name="book" value="book" data-id="7" />
            </li>
        </ul>
                <button class="btn" type="submit">Signup</button>
    </form>
    </div><!-- .register-wrapper -->
    </div>
</div>
@endsection

@section('footer')

@section('inline-scripts')

<script src="/assets/js/prettyCheckable/dist/prettyCheckable.min.js"></script>
<script type="text/javascript">

    jQuery( document ).ready(function($){
/*
      $('input.interests').each(function(){
          $(this).prettyCheckable({
            color: 'red',
            labelPosition: 'left',
          });
        });
*/
        $("li[class^='button-']").click(function(e){
            var id = $(this).data('id');
            id--;
            if ( 'checked' === $(".interests").eq(id).attr('checked') )
            {
              $(".interests").eq(id).attr('checked', false );
              $(".prettyradio>a").eq(id).removeClass('checked');
            } else {
              $(".interests").eq(id).attr('checked', 'checked');
            }
            console.log(id);
            e.preventDefault();
            return false;
        });

    });

</script>
@endsection
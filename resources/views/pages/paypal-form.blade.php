@extends('layouts.master')

@section('title', 'Chronicle Ferguson')
@section('bodyclass', 'donation')
@section('content')

<?php
/**
 * Paypal form view file, please check the code to see how to use. All Paypal Variables can be found here.
 *
 * SC-Tools are a few random classes I wrote over the years to make my life easier.  If it makes your life easier consider buying me a beer or a coffee would be better.
 *
 * @author Shawn Crigger
 * @version 0.1.1
 * @copyright February 21, 2013
 * @package SC-Tools
 * @subpackage paypal
 * @category views
 * @link https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/#id08A6HI00JQU
 * @uses  site_url()        Same as Wordpress's function or codeigniter's function
 * @uses  cms_get_option()  Gets option from config database, can be replaced with wordpresses get_option()
 **/


/**
 * Custom Invoice ID to pass to paypal
 * @var string
 */
$order_id = '';

/**
 * Array of (object) $item
 * @var array
 */
$items = array();
/*
// items is a array of objects with a name and amount per item.
$item = (object) array(
	'name' => 'name of item',
	'amount' => 'amount for this item.'
);

*/


/**
 * Set to true to use live paypal, false for sandbox.
 * @var boolean
 */
$paypal_live   = TRUE;

/**
 * Business email address to send payments to
 *
 * @var string
 */
$paypal_email  = 'chronicle.ferguson@gmail.com';

//$paypal_email = cms_get_option( 'paypal_email' );

/**
 * The URL to POST the form to PayPal's website.
 * @var string
 */
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr';

// debug code.
if ( FALSE === $paypal_live )
{
    $paypal_url    = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    $paypal_email  = 'postcard840-facilitator@yahoo.com';
}

//$paypal_email  = 'postcard840@yahoo.com';

/**
 * Optional pass-thru variable, will be returned in the ipn handler as $post_data['custom']
 * @var string
 */
$paypal_custom = NULL;

/**
 * Item name displayed on PayPal invoice.
 * @var string
 */
$paypal_item   = 'Donation to ' . \Config::get('blog.title');

/**
 * The URL of the 150x50-pixel image displayed as your logo in the upper left corner of the PayPal checkout pages.
 * @var NULL|STRING
 */
$paypal_logo_url = NULL;

/**
 * The image at the top left of the checkout page. The image's maximum size is 750 pixels wide by 90 pixels high.
 * PayPal recommends that you provide an image that is stored only on a secure (https) server.
 *
 * @var NULL|STRING
 */
$paypal_header_url = NULL;

/**
 * The custom payment page style for checkout pages. Allowable values are:
 *      - paypal – use the PayPal page style
 *      - primary – use the page style that you marked as primary in your account profile
 *      - page_style_name – use the custom payment page style from your account profile that has the specified name
 *
 * @var NULL|STRING
 */
$paypal_page_style = 'primary';

/**
 * Sets the text for the Return to Merchant button on the PayPal Payment Complete page.
 *
 * @var NULL|STRING
 */
$paypal_return_text = 'Return to ' . \Config::get('blog.title');

?>

<section class="donation">

	<div class="row">

		<div class="small-12 medium-6 large-6 columns">
			<div class="donate-form">
				<h1 class="subtitle accented">donate today</h1>
				<form id="paypal_form" class="clearfix" action="<?php echo $paypal_url;?>" method="POST">

			<div class="row">
				<div class="small-12 medium-10 columns">
					<label for="first_name" class="show-for-sr">First Name</label>
					<input type="text" name="first_name" placeholder="NAME " />
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-10 columns">
				<label for="email" class="show-for-sr">Email Address</label>
				<input type="text" name="email" placeholder="EMAIL" />
			</div>
		</div>

		<div class="row">
			<div class="small-12 medium-10 columns">
			<label for="amount" class="show-for-sr">Donation Amount</label>
			<input type="text" name="amount" placeholder="AMOUNT" />
		</div>
	</div>

	<input type="hidden" name="item_name" value="<?php echo $paypal_item; ?>">
<?php
/*
	The following hidden fields setup various PayPal things like ipn address, return page, etc, etc
	@link https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/#id08A6HI00JQU
 */
?>

			<input type="hidden" name="cmd"           value="_xclick">
			<input type="hidden" name="upload"        value="1">

			<input type="hidden" name="charset"       value="utf-8">
			<input type="hidden" name="rm"            value="1">
			<input type="hidden" name="no_shipping"   value="1">
			<input type="hidden" name="currency_code" value="USD">

			<input type="hidden" name="notify_url"    value="<?php echo url( '/donation-ipn' ); ?>">
			<input type="hidden" name="business"      value="<?php echo $paypal_email; ?>">
	<?php if ( isset( $order_id ) && ! is_null( $order_id ) ) : ?>
			<input type="hidden" name="invoice"       value="<?php echo $order_id ?>" >
	<?php endif; ?>
	<?php if ( isset( $paypal_custom ) && ! is_null( $paypal_custom ) ) : ?>
		<input type="hidden" name="custom"            value="<?php echo $paypal_custom ?>" >
	<?php endif;  ?>
	<?php if ( isset( $paypal_logo_url ) && ! is_null( $paypal_logo_url ) ) : ?>
			<input type="hidden" name="image_url"     value="<?php echo $paypal_logo_url; ?>">
	<?php endif; ?>
	<?php if ( isset( $paypal_header_url ) && ! is_null( $paypal_header_url ) ) : ?>
			<input type="hidden" name="cpp_header_image" value="<?php echo $paypal_header_url; ?>">
	<?php endif; ?>
	<?php if ( isset( $paypal_page_style ) && ! is_null( $paypal_page_style ) ) : ?>
			<input type="hidden" name="page_style" value="<?php echo $paypal_page_style; ?>">
	<?php endif; ?>

			<input type="hidden" name="return"        value="<?php echo url( '/donation-success' ); ?>">
			<input type="hidden" name="cancel_return" value="<?php echo url( '/donation-cancel' ); ?>">
	<?php if ( isset( $paypal_return_text ) && ! is_null( $paypal_return_text ) ) : ?>
			<input type="hidden" name="cbt"           value="<?php echo $paypal_return_text; ?>">
	<?php endif; ?>
	<input type="submit" value="SEND" class="button secondary" />
		</form>
	</div><!-- end .donate-form -->
	</div><!-- end .grid columns -->

	<div class="small-12 medium-6 large-6 columns donate-bg">
<!--
		<img src="/assets/images/donate-rightside.png" alt="Police standing Guard">
-->
	</div>

	</div><!-- end .row -->

</section>

@stop

@section('subfooter')

	<section class="testimonials">
	  <div class="row">
	    <div class="small-12 medium-12 large-12 columns testimonial">
	    </div>
	  </div>
	</section>


@endsection


@section('inline-scripts')

@stop
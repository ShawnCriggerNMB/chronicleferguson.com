<section class="footer">
    <div class="row">
        <div class="small-12 medium-4 large-4 columns">

            <h5>Sign up for our Newsletter</h5>
            <form action="" method="post" accept-charset="utf-8">
                <label class="show-for-sr">Email Address</label>
                <input class="footer-input" type="text" name="email" value="">
                <input class="button secondary" type="submit" name="submit_newsletter" />
            </form>

        </div>
        <div class="small-12 medium-4 large-4 text-center columns">
            <a href="#">PRIVACY POLICY</a>&nbsp;|&nbsp;<a href="#">TERMS OF SERVICE</a>

            <ul class="footer-social">
                <li class="social">
                    <a href="{{ $social_links['facebook'] }}">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li class="social">
                    <a href="{{ $social_links['twitter'] }}">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="social">
                    <a href="{{ $social_links['instagram'] }}">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
            </ul>

        </div>
        <div class="small-12 medium-4 large-4 columns text-center">
            &copy; 2016 CHronicle :: Ferguson<br/>
            All rights reserved
        </div>
    </div>
    <p class="bottom-footer text-center">
        501 &copy; (3) Nonprofit Organization
    </p>
</section>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>$(document).foundation();</script>
    <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
</body>
</html>
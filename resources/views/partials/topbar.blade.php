<div class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menu-text">Chronicle::Ferguson</li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu top-subnav">
            <li>
                <a href="/login">LOG IN OR REGISTER</a>
            </li>
            <li class="social">
                <a href="{{ $social_links['facebook'] }}">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li class="social">
                <a href="{{ $social_links['twitter'] }}">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li class="social">
                <a href="{{ $social_links['instagram'] }}">
                    <i class="fa fa-instagram"></i>
                </a>
            </li>
        </ul>
        <ul class="grouped-navigation button-group">
            <li>
                <a href="#" class="button buy-book-btn">BUY BOOK</a>
            </li>
            <li>
                <a href="#" class="button donate-btn">Donate</a>
            </li>
        </ul>
        <ul class="menu main-navigation">
            <li>
                <a class="{{ set_active('about') }}" href="{{ url('/about') }}">About</a>
            </li>
            <li>
                <a class="{{ set_active('gallery') }}" href="{{ url('/gallery') }}">Gallery</a>
            </li>
            <li>
                <a class="{{ set_active('store') }}" href="{{ url('/store') }}">Store</a>
            </li>
            <li>
                <a class="{{ set_active('blog') }}" href="{{ url('/blog') }}">Blog</a>
            </li>

            <li>
                <a class="{{ set_active('contact') }}" href="{{ url('/contact') }}">Contact Us</a>
            </li>

        </ul>
    </div>
</div>